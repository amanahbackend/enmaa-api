﻿using DispatchProduct.Estimating.Quoting.API.ServicesViewModels;
using DispatchProduct.Estimating.Quoting.API.Settings;
using DispatchProduct.Estimating.Quoting.BLL.ServicesViewModels;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.API.ServicesCommunication.EmployeeSalary
{
    public interface IEmployeeSalaryService : IDefaultHttpClientCrud<EmployeeSalaryServiceSetting, EmployeeSalaryViewModel, EmployeeSalaryViewModel>
    {
        Task<EmployeeSalaryViewModel> GetEmployeeByEmployeeId(string empId,string authHeader);
    }
}
