﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Estimating.Quoting.BLL.IManagers;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Estimating.Quoting.API.ServicesCommunication.Inventory;
using DispatchProduct.Estimating.Quoting.API.ServicesCommunication.EmployeeSalary;
using DispatchProduct.Estimating.Quoting.API.ServicesViewModels;
using Utilites;
using DispatchProduct.Estimating.Quoting.BLL.ViewModel;
using DispatchProduct.Estimating.Quoting.BLL.ServicesViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Estimating.Quoting.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class EstimationItemController : Controller
    {
        public IEstimationItemManager manger;
        public readonly IMapper mapper;
        IInventoryService inventoryService;
        IEmployeeSalaryService employeeService;
        public EstimationItemController(IEstimationItemManager _manger, IMapper _mapper, IInventoryService _inventoryService, IEmployeeSalaryService _employeeService)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            inventoryService = _inventoryService;
            employeeService = _employeeService;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
           EstimationItemViewModel result = new EstimationItemViewModel();
            result = await manger.GetAsync(id);
            return Ok(result);
        }
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            List<EstimationItemViewModel> result = new List<EstimationItemViewModel>();
            // List<EstimationItem> entityResult = new List<EstimationItem>();
            result = await manger.GetAllAsync();
         //result = mapper.Map<List<EstimationItem>, List<EstimationItemViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]EstimationItemViewModel model)
        {

            var entityResult =await manger.AddAsync(model);
            
            return Ok(entityResult);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]EstimationItemViewModel model)
        {
            bool result = false;
            //EstimationItem entityResult = new EstimationItem();
            //entityResult = mapper.Map<EstimationItemViewModel, EstimationItem>(model);
           // await fillEntityIdentity(entityResult);
            result = await manger.UpdateAsync(model);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            var entity = await manger.GetAsync(id);
           // await fillEntityIdentity(entity);
            result = await manger.SoftDeleteAsync(entity);
            return Ok(result);
        }
        #endregion
        #endregion

        #region Buisness
        [Route("SearchItems/{key}")]
        [HttpGet("{key}")]
        public async Task<EstimationItemViewModel> SearchItems([FromRoute]string key)
        {
            EstimationItemViewModel result = null;

            string authHeader = Helper.GetValueFromRequestHeader(Request);
            if (authHeader != null)
            {
                var item =await SearchInInventory(key, authHeader);
                if (item != null)
                {
                    result = mapper.Map<ItemViewModel, EstimationItemViewModel>(item);
                }
                else
                {
                    var employee = await SearchInEmployees(key,authHeader);
                    if (employee != null)
                    {
                        result = mapper.Map<EmployeeSalaryViewModel, EstimationItemViewModel>(employee);
                    }
                }
            }
            return result;
        }
        private Task<ItemViewModel> SearchInInventory(string code,string authHeader)
        {
           return inventoryService.GetItemByCode(code, authHeader);
        }
        private Task<EmployeeSalaryViewModel> SearchInEmployees(string employeeId, string authHeader)
        {
            return employeeService.GetEmployeeByEmployeeId(employeeId, authHeader);
        }
        #endregion
    }
}
