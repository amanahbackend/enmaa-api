﻿using AutoMapper;
using DispatchProduct.Estimating.Quoting.API.ServicesCommunication.Call;

using DispatchProduct.Estimating.Quoting.BLL.IManagers;
using DispatchProduct.Estimating.Quoting.BLL.ViewModel;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Estimating.Quoting.Settings;
using DispatchProduct.Quoting.Hubs;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilities.Utilites;
using Utilities.Utilites.Paging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Estimating.Quoting.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class QuotationController : Controller
    {
        private IQuotationManager manger;
        ICallService callService;
        private readonly IMapper mapper;
        private EstimationController estimationController;
        private IQuotationHub hub;
        private IHubContext<QuotationHub> quotationHub;
        private EstimatingQuotingAppSettings settings;
        private IHostingEnvironment env;

        public QuotationController(IQuotationManager _manger, ICallService _callService, IMapper _mapper, 
            EstimationController _estimationController, IQuotationHub _hub, IHubContext<QuotationHub> _quotationHub,
            IOptions<EstimatingQuotingAppSettings> _settings, IHostingEnvironment _env)
        {
            manger = _manger;
            callService = _callService;
            mapper = _mapper;
            estimationController = _estimationController;
            settings = _settings.Value;
            quotationHub = _quotationHub;
            hub = _hub;
            env = _env;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        [Route("Get/{id}")]

        public async Task<QuotationViewModel> Get([FromRoute]int id, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var result = await manger.GetAsync(id);
            if (result.EstimationRefNumber != null)
            {
                var estimation = await estimationController.GetByRef(result.EstimationRefNumber, authHeader);
                if (estimation != null && estimation.Call != null)
                {
                    result.Estimation = estimation;
                    mapper.Map(estimation.Call, result);
                }
            }
            return result;
        }
        private async Task<EstimationViewModel> GetEstimationByRef(string estimationRefNumber, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var estimation = await estimationController.GetByRef(estimationRefNumber, authHeader);
            return estimation;
        }

        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> Get(string authHeader = null)
        {
            var entityResult =await manger.GetAllAsync();
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            //var result = mapper.Map<List<Quotation>, List<QuotationViewModel>>(entityResult);
            List<QuotationViewModel> quotation = await BindEstimationToQuotation(entityResult);
            return Ok(quotation);
        }

        [Route("GetAllByPaging")]
        [HttpPost]
        public async Task<IActionResult> GetAllByPaging([FromBody]PaginatedItemsViewModel pagingParameterModel)
        {
            int totalNumbers;
            string authHeader = null;
            var entityResult =await manger.GetAllQuotationFilledPropsByPaging(pagingParameterModel);
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            entityResult.Result = await BindEstimationToQuotation(entityResult.Result);


            return Ok(entityResult);
        }

        [HttpGet("{id}")]
        [Route("GetByRef/{refNo}")]
        public async Task<IActionResult> GetByRef([FromRoute]string refNo, string authHeader = null)
        {
            var result = await manger.GetByRefNumberAsync(refNo);
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            if (result.EstimationRefNumber != null)
            {
                var estimation = await estimationController.GetByRef(result.EstimationRefNumber, authHeader);
                result.Estimation = estimation;
                if (estimation.Call != null)
                {
                    mapper.Map(estimation.Call, result);
                }
            }
            return Ok(result);
        }
        #endregion

        #region Search
        [HttpPost]
        [Route("Filter")]
        public async Task<IActionResult> Filter([FromBody] FilterQuotationViewModel model, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            List<QuotationViewModel> quotationViewModelList = null;
            FilterQuotation filter = mapper.Map<FilterQuotationViewModel, FilterQuotation>(model);
            if (filter != null)
            {
                var callers = await callService.SearchCallByNameOrNumber(filter.SearchKey, authHeader);
                filter.CallerIds = callers.Select(x => x.Id).ToList();
                var source = await manger.FilterQuotationAsync(filter);
                
                quotationViewModelList = await BindEstimationToQuotation(source);
            }
            return Ok(quotationViewModelList);
        }




        [HttpPost]
        [Route("FilterByPaging")]
        public async Task<IActionResult> FilterByPaging([FromBody] FilterQuotationViewModel model, string authHeader = null)
        {
            PagedResult<QuotationViewModel> pasgedresult = new PagedResult<QuotationViewModel>();
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            List<QuotationViewModel> quotationViewModelList = null;
            FilterQuotation filter = mapper.Map<FilterQuotationViewModel, FilterQuotation>(model);
            if (filter != null)
            {
                var callers =new List<CallViewModel>();

                if (filter.SearchKey !=null)
                 callers = await callService.SearchCallByNameOrNumber(filter.SearchKey, authHeader);
                if(callers!=null)
                filter.CallerIds = callers.Select(x => x.Id).ToList();

                pasgedresult = await manger.FilterQuotationWithPagingAsync(filter, model.paginatedItemsViewModel);
                quotationViewModelList = await BindEstimationToQuotation(pasgedresult.Result);
            }
            pasgedresult.Result = quotationViewModelList;
            return Ok(pasgedresult);
        }
        [HttpGet("key")]
        [Route("Search/{key}")]
        public async Task<List<QuotationViewModel>> Search([FromRoute]string key, string authHeader = null)
        {
            List<QuotationViewModel> entityResult = null;
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            List<QuotationViewModel> result = new List<QuotationViewModel>();
            entityResult = await manger.SearchAsync(key);
            if (entityResult == null || entityResult.Count == 0)
            {
                var estLst = await estimationController.SearchInCall(key, authHeader);
                if (estLst != null)
                {
                    List<string> estListNo = estLst.Select(r => r.RefNumber).ToList();
                    entityResult =await  manger.GetByEstmRefNumberAsync(estListNo);
                }
            }
            return await BindEstimationToQuotation(entityResult);
        }
        #endregion
        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]QuotationViewModel model)
        {
            // await fillEntityIdentity(entityResult);
           var model2 = await manger.AddAsync(model);
             
  
            return Ok(model2);
        }
        #endregion
        [Route("GenerateWord/{quotationId}")]
        [HttpGet]
        public async Task<ActionResult> GenerateWord([FromRoute] int quotationId)
        {
            QuotationViewModel quot = await Get(quotationId);
            if (quot == null)
                return BadRequest("This Quotation Not Exist");
            if (quot.Call == null || quot.Estimation == null)
                return BadRequest("This Quotation Not Have Estimation Items");
            string bindedDoc = GetBindedDoc(quot);
            return Ok(bindedDoc);
        }
        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]QuotationViewModel model)
        {
            bool result = false;
            // await fillEntityIdentity(entityResult);
            result = await manger.UpdateAsync(model);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            var entity =await manger.GetAsync(id);
            // await fillEntityIdentity(entity);
            if (entity != null)
                result = await manger.SoftDeleteAsync(entity);
            return Ok(result);
        }
        #endregion
        public async Task<List<QuotationViewModel>> BindEstimationToQuotation(List<QuotationViewModel> result, string authHeader = null)
        {
            if (Request != null && authHeader == null)
                authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
            for (int i = 0; i < result.Count; ++i)
            {
                if (result[i].EstimationRefNumber != null)
                {
                    EstimationViewModel byRef = await estimationController.GetByRef(result[i].EstimationRefNumber, authHeader);
                    if (byRef?.Call != null)
                        result[i] = mapper.Map(byRef.Call, result[i]);
                }
            }
            return result;
        }

        public Table AddTable(List<EstimationItemViewModel> data)
        {
            Table table = new Table();
            OpenXmlElement[] openXmlElementArray1 = new OpenXmlElement[1];
            int index1 = 0;
            OpenXmlElement[] openXmlElementArray2 = new OpenXmlElement[6];
            int index2 = 0;
            TopBorder topBorder = new TopBorder();
            topBorder.Val = new EnumValue<BorderValues>(BorderValues.Single);
            topBorder.Size = 12U;
            openXmlElementArray2[index2] = topBorder;
            int index3 = 1;
            BottomBorder bottomBorder = new BottomBorder();
            bottomBorder.Val = new EnumValue<BorderValues>(BorderValues.Single);
            bottomBorder.Size = 12U;
            openXmlElementArray2[index3] = bottomBorder;
            int index4 = 2;
            LeftBorder leftBorder = new LeftBorder();
            leftBorder.Val = new EnumValue<BorderValues>(BorderValues.Single);
            leftBorder.Size = 12U;
            openXmlElementArray2[index4] = leftBorder;
            int index5 = 3;
            RightBorder rightBorder = new RightBorder();
            rightBorder.Val = new EnumValue<BorderValues>(BorderValues.Single);
            rightBorder.Size = 12U;
            openXmlElementArray2[index5] = rightBorder;
            int index6 = 4;
            InsideHorizontalBorder horizontalBorder = new InsideHorizontalBorder();
            horizontalBorder.Val = new EnumValue<BorderValues>(BorderValues.Single);
            horizontalBorder.Size = 12U;
            openXmlElementArray2[index6] = horizontalBorder;
            int index7 = 5;
            InsideVerticalBorder insideVerticalBorder = new InsideVerticalBorder();
            insideVerticalBorder.Val = new EnumValue<BorderValues>(BorderValues.Single);
            insideVerticalBorder.Size = 12U;
            openXmlElementArray2[index7] = insideVerticalBorder;
            TableBorders tableBorders = new TableBorders(openXmlElementArray2);
            openXmlElementArray1[index1] = tableBorders;
            TableProperties newChild = new TableProperties(openXmlElementArray1);
            table.AppendChild<TableProperties>(newChild);
            TableRow row1 = CreateRow(new List<TableCell>()
      {
        CreateCell("Item Number"),
        CreateCell("Name"),
        CreateCell("Quantity")
      });
            table.Append(new OpenXmlElement[1]
            {
                row1
            });
            foreach (EstimationItemViewModel estimationItemViewModel in data)
            {
                TableRow row2 = CreateRow(new List<TableCell>()
        {
          CreateCell(estimationItemViewModel.No.ToString()),
          CreateCell(estimationItemViewModel.Name.ToString()),
          CreateCell(estimationItemViewModel.Quantity.ToString())
        });
                table.Append(new OpenXmlElement[1]
                {
           row2
                });
            }
            return table;
        }

        public TableCell CreateCell(string text)
        {
            TableCell tableCell1 = new TableCell();
            tableCell1.Append(new OpenXmlElement[1]
            {
         new Paragraph(new OpenXmlElement[1]
        {
           new Run(new OpenXmlElement[1]
          {
             new Text(text)
          })
        })
            });
            TableCell tableCell2 = tableCell1;
            OpenXmlElement[] openXmlElementArray1 = new OpenXmlElement[1];
            int index1 = 0;
            OpenXmlElement[] openXmlElementArray2 = new OpenXmlElement[1];
            int index2 = 0;
            TableCellWidth tableCellWidth = new TableCellWidth();
            tableCellWidth.Type = (EnumValue<TableWidthUnitValues>)TableWidthUnitValues.Auto;
            openXmlElementArray2[index2] = tableCellWidth;
            TableCellProperties tableCellProperties = new TableCellProperties(openXmlElementArray2);
            openXmlElementArray1[index1] = tableCellProperties;
            tableCell2.Append(openXmlElementArray1);
            return tableCell1;
        }

        public TableRow CreateRow(List<TableCell> cells)
        {
            TableRow tableRow = new TableRow();
            foreach (TableCell cell in cells)
                tableRow.Append(new OpenXmlElement[1]
                {
          cell.CloneNode(true)
                });
            return tableRow;
        }

        public string GetSrcQuotationDocPath()
        {
            string str = (string)null;
            if (string.IsNullOrWhiteSpace(env.WebRootPath))
                env.WebRootPath = Directory.GetCurrentDirectory();
            string webRootPath = env.WebRootPath;
            if (!string.IsNullOrEmpty(settings.QuotationDocSrcPath))
                str = Path.Combine(webRootPath, settings.QuotationDocSrcPath);
            return str;
        }

        public string GetDstQuotationDocPath()
        {
            string str = null;
            if (string.IsNullOrWhiteSpace(env.WebRootPath))
                env.WebRootPath = Directory.GetCurrentDirectory();
            string webRootPath = env.WebRootPath;
            if (!string.IsNullOrEmpty(settings.QuotationDocDstPath))
                str = Path.Combine(webRootPath, settings.QuotationDocDstPath);
            return str;
        }

        public string GetBindedDoc(QuotationViewModel quot)
        {
            string str1 = null;
            Dictionary<string, string> dataFromQuotation = ExtractWordDataFromQuotation(quot);
            if (dataFromQuotation != null && dataFromQuotation.Count > 0)
            {
                string quotationDocPath1 = GetSrcQuotationDocPath();
                string quotationDocPath2 = GetDstQuotationDocPath();
                if (!string.IsNullOrEmpty(quotationDocPath1) && !string.IsNullOrEmpty(settings.QuotationDocSrcPath))
                {
                    string str2 = quot.Call.CallerName + settings.QuotationDocDstSuffix;
                    DocHelper.CopyFile(quotationDocPath1, settings.QuotationDocSrcName, quotationDocPath2, str2);
                    DocHelper.SearchAndReplace(Path.Combine(quotationDocPath2, str2), dataFromQuotation);
                    var path = $"{settings.QuotationDocDstPath}/{str2}";
                    str1 = GetWordDoc(path);
                }
            }
            return str1;
        }

        private Dictionary<string, string> ExtractWordDataFromQuotation(QuotationViewModel quot)
        {
            Dictionary<string, string> dictionary = (Dictionary<string, string>)null;
            if (quot != null && quot.Call != null)
            {
                dictionary = new Dictionary<string, string>();
                dictionary.Add("QuotationNoValue", quot.Call.CallerName != null ? quot.RefNumber : "");
                dictionary.Add("CustomerNameValue", quot.Call.CallerName != null ? quot.Call.CallerName : "");
                dictionary.Add("PACIValue", quot.Call.PACINumber != null ? quot.Call.PACINumber : "");
                dictionary.Add("GovernmentValue", quot.Call.Governorate != null ? quot.Call.Governorate : "");
                dictionary.Add("CityValue", quot.Call.Area != null ? quot.Call.Area : "");
                dictionary.Add("BlockValue", quot.Call.Block != null ? quot.Call.Block : "");
                dictionary.Add("StreetValue", quot.Call.Street != null ? quot.Call.Street : "");
                dictionary.Add("CallDescriptionValue", quot.Call.CustomerDescription != null ? quot.Call.CustomerDescription : "");
                dictionary.Add("PriceValue", quot.Price.ToString());
            }
            return dictionary;
        }

        public void AddEstimationItemsToWordDoc(QuotationViewModel quotation, string dstFilePath)
        {
            if (quotation == null || quotation.Estimation == null || (quotation.Estimation.lstEstimationItems == null || quotation.Estimation.lstEstimationItems.Count <= 0))
                return;
            using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open(dstFilePath, true))
            {
                wordprocessingDocument.MainDocumentPart.Document.Body.Append(new OpenXmlElement[1]
                {
           AddTable(quotation.Estimation.lstEstimationItems)
                });
                wordprocessingDocument.MainDocumentPart.Document.Save();
            }
        }

        private string GetWordDoc(string file)
        {
            string result = null;
            string str = $"{Request.Scheme}://{Request.Host}/{Request.PathBase}";
            file = file.Replace('\\', '/');
            result = $"{str}/{file}";
            return result;
        }
        #endregion
    }
}
