﻿
using DispatchProduct.HttpClient;

namespace DispatchProduct.Estimating.Quoting.API.Settings
{
    public class InventoryServiceSettings : DefaultHttpClientSettings
    {
        public override string Uri { get; set; }

        public string GetByCodeVerb { get; set; }
    }
}
