﻿using DispatchProduct.Estimating.Quoting.BLL.ViewModel;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.BLL.IManagers
{
    public interface IEstimationItemManager : IBaseManager<EstimationItemViewModel, EstimationItem>
    {
        Task<List<EstimationItemViewModel>> GetByEstimationIdAsync(int estimationId);
        Task<EstimationItemViewModel> GetAsync(int id);

    }
}
