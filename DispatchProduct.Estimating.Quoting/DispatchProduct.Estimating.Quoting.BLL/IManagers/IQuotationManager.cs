﻿using DispatchProduct.Estimating.Quoting.BLL.ViewModel;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Repoistry;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace DispatchProduct.Estimating.Quoting.BLL.IManagers
{
    public interface IQuotationManager : IBaseManager<QuotationViewModel, Quotation>
    {
        Task<QuotationViewModel> GetByRefNumberAsync(string refNo);
        Task<PagedResult<QuotationViewModel>> GetAllQuotationFilledPropsByPaging(PaginatedItemsViewModel pagingParameterModel);

        Task<QuotationViewModel> GetByEstmRefNumberAsync(string refNo);

        Task<QuotationViewModel> GetAsync(int id);

        Task<List<QuotationViewModel>> SearchAsync(string key);

        Task<List<QuotationViewModel>> GetByEstmRefNumberAsync(List<string> estimationRefNos);

      Task<List<QuotationViewModel>> FilterQuotationAsync(FilterQuotation obj);
        Task<PagedResult<QuotationViewModel>> FilterQuotationWithPagingAsync(FilterQuotation obj, PaginatedItemsViewModel paginatedItemsViewModel);

    }
}
