﻿
using DispatchProduct.Estimating.Quoting.BLL.ViewModel;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.BLL.IManagers
{
    public interface IEstimationManager : IBaseManager<EstimationViewModel, Estimation>
    {
        Task<EstimationViewModel> GetAsync(int id);
        Task<bool> DeleteAsync(int id);
        Task<List<EstimationViewModel>> GetByCallAsync(int callId);

        Task<EstimationViewModel> GetByRefNumberAsync(string refNo);

        Task<bool> UpdateHasQuotationAsync(EstimationViewModel entity);
        Task<List<EstimationViewModel>> SearchAsync(string key);

        Task<List<EstimationViewModel>> FilterEstimationAsync(FilterEstimation filter);
    }
}
