﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DispatchProduct.Estimating.Quoting.BLL.IManagers;
using DispatchProduct.Estimating.Quoting.BLL.ViewModel;
using DispatchProduct.Estimating.Quoting.Context;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Estimating.Quoting.Settings;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilites;

namespace DispatchProduct.Estimating.Quoting.BLL.Managers
{
    public class EstimationManager : BaseManager<EstimationViewModel, Estimation>, IEstimationManager
    {
        IEstimationItemManager estimationItemManager;
        EstimatingQuotingAppSettings estQuot;
        public EstimationManager(EstimationQuotationDbContext context, IMapper _mapper, IEstimationItemManager _estimationItemManager, IRepositry<Estimation> _repository
          , IOptions<EstimatingQuotingAppSettings> _estQuot)
            : base(context, _repository, _mapper)
        {
            estimationItemManager = _estimationItemManager;
            estQuot = _estQuot.Value;
        }
        public override async Task<EstimationViewModel> AddAsync(EstimationViewModel entity)
        {
            // var estEntity = mapper.Map<EstimationViewModel, Estimation>(entity);

            entity.RefNumber =await CreateRefNumberAsync();

            var result = await Task.Run(() => base.AddAsync(entity));

            return result;
        }
        public override async Task<bool> UpdateAsync(EstimationViewModel entity)
        {
            bool result = false;

            var estEntity = mapper.Map<EstimationViewModel, Estimation>(entity);

            await DeleteRelatedItemstoEstimationAsync(estEntity.Id);
          
            result = await Task.Run(() => repository.Update(estEntity));

            return result;
        }
        public async Task<bool> UpdateHasQuotationAsync(EstimationViewModel entity)
        {
            bool result = false;
            var estEntity = mapper.Map<EstimationViewModel, Estimation>(entity);

            result = await Task.Run(() => repository.Update(estEntity));

            return result;
        }
        public async Task<List<EstimationViewModel>> GetByCallAsync(int callId)
        {
            List<EstimationViewModel> result = null;

           result = await Task.Run(() =>
            {
               return repository.GetAll().ProjectTo<EstimationViewModel>().Where(cal => cal.FK_Call_Id == callId).ToList();

            });
            return result;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            await DeleteRelatedItemstoEstimationAsync(id);
            return await base.DeleteByIdAsync(id);
        }
        private async Task DeleteRelatedItemstoEstimationAsync(int id)
        {
            var lstEstitms =await estimationItemManager.GetByEstimationIdAsync(id);
            if (lstEstitms.Count > 0)
            {
                await estimationItemManager.DeleteAsync(lstEstitms);
            }
        }
        public async Task<EstimationViewModel> GetAsync(int id)
        {
            Estimation result = null;
            result = await Task.Run(() => repository.Get(id));
            var estEntity = mapper.Map<Estimation, EstimationViewModel>(result);

            return estEntity;
        }

        //private Estimation AddRefrenceNumber(Estimation entity)
        //{
        //    if (entity != null && entity.Id > 0)
        //    {
        //        entity.RefNumber = estQuot.EstimationPrefix + entity.Id;
        //        base.Update(entity);
        //    }
        //    return entity;
        //}


        public async Task<List<EstimationViewModel>> FilterEstimationAsync(FilterEstimation obj)
        {
            #region Commented
            //var estimations = GetAll()
            //    .Where(est =>
            //    ((obj.RefNumbers != null && obj.RefNumbers.Count > 0 ? obj.RefNumbers.Contains(est.RefNumber) : true)
            //    ||
            //    (obj.RefNumbers != null && obj.RefNumbers.Count > 0 ? obj.RefNumbers.Contains(est.Area) : true))


            //    &&
            //    ((obj.QuotationRefNumbers != null && obj.QuotationRefNumbers.Count > 0 ? obj.QuotationRefNumbers.Contains(est.QuotationRefNumber) : true)
            //    ||
            //    (obj.QuotationRefNumbers != null && obj.QuotationRefNumbers.Count > 0 ? obj.QuotationRefNumbers.Contains(est.Area) : true))


            //    &&
            //    (obj.HasQuotation != null ? est.HasQuotation == obj.HasQuotation : true)


            //    &&
            //    (obj.Area != null ? est.Area.ToLower().Contains(obj.Area) : true)

            //    &&
            //    (obj.PriceFrom != null ? est.Price >= obj.PriceFrom : true)

            //    &&
            //    (obj.PriceTo != null ? est.Price <= obj.PriceTo : true)

            //    &&
            //    (obj.FromDate != null ? est.CreatedDate >= obj.FromDate : true)

            //    &&
            //    (obj.ToDate != null ? est.CreatedDate <= obj.ToDate : true))

            //    //&&
            //    //(obj.SearchText != null ? est.Area.ToLower().Contains(obj.SearchText) : true)

            //    .ToList();

            #endregion

            List<EstimationViewModel> estimations;

            if (obj.FromDate != null && obj.ToDate != null)
            {
                estimations = await Task.Run(() =>
                {
                   return repository.GetAll().ProjectTo<EstimationViewModel>().Where(est => obj.CallerIds.Contains(est.FK_Call_Id) && est.CreatedDate >= obj.FromDate && est.CreatedDate <= obj.ToDate).ToList();
                });
            }
            else
            {
                estimations = await Task.Run(() =>
                {

                   return repository.GetAll().ProjectTo<EstimationViewModel>().Where(est => obj.CallerIds.Contains(est.FK_Call_Id)).ToList();

                });
            }
            return estimations;
        }
        public async Task<List<EstimationViewModel>> SearchAsync(string key)
        {
            return await Task.Run(() =>
            {
             return   repository.GetAll().ProjectTo<EstimationViewModel>().Where(r => r.RefNumber == key || r.Area == key).ToList();
            });

        }
        public async Task<EstimationViewModel> GetByRefNumberAsync(string refNo)
        {
            return await Task.Run(() => repository.GetAll().ProjectTo<EstimationViewModel>().FirstOrDefault(est => est.RefNumber == refNo));
        }
 
       
        private async Task<int?> GetMaxOrderTodayAsync(string date)
        {
            int? result = null;
            var lstEst = await Task.Run(() =>
            {
              return repository.GetAll().Where(refNo => refNo.RefNumber.StartsWith(estQuot.EstimationPrefix + date)).Select(e => e.RefNumber.Replace(estQuot.EstimationPrefix + date, "")).ToList();
            });
            if (lstEst != null && lstEst.Count > 0)
            {
                result = lstEst.Select(str => int.Parse(str)).Max();
            }
            return result;
        }
        private async Task<string> CreateRefNumberAsync()
        {
            string result = null;
            string orderNo = estQuot.EstimationOrderDayStartNo;
            string orderdate = Helper.CreateInfixDateCode();
            int? max =await  GetMaxOrderTodayAsync(orderdate);
            if (max != null)
            {
                orderNo = (max.Value + 1).ToString();
            }
            result = estQuot.EstimationPrefix + orderdate + orderNo;
            return result;
        }

      
    }
}
