﻿using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Estimating.Quoting.BLL.IManagers;
using DispatchProduct.Estimating.Quoting.Context;
using Microsoft.Extensions.Options;
using DispatchProduct.Estimating.Quoting.Settings;
using System.Linq;
using Utilites;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using DispatchProduct.Estimating.Quoting.BLL.ViewModel;
using AutoMapper;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;

namespace DispatchProduct.Estimating.Quoting.BLL.Managers
{
    public class QuotationManager : BaseManager<QuotationViewModel, Quotation>, IQuotationManager
    {
        EstimatingQuotingAppSettings estQuot;
        IEstimationManager estimationManager;
        public QuotationManager(EstimationQuotationDbContext context, IMapper _mapper,IRepositry<Quotation> _repository ,
            IOptions<EstimatingQuotingAppSettings> _estQuot, IEstimationManager _estimationManager)
            : base(context, _repository, _mapper)
        {
            estQuot = _estQuot.Value;
            estimationManager = _estimationManager;
        }

        public override async Task<QuotationViewModel> AddAsync(QuotationViewModel entity)
        {
            entity.RefNumber = await CreateRefNumberAsync();
            EstimationViewModel estimation = null;
            if (entity.EstimationRefNumber != null)
            {
                estimation =await estimationManager.GetByRefNumberAsync(entity.EstimationRefNumber);
                entity.Area = estimation.Area;
            }
            entity =await Task.Run(() =>
            {
               return base.AddAsync(entity);
            });
            if (estimation != null)
            {
                estimation = await estimationManager.GetByRefNumberAsync(entity.EstimationRefNumber);
                estimation.HasQuotation = true;
                estimation.QuotationRefNumber = entity.RefNumber;
                await estimationManager.UpdateHasQuotationAsync(estimation);
                entity.Estimation = estimation;
            }
            return entity;
        }
        //public override bool Update(Quotation entity)
        //{
        //    bool result = false;
        //    Estimation estimation = null;
        //    var quotation=Get(entity.Id);
        //     estimation = estimationManager.GetByRefNumber(quotation.EstimationRefNumber);
        //    if (estimation != null && estimation.RefNumber != entity.EstimationRefNumber)
        //    {
        //        estimation.HasQuotation = false;
        //        estimationManager.UpdateHasQuotation(estimation);
        //        var newEstimation = estimationManager.GetByRefNumber(quotation.EstimationRefNumber);
        //        newEstimation.HasQuotation = true;
        //        estimationManager.UpdateHasQuotation(newEstimation);
        //    }
        //    result=base.Update(entity);
        //    return result;
        //}

        public Task<PagedResult<QuotationViewModel>> GetAllQuotationFilledPropsByPaging(PaginatedItemsViewModel pagingParameterModel)
        {
            var quotationQuery = repository.GetAll();

            return GetAllByPaginationAsync(quotationQuery, pagingParameterModel);
        }

        public async Task<List<QuotationViewModel>> SearchAsync(string key)
        {
            return await Task.Run(() => repository.GetAll().ProjectTo<QuotationViewModel>().Where(r => r.RefNumber == key || r.Area == key).ToList());
        }
        public override async Task<bool> DeleteAsync(QuotationViewModel entity)
        {
            bool result = false;

            //if (entity.EstimationRefNumber != null)
            //{
            //var estimation = estimationManager.GetByRefNumber(entity.EstimationRefNumber);
            if (entity.Estimation != null)
            {
                entity.Estimation.HasQuotation = false;
                entity.Estimation.QuotationRefNumber = null;
               // var estEntity = mapper.Map<Estimation, EstimationViewModel>(entity.Estimation);

                await estimationManager.UpdateHasQuotationAsync(entity.Estimation);
            }
            //}
            result = await base.SoftDeleteAsync(entity);
            return result;
        }
        private async Task<string> CreateRefNumberAsync()
        {
            string result = null;
            string orderNo = estQuot.EstimationOrderDayStartNo;
            string orderdate = Helper.CreateInfixDateCode();
            int? max =await GetMaxOrderTodayAsync(orderdate);
            if (max != null)
            {
                orderNo = (max.Value + 1).ToString();
            }
            result = estQuot.EstimationPrefix + orderdate + orderNo;
            return result;
        }
        private async Task<int?> GetMaxOrderTodayAsync(string date)
        {
            int? result = null;
            var lstEst =await Task.Run(()=>{
               return repository.GetAll().Where(refNo => refNo.RefNumber.StartsWith(estQuot.EstimationPrefix + date)).Select(e => e.RefNumber.Replace(estQuot.EstimationPrefix + date, "")).ToList();
            });
            if (lstEst != null && lstEst.Count > 0)
            {
                result = lstEst.Select(str => int.Parse(str)).Max();
            }
            return result;
        }
        public async Task<QuotationViewModel> GetAsync(int id)
        {
            var quotation = await (context as EstimationQuotationDbContext).Quotation.Where(x => x.Id==id).FirstOrDefaultAsync(); 

           var estEntity = mapper.Map<Quotation, QuotationViewModel>(quotation);

            if (quotation != null && quotation.EstimationRefNumber != null)
                estEntity.Estimation = await  estimationManager.GetByRefNumberAsync(estEntity.EstimationRefNumber);
            return estEntity;
        }
        public async Task<QuotationViewModel> GetByRefNumberAsync(string refNo)
        {
            return await Task.Run(() => repository.GetAll().ProjectTo<QuotationViewModel>().FirstOrDefault(quot => quot.RefNumber == refNo));
          
        }
        public async Task<QuotationViewModel> GetByEstmRefNumberAsync(string refNo)
        {
            return await Task.Run(() => repository.GetAll().ProjectTo<QuotationViewModel>().FirstOrDefault(quot => quot.EstimationRefNumber == refNo));
        }
        public async Task<List<QuotationViewModel>> FilterQuotationAsync(FilterQuotation obj)
        {
            #region Commented
            // return GetAll().Where(est =>
            // (
            // (filter.RefNumbers != null && filter.RefNumbers.Count > 0 ? filter.RefNumbers.Contains(est.RefNumber) : true)
            // ||
            // (filter.RefNumbers != null && filter.RefNumbers.Count > 0 ? filter.RefNumbers.Contains(est.Area) : true)
            // )
            // &&
            // (
            // (filter.EstimationRefNumbers != null && filter.EstimationRefNumbers.Count > 0 ? filter.EstimationRefNumbers.Contains(est.EstimationRefNumber) : true)
            // ||
            // (filter.EstimationRefNumbers != null && filter.EstimationRefNumbers.Count > 0 ? filter.EstimationRefNumbers.Contains(est.Area) : true)
            // )
            // &&
            // (filter.PriceFrom != null ? est.Price >= filter.PriceFrom : true)
            // &&
            // (filter.Area != null ? est.Area.ToLower().Contains(filter.Area) : true)
            // &&
            // (filter.PriceTo != null ? est.Price <= filter.PriceTo : true)
            // &&
            // (filter.FromDate != null ? est.CreatedDate >= filter.FromDate : true)
            //// &&
            ////(filter.SearchText != null ? est.Area.ToLower().Contains(filter.SearchText) : true)
            // &&
            // (filter.ToDate != null ? est.CreatedDate <= filter.ToDate : true)
            // ).ToList();
            #endregion

            List<QuotationViewModel> quotations = new List<QuotationViewModel>();

            var estimations =await  estimationManager.FilterEstimationAsync(new FilterEstimation
            {
                CallerIds = obj.CallerIds,
                FromDate = obj.FromDate,
                SearchKey = obj.SearchKey,
                ToDate = obj.ToDate
            });

            if (estimations != null && estimations.Count > 0)
            {
                var estRefsNumber = estimations.Select(x => x.RefNumber).ToList();
                if (obj.FromDate != null && obj.ToDate != null)
                {
                    quotations = await Task.Run(() => repository.GetAll().Where(quo => estRefsNumber.Contains(quo.EstimationRefNumber) && quo.CreatedDate >= obj.FromDate && quo.CreatedDate <= obj.ToDate).ProjectTo<QuotationViewModel>().ToList());
                }
                else
                {

                    quotations =await Task.Run(() => repository.GetAll().Where(quo => estRefsNumber.Contains(quo.EstimationRefNumber)).ProjectTo<QuotationViewModel>().ToList());

                }
            }

            return quotations;

        }


        public async Task<PagedResult<QuotationViewModel>> FilterQuotationWithPagingAsync(FilterQuotation obj, PaginatedItemsViewModel paginatedItemsViewModel)
        {
            var qutationQueruy = repository.GetAll().AsQueryable();

            var estimations = await estimationManager.FilterEstimationAsync(new FilterEstimation
            {
                CallerIds = obj.CallerIds,
                FromDate = obj.FromDate,
                SearchKey = obj.SearchKey,
                ToDate = obj.ToDate
            });

            if (estimations != null && estimations.Count > 0)
            {
                var estRefsNumber = estimations.Select(x => x.RefNumber).ToList();
                if (obj.FromDate != null && obj.ToDate != null)
                {
                     qutationQueruy = qutationQueruy.Where(quo => estRefsNumber.Contains(quo.EstimationRefNumber) && quo.CreatedDate >= obj.FromDate && quo.CreatedDate <= obj.ToDate).AsQueryable();
                }
                else
                {
                     qutationQueruy = qutationQueruy.Where(quo => estRefsNumber.Contains(quo.EstimationRefNumber)).AsQueryable();
                }
            }

           var quotations = await base.GetAllByPaginationAsync(qutationQueruy, paginatedItemsViewModel);

        
            return quotations;
        }
        public async Task<List<QuotationViewModel>> GetByEstmRefNumberAsync(List<string> estimationRefNos)
        {
            List<QuotationViewModel> result = null;
            if (estimationRefNos != null && estimationRefNos.Count > 0)
                result = await Task.Run(() => repository.GetAll().Where(quot => estimationRefNos.Contains(quot.EstimationRefNumber)).ProjectTo<QuotationViewModel>().ToList());

            return result;
        }
    }
}
