﻿using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.Estimating.Quoting.Context;
using DispatchProduct.Estimating.Quoting.BLL.IManagers;
using DispatchProduct.Estimating.Quoting.Entities;
using System.Linq;
using DispatchProduct.Estimating.Quoting.BLL.ViewModel;
using AutoMapper;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;

namespace DispatchProduct.Estimating.Quoting.BLL.Managers
{
    public class EstimationItemManager :BaseManager<EstimationItemViewModel,EstimationItem> , IEstimationItemManager
    {

        public EstimationItemManager(EstimationQuotationDbContext context , IMapper _mapper, IRepositry<EstimationItem> _repository)
            : base(context,_repository, _mapper)
        {
        
        }
        public async Task<List<EstimationItemViewModel>> GetByEstimationIdAsync(int estimationId)
        {
             return await Task.Run(() => repository.GetAll().ProjectTo<EstimationItemViewModel>().Where(est => est.FK_Estimation_Id == estimationId).ToList());
        }
        public async Task<EstimationItemViewModel> GetAsync(int id)
        {
            EstimationItem result = null;
            result = await Task.Run(() => repository.Get(id));
            var estEntity = mapper.Map<EstimationItem, EstimationItemViewModel>(result);

            return estEntity;
        }
    }
}
