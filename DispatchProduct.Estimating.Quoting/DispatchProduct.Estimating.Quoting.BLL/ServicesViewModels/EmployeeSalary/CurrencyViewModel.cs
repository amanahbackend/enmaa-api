﻿
using DispatchProduct.Estimating.Quoting.BLL.ViewModel;

namespace DispatchProduct.Estimating.Quoting.BLL.ServicesViewModels
{
    public class CurrencyViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
