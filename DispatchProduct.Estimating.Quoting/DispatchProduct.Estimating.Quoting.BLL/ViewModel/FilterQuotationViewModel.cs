﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

namespace DispatchProduct.Estimating.Quoting.BLL.ViewModel
{
    public class FilterQuotationViewModel
    {
        public string SearchKey { get; set; }


        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public List<int> CallerIds { get; set; }

        public PaginatedItemsViewModel paginatedItemsViewModel { get; set; }

        //public List<string> RefNumbers { get; set; }

        //public List<string> EstimationRefNumbers { get; set; }

        //public double? PriceFrom { get; set; }

        //public double? PriceTo { get; set; }

        //public string Area { get; set; }

        //public string SearchText { get; set; }

    }
}
