﻿using System;
using System.Collections.Generic;

namespace DispatchProduct.Estimating.Quoting.Entities
{
    public class FilterEstimation
    {
        public string SearchKey { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public List<int> CallerIds { get; set; }

        //public List<string> RefNumbers { get; set; }

        //public List<string> QuotationRefNumbers { get; set; }

        //public double? PriceFrom { get; set; }

        //public double? PriceTo { get; set; }

        //public bool? HasQuotation { get; set; }

        //public string Area { get; set; }

        //public string SearchText { get; set; }

    }
}