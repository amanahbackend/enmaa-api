﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Estimating.Quoting.IEntities
{
    public interface IQuotation : IBaseEntity
    {
        int Id { get; set; }

        string RefNumber { get; set; }

        string EstimationRefNumber { get; set; }

        double Price { get; set; }
    }
}
