﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Estimating.Quoting.Entities;

namespace DispatchProduct.Estimating.Quoting.IEntities
{
    public interface IEstimationItem : IBaseEntity
    {
        int Id { get; set; }

        int FK_Estimation_Id { get; set; }

        string No { get; set; }

        string Name { get; set; }

        double Quantity { get; set; }

        double UnitPrice { get; set; }

        double TotalPrice { get; set; }

        double Margin { get; set; }

        double TotalMarginPrice { get; set; }

        Estimation Estimation { get; set; }
    }
}
