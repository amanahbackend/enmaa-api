﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Estimating.Quoting.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Estimating.Quoting.EntityConfigurations
{
    public class EstimationEntityTypeConfiguration : BaseEntityTypeConfiguration<Estimation>, IEntityTypeConfiguration<Estimation> 
    {
        public void Configure(EntityTypeBuilder<Estimation> EstimationConfiguration)
        {
            EstimationConfiguration.ToTable("Estimation");

            EstimationConfiguration.HasKey(o => o.Id);

            EstimationConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("Estimationseq");
            EstimationConfiguration.Property(o => o.Price).IsRequired();
            EstimationConfiguration.Property(o => o.FK_Call_Id).IsRequired();
            EstimationConfiguration.Property(o => o.RefNumber).IsRequired();
            //EstimationConfiguration.Ignore(o => o.lstEstimationItems);
            EstimationConfiguration.HasMany(o => o.lstEstimationItems).WithOne(o => o.Estimation).HasForeignKey(o => o.FK_Estimation_Id);
        }
    }
}
