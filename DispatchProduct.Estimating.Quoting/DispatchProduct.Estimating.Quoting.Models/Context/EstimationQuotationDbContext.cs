﻿using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Estimating.Quoting.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace DispatchProduct.Estimating.Quoting.Context
{
    public class EstimationQuotationDbContext : DbContext
    {

        public DbSet<EstimationItem> EstimationItem { get; set; }
        public DbSet<Estimation> Estimation { get; set; }
        public DbSet<Quotation> Quotation { get; set; }

        public EstimationQuotationDbContext(DbContextOptions<EstimationQuotationDbContext> options)
            : base(options)
        {
            


        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EstimationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new QuotationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new EstimationItemEntityTypeConfiguration());
        }
    }
}
