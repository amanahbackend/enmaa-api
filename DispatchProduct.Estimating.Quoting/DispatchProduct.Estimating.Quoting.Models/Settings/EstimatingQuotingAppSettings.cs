﻿namespace DispatchProduct.Estimating.Quoting.Settings
{
    public class EstimatingQuotingAppSettings
    {
        public bool UseCustomizationData { get; set; }

        public string IdentityUrl { get; set; }

        public string ClientId { get; set; }

        public string Secret { get; set; }

        public string EstimationPrefix { get; set; }

        public string EstimationOrderDayStartNo { get; set; }

        public string QuotationOrderDayStartNo { get; set; }

        public string QuotationDocSrcPath { get; set; }

        public string QuotationDocDstPath { get; set; }

        public string QuotationDocSrcName { get; set; }

        public string QuotationDocDstSuffix { get; set; }
    }
}
