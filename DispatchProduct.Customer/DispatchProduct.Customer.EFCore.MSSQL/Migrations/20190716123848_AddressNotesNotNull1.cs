﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.EFCore.MSSQL.Migrations
{
    public partial class AddressNotesNotNull1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "AddressNote",
                table: "Location",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.CreateIndex(
                name: "IX_Location_Fk_Customer_Id",
                table: "Location",
                column: "Fk_Customer_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPhoneBook_FK_Customer_Id",
                table: "CustomerPhoneBook",
                column: "FK_Customer_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPhoneBook_FK_PhoneType_Id",
                table: "CustomerPhoneBook",
                column: "FK_PhoneType_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_FK_CustomerType_Id",
                table: "Customer",
                column: "FK_CustomerType_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_FK_MachineType_Id",
                table: "Customer",
                column: "FK_MachineType_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Complain_FK_Customer_Id",
                table: "Complain",
                column: "FK_Customer_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Complain_Customer_FK_Customer_Id",
                table: "Complain",
                column: "FK_Customer_Id",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Customer_CustomerType_FK_CustomerType_Id",
                table: "Customer",
                column: "FK_CustomerType_Id",
                principalTable: "CustomerType",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Customer_MachineType_FK_MachineType_Id",
                table: "Customer",
                column: "FK_MachineType_Id",
                principalTable: "MachineType",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPhoneBook_Customer_FK_Customer_Id",
                table: "CustomerPhoneBook",
                column: "FK_Customer_Id",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPhoneBook_PhoneType_FK_PhoneType_Id",
                table: "CustomerPhoneBook",
                column: "FK_PhoneType_Id",
                principalTable: "PhoneType",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Location_Customer_Fk_Customer_Id",
                table: "Location",
                column: "Fk_Customer_Id",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Complain_Customer_FK_Customer_Id",
                table: "Complain");

            migrationBuilder.DropForeignKey(
                name: "FK_Customer_CustomerType_FK_CustomerType_Id",
                table: "Customer");

            migrationBuilder.DropForeignKey(
                name: "FK_Customer_MachineType_FK_MachineType_Id",
                table: "Customer");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPhoneBook_Customer_FK_Customer_Id",
                table: "CustomerPhoneBook");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPhoneBook_PhoneType_FK_PhoneType_Id",
                table: "CustomerPhoneBook");

            migrationBuilder.DropForeignKey(
                name: "FK_Location_Customer_Fk_Customer_Id",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_Fk_Customer_Id",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_CustomerPhoneBook_FK_Customer_Id",
                table: "CustomerPhoneBook");

            migrationBuilder.DropIndex(
                name: "IX_CustomerPhoneBook_FK_PhoneType_Id",
                table: "CustomerPhoneBook");

            migrationBuilder.DropIndex(
                name: "IX_Customer_FK_CustomerType_Id",
                table: "Customer");

            migrationBuilder.DropIndex(
                name: "IX_Customer_FK_MachineType_Id",
                table: "Customer");

            migrationBuilder.DropIndex(
                name: "IX_Complain_FK_Customer_Id",
                table: "Complain");

            migrationBuilder.AlterColumn<string>(
                name: "AddressNote",
                table: "Location",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);
        }
    }
}
