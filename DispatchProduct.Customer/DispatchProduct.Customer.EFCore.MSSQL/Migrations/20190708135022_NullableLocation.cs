﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.EFCore.MSSQL.Migrations
{
    public partial class NullableLocation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPhoneBook_Customer_FK_Customer_Id",
                table: "CustomerPhoneBook");

            migrationBuilder.DropIndex(
                name: "IX_CustomerPhoneBook_FK_Customer_Id",
                table: "CustomerPhoneBook");

            migrationBuilder.AlterColumn<double>(
                name: "HouseNo",
                table: "Location",
                type: "float",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "FloorNo",
                table: "Location",
                type: "float",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "AppartmentNo",
                table: "Location",
                type: "float",
                nullable: true,
                oldClrType: typeof(double));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "HouseNo",
                table: "Location",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float",
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "FloorNo",
                table: "Location",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float",
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "AppartmentNo",
                table: "Location",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPhoneBook_FK_Customer_Id",
                table: "CustomerPhoneBook",
                column: "FK_Customer_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPhoneBook_Customer_FK_Customer_Id",
                table: "CustomerPhoneBook",
                column: "FK_Customer_Id",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
