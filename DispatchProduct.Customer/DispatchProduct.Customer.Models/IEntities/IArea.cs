﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.Entities;

namespace DispatchProduct.CustomerModule.IEntities
{
    public interface IArea : IBaseEntity
    {
        string Id { get; set; }
        string Name { get; set; }
        string FK_Governrate_Id { get; set; }
    }
}
