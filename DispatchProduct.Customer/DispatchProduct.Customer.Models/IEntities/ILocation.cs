﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.Entities;
using System;

namespace DispatchProduct.CustomerModule.IEntities
{
    public interface ILocation : IBaseEntity
    {
        int Id { get; set; }

        string PACINumber { get; set; }

        string Governorate { get; set; }

        string Area { get; set; }

        string Block { get; set; }

        string Street { get; set; }

        string AddressNote { get; set; }

        string Building { set; get; }

        double? AppartmentNo { set; get; }

        double? FloorNo { set; get; }

        double? HouseNo { set; get; }

        double Latitude { get; set; }

        double Longitude { get; set; }

        Customer Customer { get; set; }
    }
}

