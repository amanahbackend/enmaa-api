﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.CustomerModule.IEntities
{
    public interface ICustomerPhoneBook : IBaseEntity
    {
        int FK_Customer_Id { get; set; }

        string Phone { get; set; }

        int FK_PhoneType_Id { get; set; }
    }
}
