﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.Entities;

namespace DispatchProduct.CustomerModule.IEntities
{
    public interface IGovernrate : IBaseEntity
    {
        string Id { get; set; }
        string Name { get; set; }
    }
}
