﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.CustomerModule.IEntities
{
    public interface IPhoneType : IBaseEntity
    {
        int Id { get; set; }

        string Name { get; set; }
         bool IsDefault { set; get; }

    }
}
