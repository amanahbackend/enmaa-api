﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.IEntities;

namespace DispatchProduct.CustomerModule.Entities
{
    public class Complain : BaseEntity, IComplain, IBaseEntity
    {
        public int Id { get; set; }

        public string Note { get; set; }

        public int FK_Customer_Id { get; set; }

        public Customer Customer { get; set; }
    }
}

