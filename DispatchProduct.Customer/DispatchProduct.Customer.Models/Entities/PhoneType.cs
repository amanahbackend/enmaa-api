﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.IEntities;
using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.Entities
{
    public class PhoneType : BaseEntity, IPhoneType, IBaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public bool IsDefault { set; get; }

        public ICollection<CustomerPhoneBook> CustomerPhoneBooks { get; set; }

        
    }
}
