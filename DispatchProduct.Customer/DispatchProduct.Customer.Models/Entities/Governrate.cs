﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.IEntities;

namespace DispatchProduct.CustomerModule.Entities
{
    public class Governrate : BaseEntity, IGovernrate, IBaseEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
