﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.IEntities;

namespace DispatchProduct.CustomerModule.Entities
{
    public class CustomerPhoneBook : BaseEntity, ICustomerPhoneBook, IBaseEntity
    {
        public int Id { get; set; }

        public int FK_Customer_Id { get; set; }

        public string Phone { get; set; }

        public int FK_PhoneType_Id { get; set; }

        public PhoneType PhoneType { get; set; }

        public Customer Customer { get; set; }
    }
}

