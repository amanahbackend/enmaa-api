﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.IEntities;

namespace DispatchProduct.CustomerModule.Entities
{
    public class Area : BaseEntity, IArea, IBaseEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string FK_Governrate_Id { get; set; }
    }
}
