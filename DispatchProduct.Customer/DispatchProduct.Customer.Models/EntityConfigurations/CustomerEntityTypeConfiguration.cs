﻿using DispatchProduct.CustomerModule.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.CustomerModule.EntityConfigurations
{
    public class CustomerEntityTypeConfiguration
        : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> CustomerConfiguration)
        {
            CustomerConfiguration.ToTable("Customer");
            CustomerConfiguration.HasKey(o => o.Id);
            CustomerConfiguration.Property<int>(o => o.Id)
                .ForSqlServerUseSequenceHiLo<int>("Customerseq");
            //CustomerConfiguration.Ignore(o => o.Complains);
            //CustomerConfiguration.Ignore(o => o.Locations);

            CustomerConfiguration.HasMany(x => x.Locations).WithOne(x => x.Customer).HasForeignKey(x => x.Fk_Customer_Id).OnDelete(DeleteBehavior.SetNull);
            CustomerConfiguration.HasOne(o => o.CustomerType).WithMany(x=>x.Customers).HasForeignKey(x=>x.FK_CustomerType_Id).OnDelete(DeleteBehavior.SetNull);
            CustomerConfiguration.HasOne(o => o.MachineType).WithMany(x => x.Customers).HasForeignKey(x => x.FK_MachineType_Id).OnDelete(DeleteBehavior.SetNull);

            CustomerConfiguration.HasMany(x => x.Complains).WithOne(x => x.Customer).HasForeignKey(x => x.FK_Customer_Id);
            CustomerConfiguration.HasMany(x => x.CustomerPhoneBook).WithOne(x => x.Customer).HasForeignKey(x => x.FK_Customer_Id);
            CustomerConfiguration.Property<string>(o => o.CivilId).IsRequired(false);
            CustomerConfiguration.Property<string>(o => o.Remarks).IsRequired(false);
        }
    }
}
