﻿using DispatchProduct.CustomerModule.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.CustomerModule.EntityConfigurations
{
    public class GovernrateEntityTypeConfiguration : IEntityTypeConfiguration<Governrate>

    {
        public void Configure(EntityTypeBuilder<Governrate> GovernrateConfiguration)
        {
            GovernrateConfiguration.ToTable("Governrate");
        }
    }

}
