﻿using DispatchProduct.CustomerModule.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.CustomerModule.EntityConfigurations
{
    public class AreaEntityTypeConfiguration : IEntityTypeConfiguration<Area>

    {
        public void Configure(EntityTypeBuilder<Area> AreaConfiguration)
        {
            AreaConfiguration.ToTable("Area");
        }
    }

}
