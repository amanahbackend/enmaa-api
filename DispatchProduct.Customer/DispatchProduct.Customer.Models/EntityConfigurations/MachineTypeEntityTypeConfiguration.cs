﻿using DispatchProduct.CustomerModule.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.CustomerModule.EntityConfigurations
{
    public class MachineTypeEntityTypeConfiguration
        : IEntityTypeConfiguration<MachineType>
    {
        public void Configure(EntityTypeBuilder<MachineType> CustomerTypeConfiguration)
        {
            CustomerTypeConfiguration.ToTable("MachineType");

            CustomerTypeConfiguration.HasKey(o => o.Id);

            CustomerTypeConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("MachineTypeseq");


            CustomerTypeConfiguration.Property(o => o.Name)
                .HasMaxLength(500)
                .IsRequired();



            CustomerTypeConfiguration.HasMany(o => o.Customers).WithOne(x => x.MachineType).HasForeignKey(x => x.FK_MachineType_Id).OnDelete(DeleteBehavior.SetNull);

        }
    }
}
