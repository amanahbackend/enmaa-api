﻿using DispatchProduct.CustomerModule.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.CustomerModule.EntityConfigurations
{
    public class CustomerPhoneBookEntityTypeConfiguration
        : IEntityTypeConfiguration<CustomerPhoneBook>
    {
        public void Configure(EntityTypeBuilder<CustomerPhoneBook> CustomerPhoneBookConfiguration)
        {
            CustomerPhoneBookConfiguration.ToTable<CustomerPhoneBook>("CustomerPhoneBook");
            CustomerPhoneBookConfiguration.HasKey(o => o.Id);
            CustomerPhoneBookConfiguration.HasOne(o => o.PhoneType).WithMany(x=>x.CustomerPhoneBooks).HasForeignKey(x=>x.FK_PhoneType_Id).OnDelete(DeleteBehavior.SetNull);
            CustomerPhoneBookConfiguration.HasOne(o => o.Customer).WithMany(x => x.CustomerPhoneBook).HasForeignKey(x => x.FK_Customer_Id).IsRequired(true);
            CustomerPhoneBookConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo<int>("CustomerPhoneBookseq", (string)null);
            CustomerPhoneBookConfiguration.Property(o => o.FK_PhoneType_Id).IsRequired(true);
            //CustomerPhoneBookConfiguration.Property(o => o.FK_Customer_Id).IsRequired(true);
            CustomerPhoneBookConfiguration.Property(o => o.Phone).IsRequired(true);
        }
    }
}
