﻿using AutoMapper;
using DispatchProduct.CustomerModule.BLL.ViewModel;
using DispatchProduct.CustomerModule.BLL.IManagers;
using DispatchProduct.CustomerModule.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ExcelToGenericList;
using Utilites.UploadFile;
using Utilities.Utilites;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Route("api/Customer")]
    public class CustomerController : Controller
    {
        private ICustomerManager manger;
        private readonly IMapper mapper;
        ICustomerPhoneBookManager customerPhoneManger;
        ILocationManager locationManger;
        ICustomerTypeManager customerTypeManager;
        private readonly IHostingEnvironment _hostingEnv;
        public CustomerController(ICustomerManager _manger, IMapper _mapper,
            ICustomerPhoneBookManager _customerPhoneManger, ILocationManager _locationManger,
            IHostingEnvironment hostingEnv, ICustomerTypeManager customerTypeManager)
        {
            this.customerTypeManager = customerTypeManager;
            this.manger = _manger;
            this.mapper = _mapper;
            customerPhoneManger = _customerPhoneManger;
            locationManger = _locationManger;
            _hostingEnv = hostingEnv;
        }



        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public  CustomerViewModel GetAsync([FromRoute]int id)
        {
            var result =  manger.Get(id);
            return result;
        }
        [Route("GetAllByPaging")]
        [HttpPost]
        public async Task<ActionResult> GetAllAsync([FromBody]PaginatedItemsViewModel pagingParameterModel)
        {
            var Result = await manger.GetAllCustomersFilledPropsByPagingAsync(pagingParameterModel);
            return Ok(Result);
        }

        [Route("GetAll")]
        [HttpGet]
        public async Task<List<CustomerViewModel>> GetAsync()
        {
           List<CustomerViewModel> result = new List<CustomerViewModel>();
           result = await manger.GetAllDetailedCustomerAsync();      
           return result;
        }

        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]CustomerViewModel model)
        {
             var result = await manger.AddAsync(model);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]CustomerViewModel model)
        {
            bool result = false;
            result =await manger.UpdateAsync(model);
            return Ok(result);

        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            var entity = manger.Get(id);
            result = await manger.SoftDeleteAsync(entity);
            return Ok(result);
        }
        #endregion
        #endregion


        #region Custom Buisness
        [Route("CustomerHistory/{id}")]
        [HttpGet]
        public async Task<IActionResult> CustomerHistoryAsync([FromRoute]int id)
        {
            CustomerHistoryViewModel result = new CustomerHistoryViewModel();
            CustomerHistory entityResult = new CustomerHistory();
            entityResult =await manger.GetCustomerHistory(id);
            result = mapper.Map<CustomerHistory, CustomerHistoryViewModel>(entityResult);
            return Ok(result);
        }


        [Route("SearchCustomer/{key}")]
        [HttpGet]
        public async Task<IActionResult> SearchCustomerAsync([FromRoute]string key)
        {
            var entityResult = await manger.SearchForCustomer(key);
            return Ok(entityResult);
        }

        [Route("SearchCustomer")]
        [HttpPost]
        public async Task<IActionResult> SearchCustomer([FromBody]PaginatedItemsViewModel pagingparametermodel)
        {
            
            if (pagingparametermodel.SearchBy != null)
            {
                var Result =await manger.SearchForCustomerByCIDorPhoneWithPagingAsync(pagingparametermodel);
                return Ok(Result);
            }
            return BadRequest();

        }
        [Route("SearchCustomerByName/{key}")]
        [HttpGet]
        public IActionResult SearchCustomerByName([FromRoute]string key)
        {
            var entityResult = manger.SearchForCustomerByName(key);
            var result = mapper.Map<List<Customer>, List<CustomerViewModel>>(entityResult);
            return Ok(result);
        }

        //[Route("SearchCustomerByCIDorPhone/{key}")]
        //[HttpGet]
        //public IActionResult SearchCustomerByCIDorPhone([FromRoute]string key)
        //{
        //    var entityResult = manger.SearchForCustomerByCIDorPhone(key);
        //    var result = mapper.Map<List<Customer>, List<CustomerViewModel>>(entityResult);
        //    return Ok(result);
        //}


    
        //[Route("SearchCustomerByCIDorPhone/{key}")]
        //[HttpGet]
        //public IActionResult SearchCustomerByCIDorPhone([FromRoute]string key)
        //{
        //    var entityResult = manger.SearchForCustomerByCIDorPhone(key);
        //    var result = mapper.Map<List<Customer>, List<CustomerViewModel>>(entityResult);
        //    return Ok(result);
        //}

        [Route("CreateDetailedCustomer")]
        [HttpPost]
        public async Task<Customer> CreateDetailedCustomer([FromBody] DetailedCustomerViewModel model)
        {
            var customer = mapper.Map<DetailedCustomerViewModel, Customer>(model);
            var customerVM = mapper.Map<Customer, CustomerViewModel>(customer);
            customerVM =await manger.AddAsync(customerVM);
            if (customerVM != null)
            {
                customer.Locations = new List<Location>();
                model.Id = customerVM.Id;
                var call = mapper.Map<DetailedCustomerViewModel, CustomerPhoneBook>(model);
                customerPhoneManger.AddAsync(call);
                var location = mapper.Map<DetailedCustomerViewModel, Location>(model);
                customer.Locations.Add(locationManger.AddAsync(location));
            }
            return customer;
        }

        [Route("GetBy")]
        [HttpGet]
        public IActionResult GetBy([FromQuery]string name, [FromQuery]string phone)
        {
            var entityResult = manger.SearchForCustomerByName(phone).FirstOrDefault();
            var result = mapper.Map<Customer, CustomerViewModel>(entityResult);
            return Ok(result);
        }


        #endregion

        [HttpPost, Route("ImportFromFile")]
        public IActionResult ImportFromFile([FromBody]UploadFile file)
        {
            var fileExt = StringUtilities.GetFileExtension(file.FileContent);
            file.FileName = $"Customers_{DateTime.Now.Ticks}.{fileExt}";
            UploadExcelFileManager fileManager = new UploadExcelFileManager();
            var path = $@"{_hostingEnv.WebRootPath}\Import\";
            var addFileResult = fileManager.AddFile(file, path);
            var result = AddFromFileAsync($@"{addFileResult.returnData}\{file.FileName}");
            return Ok(result);
        }

        private async Task<List<Customer>> AddFromFileAsync(string filePath)
        {
            var customers = new List<Customer>();
            var models = ExcelReader.GetDataToList(filePath, GetItems);
            string fileName = Path.GetFileName(filePath);
            int i = 0;
            foreach (var item in models)
            {
                i++;
                var customerData = new DetailedCustomerViewModel
                {
                    Name = item.Name,
                    Remarks = item.Remarks,
                    CivilId = item.CivilId,
                    AddressNote = item.AddressNote,
                    Area = item.Area,
                    Block = item.Block,
                    Governorate = item.Governorate,
                    Latitude = item.Latitude,
                    Longitude = item.Longitude,
                    PACINumber = item.PACINumber,
                    PhoneNumber = item.Phone,
                    Street = item.Street,
                    Title = item.Title,
                    FK_CustomerType_Id = customerTypeManager.Get(x => x.Name.Equals(item.CustomerType)).Id
                };
                var customer =await CreateDetailedCustomer(customerData);
                var customerVM= mapper.Map<Customer, CustomerViewModel>(customer);

                customers.Add(customer);
            }
            return customers;
        }

        private CustomerFromFileViewModel GetItems(IList<string> rowData, IList<string> columnNames)
        {
            var model = new CustomerFromFileViewModel();

            var customer = new CustomerFromFileViewModel()
            {
                AddressNote = rowData[columnNames.IndexFor(nameof(model.AddressNote))],
                Area = rowData[columnNames.IndexFor(nameof(model.Area))],
                Block = rowData[columnNames.IndexFor(nameof(model.Block))],
                CivilId = rowData[columnNames.IndexFor(nameof(model.CivilId))],
                CustomerType = rowData[columnNames.IndexFor(nameof(model.CustomerType))],
                Governorate = rowData[columnNames.IndexFor(nameof(model.Governorate))],
                Latitude = Convert.ToDouble(rowData[columnNames.IndexFor(nameof(model.Latitude))]),
                Longitude = Convert.ToDouble(rowData[columnNames.IndexFor(nameof(model.Longitude))]),
                Name = rowData[columnNames.IndexFor(nameof(model.Name))],
                PACINumber = rowData[columnNames.IndexFor(nameof(model.PACINumber))],
                Phone = rowData[columnNames.IndexFor(nameof(model.Phone))],
                Remarks = rowData[columnNames.IndexFor(nameof(model.Remarks))],
                Street = rowData[columnNames.IndexFor(nameof(model.Street))],
                Title = rowData[columnNames.IndexFor(nameof(model.Title))]
            };
            return customer;
        }

        public void LogResult(string resultMessage, string excelFileName)
        {
            string date = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year;
            //string dir = @"D:\Amanah\Dev\Migration"; 
            string dir = $@"{_hostingEnv.WebRootPath}\Logs";

            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string path = dir + @"\" + "Request_Log_" + excelFileName + ".txt";
            if (!System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
        }
    }
}