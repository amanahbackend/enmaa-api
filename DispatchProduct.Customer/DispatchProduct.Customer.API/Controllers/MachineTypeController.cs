﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using DispatchProduct.CustomerModule.BLL.ViewModel;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.BLL.IManagers;
using Microsoft.AspNetCore.Authorization;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    //[Authorize]
    [Route("api/MachineType")]
    public class MachineTypeController : Controller
    {
        public readonly IMachineTypeManager manger;
        public readonly IMapper mapper;

        public MachineTypeController(IMachineTypeManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;

        }


        public IActionResult Index()
        {
          
            return Ok();
        }


        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public MachineTypeViewModel Get(int id)
        {
            MachineTypeViewModel result = new MachineTypeViewModel();
            MachineType entityResult = new MachineType();
            entityResult=manger.Get(id);
            result = mapper.Map<MachineType,MachineTypeViewModel> (entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public List<MachineTypeViewModel> Get()
        {
            List<MachineTypeViewModel> result = new List<MachineTypeViewModel>();
            List<MachineType> entityResult = new List<MachineType>();
            entityResult = manger.GetAll().ToList();
            result= mapper.Map < List <MachineType> ,List<MachineTypeViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]MachineTypeViewModel model)
        {
            MachineTypeViewModel result = new MachineTypeViewModel();
            MachineType entityResult = new MachineType();
            entityResult = mapper.Map<MachineTypeViewModel, MachineType> (model);


            entityResult = manger.AddAsync(entityResult);
            result = mapper.Map<MachineType, MachineTypeViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]MachineTypeViewModel model)
        {
            bool result = false;
            MachineType entityResult = new MachineType();
            entityResult = mapper.Map<MachineTypeViewModel, MachineType>(model);
            result = manger.Update(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi

        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            MachineType entityResult = new MachineType();
            MachineTypeViewModel model = new MachineTypeViewModel();
            model.Id = id;
            entityResult = mapper.Map<MachineTypeViewModel, MachineType>(model);
            result = manger.Delete(entityResult);
            return Ok(result);
        }
        #endregion
        #endregion
      
    }
}