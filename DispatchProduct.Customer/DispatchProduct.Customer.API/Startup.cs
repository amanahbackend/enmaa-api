﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IdentityModel.Tokens.Jwt;
using IdentityServer4.AccessTokenValidation;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using DispatchProduct.Repoistry;
using DispatchProduct.CustomerModule.Models;
using DispatchProduct.CustomerModule.API.Settings;
using Swashbuckle.AspNetCore.Swagger;
using DispatchProduct.CustomerModule.IEntities;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.BLL.IManagers;
using DispatchProduct.CustomerModule.Managers;
using DispatchProduct.CustomerModule.BLL.Managers;
using DispatchProduct.CustomerModule.Entities.LocationSettings;
using DispatchProduct.CustomerModule.API.ServicesCommunication.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Diagnostics;
using System.Net;

namespace DispatchProduct.CustomerModule.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddDbContext<CustomerDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("DispatchProduct.Customer.EFCore.MSSQL")));

            services.AddOptions();
            OptionsServiceCollectionExtensions.AddOptions(services);
            services.Configure<CustomerAppSettings>(Configuration);
            services.Configure<LocationSettings>(Configuration.GetSection("LocationSettings"));
            services.Configure<IdentityServiceSetting>(Configuration.GetSection("IdentityServiceSetting"));


            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1",
                    new Info()
                    {
                        Title = "Customer API",
                        Description = "Customer  API"
                    });
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { "Customer", "Customer API" }
                    }
                });
            });

            ConfigureAuthService(services);

            services.AddMvc();

            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.AddTransient<DbContext, CustomerDbContext>();
            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));
            services.AddScoped(typeof(IComplain), typeof(Complain));
            services.AddScoped(typeof(ICustomer), typeof(Customer));
            services.AddScoped(typeof(ICustomerHistory), typeof(CustomerHistory));
            services.AddScoped(typeof(ICustomerPhoneBook), typeof(CustomerPhoneBook));
            services.AddScoped(typeof(ICustomerType), typeof(CustomerType));
            services.AddScoped(typeof(IMachineType), typeof(MachineType));
            services.AddScoped(typeof(IPhoneType), typeof(PhoneType));
            services.AddScoped(typeof(IArea), typeof(Area));
            services.AddScoped(typeof(IGovernrate), typeof(Governrate));
            services.AddScoped(typeof(IComplainManager), typeof(ComplainManager));
            services.AddScoped(typeof(ICustomerPhoneBookManager), typeof(CustomerPhoneBookManager));
            services.AddScoped(typeof(ICustomerTypeManager), typeof(CustomerTypeManager));
            services.AddScoped(typeof(IMachineTypeManager), typeof(MachineTypeManager));
            services.AddScoped(typeof(ICustomerManager), typeof(CustomerManager));
            services.AddScoped(typeof(ILocation), typeof(Location));

            services.AddScoped(typeof(ILocationManager), typeof(LocationManager));
            services.AddScoped(typeof(IPhoneTypeManager), typeof(PhoneTypeManager));
            services.AddScoped(typeof(IAreaManager), typeof(AreaManager));
            services.AddScoped(typeof(IGovernrateManager), typeof(GovernrateManager));

            services.AddScoped(typeof(IIdentityRoleService), typeof(IdentityRoleService));
            var container = new ContainerBuilder();
            container.Populate(services);
            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("AllowAll");

      


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseExceptionHandler(builder => {
                // Adds a terminal middleware delegate to the application
                // request pipeline
                // The Run() method can access the http request/response context
                builder.Run(async context => {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                    var error = context.Features.Get<IExceptionHandlerFeature>();

                    if (error != null)
                    {
                        // We are going to extend context.Response so that we can add our
                        // own custom error response headers

                        //context.Response.AddApplicationError(error.Error.Message);

                        await context.Response.WriteAsync(error.Error.Message);
                    }
                });
            });
           

           
            ConfigureAuth(app);
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Calling API");
            });


            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
            //    await next();
            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = identityUrl;

                // name of the API resource
                options.ApiName = Configuration["ClientId"];
                options.ApiSecret = Configuration["Secret"];
                options.RequireHttpsMetadata = false;
                options.EnableCaching = true;
                options.CacheDuration = TimeSpan.FromMinutes(10);
                options.SaveToken = true;
            });

        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
        }
    }
}
