﻿using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.PACI;

namespace DispatchProduct.CustomerModule.BLL.IManagers
{
    public interface IAreaManager : IRepositry<Area>
    {
        List<Area> GetAreas(string govId);
        void SaveAreas(List<PACIHelper.DropPACI> Areas,string GovernrateId);
    }

}
