﻿using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.PACI;

namespace DispatchProduct.CustomerModule.BLL.IManagers
{
    public interface IGovernrateManager : IRepositry<Governrate>
    {
        List<Governrate> GetAllGovernorates();
        Governrate GetGovernrate(string governrateId);
        List<Governrate> SaveGovernrates(List<PACIHelper.DropPACI> governrates);

    }
}
