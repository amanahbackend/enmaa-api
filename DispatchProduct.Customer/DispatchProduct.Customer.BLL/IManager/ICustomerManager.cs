﻿
using DispatchProduct.CustomerModule.BLL.ViewModel;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.Repoistry;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace DispatchProduct.CustomerModule.BLL.IManagers
{
    public interface ICustomerManager : IBaseManager<CustomerViewModel,Customer>
    {
        CustomerViewModel Get(int id);
        Task<CustomerViewModel> SearchForCustomer(string key);
        Task<CustomerHistory> GetCustomerHistory(int customerId);
        List<Customer> SearchForCustomerByName(string name);
        List<Customer> SearchForCustomerByCIDorPhone(string name);
        Task<PagedResult<CustomerViewModel>> SearchForCustomerByCIDorPhoneWithPagingAsync(PaginatedItemsViewModel pagingparametermodel);
        Task<List<CustomerViewModel>> GetAllDetailedCustomerAsync();
        Task<PagedResult<CustomerViewModel>> GetAllCustomersFilledPropsByPagingAsync(PaginatedItemsViewModel pagingParameterModel);
    }
}
