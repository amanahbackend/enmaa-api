﻿namespace DispatchProduct.CustomerModule.BLL.ViewModel
{
    public class CustomerTypeViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public bool IsDefault { set; get; }

    }
}