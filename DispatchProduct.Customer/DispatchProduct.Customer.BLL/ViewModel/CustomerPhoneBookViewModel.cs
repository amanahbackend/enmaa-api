﻿namespace DispatchProduct.CustomerModule.BLL.ViewModel
{
    public class CustomerPhoneBookViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public int FK_Customer_Id { get; set; }

        public string Phone { get; set; }

        public int FK_PhoneType_Id { get; set; }

        public PhoneTypeViewModel PhoneType { get; set; }

        public CustomerViewModel Customer { get; set; }
    }
}
