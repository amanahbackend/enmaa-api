﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
namespace DispatchProduct.CustomerModule.BLL.ViewModel
{
    public class ComplainViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Note { get; set; }

        public int FK_Customer_Id { get; set; }

        public CustomerViewModel Customer { get; set; }
    }
}
