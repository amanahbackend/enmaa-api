﻿using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.BLL.ViewModel
{
    public class CustomerHistoryViewModel : BaseEntityViewModel
    {
        public CustomerViewModel Customer { get; set; }

        public IEnumerable<ComplainViewModel> Complains { get; set; }
    }
}
