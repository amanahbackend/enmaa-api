﻿using DispatchProduct.CustomerModule.BLL.IManagers;
using DispatchProduct.CustomerModule.Models;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using AutoMapper.QueryableExtensions;
using DispatchProduct.CustomerModule.BLL.ViewModel;
using Utilities.Utilites.Paging;
using System.Linq.Expressions;
using AutoMapper;
using Utilities.Utilites;

namespace DispatchProduct.CustomerModule.BLL.Managers
{
    public class CustomerManager : BaseManager<CustomerViewModel, Customer>, ICustomerManager
    {

        public CustomerManager(CustomerDbContext _context, IRepositry<Customer> _repository, IMapper _mapper)
             : base(_context, _repository, _mapper)
        {
        }



        #region

        public override async Task<bool> UpdateAsync(CustomerViewModel entity)
        {
            bool result;
            if (entity.CustomerPhoneBook != null)
            {
                if (entity.Locations != null)
                {
                    var locations = mapper.Map<ICollection<LocationViewModel>, ICollection<Location>>(entity.Locations);

                    context.UpdateRange(locations);
                }
                if (entity.CustomerPhoneBook != null)
                {

                    var customerPhonbooks = mapper.Map<ICollection<CustomerPhoneBookViewModel>, ICollection<CustomerPhoneBook>>(entity.CustomerPhoneBook);

                    context.UpdateRange(customerPhonbooks);
                }

                result = await base.UpdateAsync(entity);
            }
            else
                result = await base.UpdateAsync(entity);
            return result;
        }


        #endregion



        public async Task<PagedResult<CustomerViewModel>> GetAllCustomersFilledPropsByPagingAsync(PaginatedItemsViewModel pagingParameterModel)
        {
            try
            {
                PagedResult<CustomerViewModel> pagedResultviw = new PagedResult<CustomerViewModel>();

                var customerQuery = repository.GetAll().AsQueryable();

                var pagedResult = await GetAllByPaginationAsync(customerQuery, pagingParameterModel);



                return pagedResult;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async Task<List<CustomerViewModel>> GetAllDetailedCustomerAsync()
        {
            var customers = await base.GetAllAsync();
            return customers;
        }
        public async Task<CustomerViewModel> SearchForCustomer(string key)
        {
            return (context as CustomerDbContext).Customer.ProjectTo<CustomerViewModel>().Where(x => x.Name.ToLower().Contains(key.ToLower())).FirstOrDefault();
        }
        public List<Customer> SearchForCustomerByName(string name)
        {
            var customers = repository.GetAll().Include(x => x.CustomerPhoneBook).Where(x => x.Name.ToLower().Contains(name.ToLower())).Take(10).ToList();      
            return customers;
        }


        public async Task<PagedResult<CustomerViewModel>> SearchForCustomerByCIDorPhoneWithPagingAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            var orderListQuery = repository.GetAll()
                .Where(x => x.CivilId.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                x.Name.ToLower().Contains(pagingparametermodel.SearchBy.ToLower())).AsQueryable();

            if (!(orderListQuery.ToList().Count > 0))
            {
                orderListQuery = repository.GetAll().Where(x => HasPhone(x.CustomerPhoneBook, pagingparametermodel.SearchBy)).AsQueryable();
            }
            var lstCustomers =  await base.GetAllByPaginationAsync(orderListQuery, pagingparametermodel);
        
            return lstCustomers;
        }






        public List<Customer> SearchForCustomerByCIDorPhone(string name)
        {
            var customers = repository.GetAll().Include(x => x.CustomerPhoneBook).Where(x => x.CivilId.ToLower().Contains(name.ToLower()) || HasPhone(x.CustomerPhoneBook, name)).Take(10).ToList();
            foreach (var customer in customers)
            {
                customer.CustomerPhoneBook = null;
                BindNavProps(customer);
            }
            return customers;
        }



        private bool HasPhone(ICollection<CustomerPhoneBook> customerPhoneBooks, string phone)
        {
            try
            {
                bool exist = customerPhoneBooks.Any(x => x.Phone.ToLower().Contains(phone.ToLower()));

                return exist;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public async Task<CustomerHistory> GetCustomerHistory(int customerId)
        {
            var result = new CustomerHistory();
            result.Customer = await (context as CustomerDbContext).Customer.Where(x => x.Id == customerId).Include(x => x.Complains).FirstOrDefaultAsync(); ;
            result.Complains = result.Customer.Complains;
            return result;
        }



        public CustomerViewModel Get(int id)
        {
            return (context as CustomerDbContext).Customer.ProjectTo<CustomerViewModel>().Where(x => x.Id == id).FirstOrDefault();
        }

        //public override Task<bool> DeleteAsync(CustomerViewModel viewModel)
        //{
        //        bool result = false;
        //        //if (locationManger.DeleteByCustomerId(entity.Id))
        //        //{
        //        //   if (complainManger.DeleteByCustomerId(entity.Id))
        //        //    {
        //        //       result = repository.Delete(entity);
        //        //    }
        //        //}






        //    return result;
        //}

        public Customer BindNavProps(Customer model)
        {
            return model;
        }

     
    }
}
