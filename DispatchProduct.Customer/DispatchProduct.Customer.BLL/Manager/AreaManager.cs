﻿using DispatchProduct.CustomerModule.BLL.IManagers;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.Models;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.PACI;

namespace DispatchProduct.CustomerModule.Managers
{
    public class AreaManager : Repositry<Area>, IAreaManager
    {
        public AreaManager(CustomerDbContext context)
        : base(context)
        {

        }

        public List<Area> GetAreas(string govId)
        {
            return GetAll().Where(a => a.FK_Governrate_Id == govId).ToList();
        }

        public void SaveAreas(List<PACIHelper.DropPACI> areas, string govId)
        {
            Area area = new Area();
            foreach (var item in areas)
            {
                area.Id = item.Id;
                area.Name = item.Name;
                area.FK_Governrate_Id = govId;
                area = AddAsync(area);
            }
        }
    }
}
