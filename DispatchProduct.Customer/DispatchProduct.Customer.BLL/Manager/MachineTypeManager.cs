﻿
using Microsoft.EntityFrameworkCore;
using DispatchProduct.CustomerModule.BLL.IManagers;
using System.Threading.Tasks;
using DispatchProduct.Repoistry;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.Models;

namespace DispatchProduct.CustomerModule.BLL.Managers
{
    public class MachineTypeManager : Repositry<MachineType>, IMachineTypeManager
    {
        public MachineTypeManager(CustomerDbContext context)
            : base(context)
        {

        }

    }
}
