﻿using DispatchProduct.CustomerModule.BLL.IManagers;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.Models;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.PACI;

namespace DispatchProduct.CustomerModule.BLL.Managers
{
    public class GovernrateManager : Repositry<Governrate>, IGovernrateManager
    {
        public GovernrateManager(CustomerDbContext context)
    : base(context)
        {

        }

        public List<Governrate> GetAllGovernorates()
        {
            return GetAll().ToList();
        }

        public Governrate GetGovernrate(string governrateId)
        {
            Governrate result = null;
            result = base.Get(governrateId);
            return result;
        }

        public List<Governrate> SaveGovernrates(List<PACIHelper.DropPACI> governrates)
        {
            Governrate governrate = new Governrate();
            List<Area> areas = new List<Area>();
            foreach (var item in governrates)
            {
                governrate.Id = item.Id;
                governrate.Name = item.Name;
                governrate = AddAsync(governrate);
            }
            return GetAll().ToList();
        }

    }
}
