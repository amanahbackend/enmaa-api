﻿using DispatchProduct.CustomerModule.BLL.IManagers;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.Entities.LocationSettings;
using DispatchProduct.CustomerModule.Models;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Utilites.PACI;
using Utilities.Utilites.PACI;

namespace DispatchProduct.CustomerModule.BLL.Managers
{
    public class LocationManager : Repositry<Location>, ILocationManager
    {
        LocationSettings settings;
        public LocationManager(CustomerDbContext context,IOptions<LocationSettings> _settings)
            : base(context)
        {
            settings = _settings.Value;
        }

        public override Location AddAsync(Location entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(entity.PACINumber))
                {
                    if (entity.Latitude == 0 && entity.Longitude == 0)
                    {
                        var point = GetCoordinatesByPaci(Convert.ToInt32(entity.PACINumber));
                        entity.Latitude = point.Latitude.Value;
                        entity.Longitude = point.Longitude.Value;
                    }
                }
                else
                {
                    var point = GetCoordinatesByStreet_Block(entity.Street, entity.Block);
                    entity.Latitude = point.Latitude.Value;
                    entity.Longitude = point.Longitude.Value;
                }
                entity.AddressNote = string.IsNullOrEmpty(entity.AddressNote) ? "" : entity.AddressNote;
                return base.AddAsync(entity);
            }
            catch (Exception ex)
            { 
                entity.Latitude = 0.0;
                entity.Longitude = 0.0;
        
                entity.Fk_Customer_Id = entity.Fk_Customer_Id;
                entity.AddressNote = string.IsNullOrEmpty(entity.AddressNote) ? "" : entity.AddressNote;
                return base.AddAsync(entity);
            }
        }
        public Point GetCoordinatesByPaci(int? paciNumber)
        {
            Point point = new Point();
            try
            {
                if (paciNumber.HasValue)
                {
                    PACIModel paciModel = PACIHelper.GetPACIModel(paciNumber, settings.ProxyUrl, settings.PaciServiceUrl, settings.PACIFieldNamePaciService);
                    point = new Point();
                    point.Longitude = new double?((double)paciModel.features[0].attributes.lon.Value);
                    point.Latitude = new double?((double)paciModel.features[0].attributes.lat.Value);
                }
            }
            catch (Exception ex)
            {
            }
            return point;
        }

        public Point GetCoordinatesByStreet_Block(string street, string Block)
        {
            Point point = (Point)null;
            try
            {
                if (!string.IsNullOrEmpty(street))
                {
                    if (!string.IsNullOrEmpty(Block))
                    {
                        StreetPACIResultModel streetPacItModel = PACIHelper.GetStreetPACItModel(street, Block, settings.ProxyUrl, settings.StreetServiceUrl, settings.StreetFieldNameStreetService, settings.BlockNameFieldNameStreetService);
                        if (streetPacItModel.features[0] != null)
                        {
                            point = new Point();
                            point.Latitude = new double?((double)streetPacItModel.features[0].attributes.CENTROID_Y.Value);
                            point.Longitude = new double?((double)streetPacItModel.features[0].attributes.CENTROID_X.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return point;
        }

        public new List<Location> Add(List<Location> entity)
        {
            List<Location> locationList = new List<Location>();
            foreach (Location entity1 in entity)
            {
                Location location = AddAsync(entity1);
                if (location != null)
                    locationList.Add(location);
            }
            return locationList;
        }

        public List<Location> GetByCustomerId(int customerId)
        {
            return GetAll().Where(l => l.Fk_Customer_Id == customerId).ToList();
        }

        public bool DeleteByCustomerId(int customerId)
        {
            List<Location> byCustomerId = GetByCustomerId(customerId);
            return byCustomerId == null || byCustomerId.Count <= 0 || Delete(byCustomerId);
        }

        public PACIHelper.PACIInfo GetLocationByPACI(int? paciNumber)
        {
            return PACIHelper.GetLocationByPACI(paciNumber, settings.ProxyUrl, settings.PaciServiceUrl, settings.PACIFieldNamePaciService, settings.BlockServiceUrl, settings.BlockNameFieldNameBlockService, settings.AreaNameFieldNameBlockService, settings.StreetServiceUrl, settings.BlockNameFieldNameStreetService, settings.StreetFieldNameStreetService);
        }

        public List<PACIHelper.DropPACI> GetAllGovernorates()
        {
            return PACIHelper.GetAllGovernorates(settings.ProxyUrl, settings.GovernorateServiceUrl);
        }

        public List<PACIHelper.DropPACI> GetAreas(int? govId)
        {
            return PACIHelper.GetAreas(govId, settings.GovernorateIdFieldNameAreaService, settings.ProxyUrl, settings.AreaServiceUrl);
        }

        public List<PACIHelper.DropPACI> GetStreets(int? govId, int? areaId, string blockName)
        {
            return PACIHelper.GetStreets(govId, settings.GovernorateIdFieldNameAreaService, areaId, settings.AreaIdFieldNameStreetService, blockName, settings.BlockNameFieldNameStreetService, settings.ProxyUrl, settings.StreetServiceUrl);
        }

        public List<PACIHelper.DropPACI> GetBlocks(int? areaId)
        {
            return PACIHelper.GetBlocks(areaId, settings.AreaIdFieldNameBlockService, settings.ProxyUrl, settings.BlockServiceUrl);
        }
    }


}
