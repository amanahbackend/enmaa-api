﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Identity.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Identity.Models.Entities
{
    public class WebsiteSetting : BaseEntity, IWebsiteSetting 
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
