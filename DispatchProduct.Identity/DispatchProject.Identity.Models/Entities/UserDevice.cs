﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Identity.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Identity.Models.Entities
{
    public class UserDevice 
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string DeveiceId { get; set; }
    }
}
