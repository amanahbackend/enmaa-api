﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Identity.Models.IEntities
{
    public interface IWebsiteSetting
    {
        string Key { get; set; }

        string Value { get; set; }
    }
}
