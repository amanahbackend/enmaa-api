﻿using DispatchProduct.Identity.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Identity.Models.Entities
{
    public interface IUserDevice
    {
         int Id { get; set; }
         string UserId { get; set; }
         ApplicationUser User { get; set; }
         string DeveiceId { get; set; }
    }
}
