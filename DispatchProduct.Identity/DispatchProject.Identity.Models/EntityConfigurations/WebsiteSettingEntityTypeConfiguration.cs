﻿using DispatchProduct.Identity.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Identity.EntityConfigurations
{
    public class WebsiteSettingEntityTypeConfiguration : IEntityTypeConfiguration<WebsiteSetting>
    {
        public void Configure(EntityTypeBuilder<WebsiteSetting> WebsiteSettingsConfiguration)
        {
            WebsiteSettingsConfiguration.ToTable("WebsiteSettings");
            WebsiteSettingsConfiguration.HasKey(w=>w.Key);

        }
    }
}
