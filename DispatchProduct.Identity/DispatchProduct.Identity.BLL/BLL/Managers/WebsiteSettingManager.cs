﻿using DispatchProduct.Identity.Context;
using DispatchProduct.Identity.Models.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DispatchProduct.Identity.BLL.IManagers
{
    public class WebsiteSettingManager :  IWebsiteSettingManager
    {
        private readonly ApplicationDbContext _dbContext;
        public WebsiteSettingManager(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool AddWebsiteSettingIfNotExist(WebsiteSetting WebsiteSetting)
        {
            var count = _dbContext.WebsiteSetting.Count(w=>w.Key==WebsiteSetting.Key);
            if (count > 0)
            {
                var Entity = _dbContext.WebsiteSetting.FirstOrDefault(w => w.Key == WebsiteSetting.Key);
                Entity.Value = WebsiteSetting.Value;
                if (Entity != null)   //this Key already exist
                {
                    _dbContext.WebsiteSetting.Update(Entity);
                    _dbContext.SaveChanges();
                    return true;
                }
                return false;
            }
            else
            {
              _dbContext.WebsiteSetting.Add(WebsiteSetting);
              _dbContext.SaveChanges();
               return true;
            }
        }

        public List<WebsiteSetting> Get()
        {
            return _dbContext.WebsiteSetting.ToList();
        }


        public WebsiteSetting GetByWebsiteSettingKey(string Key)
        {
            var WebsiteSetting = _dbContext.WebsiteSetting.FirstOrDefault(w=>w.Key==Key);
            return WebsiteSetting;
        }

        public bool DeleteWebsiteSetting(string Key)
        {
            var WebsiteSetting = _dbContext.WebsiteSetting.FirstOrDefault(w => w.Key.Equals(Key));
            if (WebsiteSetting != null)
            {
                _dbContext.Remove(WebsiteSetting);
                return _dbContext.SaveChanges() > 0;
            }
            return false;
        }

    }
}
