﻿using DispatchProduct.Identity.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Identity.BLL.IManagers
{
    public interface IWebsiteSettingManager
    {
        List<WebsiteSetting> Get();
        bool AddWebsiteSettingIfNotExist(WebsiteSetting WebsiteSetting);
        WebsiteSetting GetByWebsiteSettingKey(string Key);
        bool DeleteWebsiteSetting(string Key);

    }
}
