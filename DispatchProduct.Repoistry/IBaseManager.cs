﻿using DispatchProduct.Calling.Entities;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace DispatchProduct.Repoistry
{
    public interface IBaseManager<TViewModel, TEntity>
    {
        Task<List<TViewModel>> GetAllAsync();

        Task<TViewModel> AddAsync(TViewModel entity);

        Task<List<TViewModel>> AddAsync(List<TViewModel> entityLst);

        Task<bool> UpdateAsync(TViewModel entity);

        Task<bool> DeleteAsync(TViewModel entity);

        Task<bool> SoftDeleteAsync(TViewModel entity);


        Task<bool> DeleteAsync(List<TViewModel> entitylst);

        Task<bool> DeleteByIdAsync(params object[] id);



        Task<PagedResult<TViewModel>> GetAllByPaginationAsync(IQueryable<TEntity> listQuery, PaginatedItemsViewModel pagingparametermodel);

    }
}