﻿

using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Calling.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace DispatchProduct.Repoistry
{
    public class Repositry<T> : IRepositry<T> where T : class, IBaseEntity
    {
        private DbContext _context;

        public DbContext Context
        {
            get { return _context; }
        }
        private DbSet<T> _set;

        public Repositry(DbContext context)
        {
            _context = context;
            _set = _context.Set<T>();
        }



        public virtual IQueryable<T> GetAll()
        {
            return _set.Where(x => x.IsDeleted == false).OrderByDescending(x=>x.CreatedDate).AsNoTracking();
        }
        public virtual T Get(params object[] id)
        {
            return _set.Find(id);
        }
        public T Get(Expression<Func<T, bool>> predicate)
        {
            T item = null;
            item = _set.FirstOrDefault(predicate);
            return item;
        }
        public virtual T AddAsync(T entity)
        {
            T result = null;

            try
            {
                if (Validator.IsValid(entity))
                {
                    entity.IsDeleted = false;
                    entity = _set.Add(entity).Entity;
                    if (SaveChanges() > 0)
                    {
                        result = entity;
                    }
                }
                else
                {
                    StringBuilder exceptionMsgs = new StringBuilder();
                    List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                    foreach (var errmsg in errorMsgs)
                    {
                        exceptionMsgs.Append(errmsg);
                        exceptionMsgs.Append("/n");
                    }
                    throw new Exception(exceptionMsgs.ToString());
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result;

        }

        public object Add(CallLog entity)
        {
            throw new NotImplementedException();
        }

        public virtual void Add(IEnumerable<T> entityLst)
        {
            foreach (var entity in entityLst)
            {
                _set.Add(entity);
            }
            SaveChanges();
        }
        public virtual bool Update(T entity)
        {

            bool result = false;
            if (Validator.IsValid(entity))
            {
                
                _context.Entry<T>(entity).State = EntityState.Modified;
                if (SaveChanges() > 0)
                {
                    result = true;
                }
            }
            else
            {
                StringBuilder exceptionMsgs = new StringBuilder();
                List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                foreach (var errmsg in errorMsgs)
                {
                    exceptionMsgs.Append(errmsg);
                    exceptionMsgs.Append("/n");
                }
                throw new Exception(exceptionMsgs.ToString());
            }
            return result;
        }
        public virtual bool Update(IEnumerable<T> entityLst)
        {
            bool result = false;
            foreach (var entity in entityLst)
            {
                if (Validator.IsValid(entity))
                {
                    _context.Entry<T>(entity).State = EntityState.Modified;

                }

                else
                {
                    StringBuilder exceptionMsgs = new StringBuilder();
                    List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                    foreach (var errmsg in errorMsgs)
                    {
                        exceptionMsgs.Append(errmsg);
                        exceptionMsgs.Append("/n");
                    }
                    throw new Exception(exceptionMsgs.ToString());
                }
                if (SaveChanges() > 0)
                {
                    result = true;
                }
            }
            return result;
        }
        public virtual bool Delete(T entity)
        {
            //entity.IsDeleted = true;
            //Get(entity)
            //return Update(entity);
            _context.Entry<T>(entity).State = EntityState.Deleted;
            return _context.SaveChanges() > 0;
        }
        public virtual bool DeleteById(params object[] id)
        {
            T entity = _set.Find(id);
            return Delete(entity);
            //entity.IsDeleted = true;
            //return Update(entity);
        }
        public virtual bool Delete(List<T> entitylst)
        {
            bool result = false;
            if (entitylst != null && entitylst.Count > 0)
            {
                foreach (var entity in entitylst)
                {
                    result = Delete(entity);
                }
                SaveChanges();
            }
            return result;
        }
        public virtual void RollBack()
        {

        }
        public virtual void Commit()
        {


        }
        public virtual int SaveChanges()
        {
            // this method handle any exception so no need to put it in try
            BaseEntityManager.AddAuditingData(_context.ChangeTracker.Entries());
            return _context.SaveChanges();
        }
        public T Get(string property, object value)
        {
            var lambda = CreateEqualSingleExpression(property, value);

            return _set.SingleOrDefault(lambda);
        }

        public Expression<Func<T, bool>> CreateEqualSingleExpression(string property, object value)
        {

            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            var propertyExpression = Expression.Property(p, property);

            //p.Property == value
            var equalsExpression = Expression.Equal(propertyExpression, Expression.Constant(value));

            //p => p.Property == value
            return Expression.Lambda<Func<T, bool>>(equalsExpression, p);
        }

        public virtual List<T> Add(List<T> entityLst)
        {
            List<T> objList = new List<T>();
            foreach (T entity in entityLst)
                objList.Add(AddAsync(entity));
            return objList;
        }

        public bool SoftDelete(T entity)
        {
            //T entity = _set.Find(entity.id);
            //return Delete(entity);
            entity.IsDeleted = true;
            return Update(entity);
        }

        public IQueryable<T> GetAllByPagination(IQueryable<T> listQuery, PaginatedItemsViewModel pagingparametermodel, out int totalNumbers)
        {
            if (pagingparametermodel.PageNumber == 0)
                pagingparametermodel.PageNumber = 1;

            var source = listQuery.AsQueryable().AsNoTracking();

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            totalNumbers = source.Count();
            var items = source.Skip((CurrentPage - 1) * PageSize).Take(PageSize);

            return items;
        }

        public async Task<PagedResult<T>> GetAllByPaginationAsync(IQueryable<T> listQuery, PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = new PagedResult<T>();


            if (pagingparametermodel.PageNumber == 0)
                pagingparametermodel.PageNumber = 1;

            var source = listQuery.AsQueryable().AsNoTracking();

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = await source.CountAsync();//
            pagedResult.Result = source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToList();

            return pagedResult;
        }
    }


}
