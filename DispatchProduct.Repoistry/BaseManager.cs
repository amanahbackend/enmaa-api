﻿

using AutoMapper;
using AutoMapper.QueryableExtensions;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace DispatchProduct.Repoistry
{
    public class BaseManager<TViewModel, TEntity> : IBaseManager<TViewModel, TEntity>
    {

        protected readonly IRepositry<TEntity> repository;
        protected readonly DbContext context;
        protected readonly IMapper mapper;

        public BaseManager(DbContext _context, IRepositry<TEntity> _repository, IMapper _mapper)
        {
            mapper = _mapper;
            context = _context;
            repository = _repository;
        }


        public async virtual Task<TViewModel> AddAsync(TViewModel viewModel)
        {
            return await Task<TViewModel>.Run(() =>
            {

                TEntity entity = mapper.Map<TViewModel, TEntity>(viewModel);

                TEntity resultentity = repository.AddAsync(entity);

                TViewModel result = mapper.Map<TEntity, TViewModel>(resultentity);
                return result;
            });
        }

        public async virtual Task<List<TViewModel>> AddAsync(List<TViewModel> ViewModelLst)
        {
            return await Task<List<TViewModel>>.Run(() =>
            {

                List<TEntity> entity = mapper.Map<List<TViewModel>, List<TEntity>>(ViewModelLst);

                List<TEntity> resultentity = repository.Add(entity);

                List<TViewModel> result = mapper.Map<List<TEntity>, List<TViewModel>>(resultentity);
                return result;
            });
        }

        public async virtual Task<bool> DeleteAsync(TViewModel viewModel)
        {
            return await Task<List<TViewModel>>.Run(() =>
            {

                TEntity entity = mapper.Map<TViewModel, TEntity>(viewModel);

                bool result = repository.Delete(entity);

                return result;
            });
        }

        public async virtual Task<bool> DeleteAsync(List<TViewModel> ViewModelLst)
        {
            return await Task<List<TViewModel>>.Run(() =>
            {

                List<TEntity> entity = mapper.Map<List<TViewModel>, List<TEntity>>(ViewModelLst);

                bool result = repository.Delete(entity);

                return result;
            });
        }

        public async virtual Task<bool> DeleteByIdAsync(params object[] id)
        {
            return await Task<List<TViewModel>>.Run(() =>
            {

                //TEntity entity = mapper.Map<TViewModel, TEntity>(viewModel);

                bool result = repository.DeleteById(id);

                return result;
            });
        }



        public async virtual Task<List<TViewModel>> GetAllAsync()
        {
            try
            {
                return await Task.Run(() => repository.GetAll().ProjectTo<TViewModel>().ToList());

            }
            catch (Exception e)
            {

                throw e.InnerException;
            }
        }



        public async virtual Task<bool> SoftDeleteAsync(TViewModel viewModel)
        {
            return await Task<List<TViewModel>>.Run(() =>
            {

                TEntity entity = mapper.Map<TViewModel, TEntity>(viewModel);

                bool result = repository.SoftDelete(entity);

                return result;
            });
        }

        public async virtual Task<bool> UpdateAsync(TViewModel viewModel)
        {
            return await Task<TViewModel>.Run(() =>
            {

                TEntity entity = mapper.Map<TViewModel, TEntity>(viewModel);



                       bool result = repository.Update(entity);
                return result;
            });
        }


        public async Task<PagedResult<TViewModel>> GetAllByPaginationAsync(IQueryable<TEntity> listQuery, PaginatedItemsViewModel pagingparametermodel)
        {
            return await Task<PagedResult<TViewModel>>.Run(() =>
            {
                var pagedResult = new PagedResult<TViewModel>();


                if (pagingparametermodel.PageNumber == 0)
                    pagingparametermodel.PageNumber = 1;

                var source = listQuery.ProjectTo<TViewModel>();

                // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
                int CurrentPage = pagingparametermodel.PageNumber;

                // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
                int PageSize = pagingparametermodel.PageSize;
                pagedResult.TotalCount = source.Count();//
                pagedResult.Result = source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToList();

                return pagedResult;

            }
          );

        }
    }


}
