﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.UploadFile;

namespace DispatchProduct.Identity.API.ViewModel
{
    public class WebsiteSettingViewModel
    {
        public UploadFile File { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }

    }
}
