﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Identity.Entities;
using DispatchProduct.UserManagment.API;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Identity.Models.Entities
{
    public class RolePrivilgeViewModel : BaseEntity
    {
        public int Id { get; set; }
        public int FK_Privilge_Id { get; set; }
        public PrivilgeViewModel Privilge { get; set; }
        public string FK_Role_Id { get; set; }
        public ApplicationRoleViewModel Role { get; set; }
    }
}
