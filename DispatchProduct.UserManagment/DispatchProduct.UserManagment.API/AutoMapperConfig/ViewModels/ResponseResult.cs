﻿namespace DispatchProduct.UserManagment.API.ViewModels
{
    public class ResponseResult<T>
    {
        public T Data { get; set; }

        public string Message { get; set; }

        /// <summary>
        /// 1: Success; 0: Unknown; Negative values are handled.
        /// </summary>
        public int StatusCode { get; set; }

        public bool IsSucceeded => StatusCode == 1;

        public ResponseResult()
        {
            StatusCode = 1;
        }
    }
}
