﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.UserManagment.API
{
    public class UserAvailableViewModel
    {
        
        public string Id { get; set; }

        public bool IsAvailable { get; set; }
    }
}
