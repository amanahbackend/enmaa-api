﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Identity.API.ViewModel;
using DispatchProduct.Identity.BLL.IManagers;
using DispatchProduct.Identity.Models.Entities;
using DispatchProduct.Identity.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace DispatchProduct.Identity.API.Controllers
{
    [Route("api/WebsiteSetting")]
    public class WebsiteSettingController : Controller
    {
        public IWebsiteSettingManager manger;
        private AppSettings appSettings;
        private IConfiguration configuration;
        public readonly IMapper mapper;

        public WebsiteSettingController(IMapper _mapper, IConfiguration _configuration, IWebsiteSettingManager _manger, IOptions<AppSettings> _appSettings)
        {
            manger = _manger;
            mapper = _mapper;
            appSettings = _appSettings.Value;
            configuration = _configuration;
        }

        [HttpGet]
        [Route("Get")]
        public async Task<IActionResult> Get()
        {
            List<WebsiteSetting> entityResult = manger.Get().ToList();
            foreach (var item in entityResult)
            {
                if(item.Key=="image")
                    BindFilesURL(item);
            }
            return Ok(entityResult);
        }
        // GET: api/WebsiteSetting/5
        [HttpGet]
        [Route("Getbykey")]
        public WebsiteSetting Get(string Key)
        {
            WebsiteSetting entity = manger.GetByWebsiteSettingKey(Key);
            BindFilesURL(entity);
            return entity;
        }

        // POST: api/WebsiteSetting
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]WebsiteSettingViewModel WebsiteSetting)
        {
            WebsiteSetting Entity = new WebsiteSetting();

            if (WebsiteSetting.File != null)
            {
                WebsiteSetting.File.FileName = WebsiteSetting.Key.ToString() + ".jpeg";
                if (appSettings.logoPath == null)
                    appSettings.logoPath = "logo";
                UploadImageFileManager imageFileManager = new UploadImageFileManager();
                string path = $"{appSettings.logoPath}/{WebsiteSetting.Key}";
                ProcessResult<string> processResult = imageFileManager.AddFile(WebsiteSetting.File, path);
                if (!processResult.IsSucceeded)
                    return BadRequest(processResult.Exception);
                WebsiteSetting.Value = $"{path}/{WebsiteSetting.File.FileName}";
                Entity.Key = WebsiteSetting.Key;
                Entity.Value = WebsiteSetting.Value;
                manger.AddWebsiteSettingIfNotExist(Entity);
                return Ok(true);
            }
            else
            {
                Entity.Key = WebsiteSetting.Key;
                Entity.Value = WebsiteSetting.Value;
                manger.AddWebsiteSettingIfNotExist(Entity);
                return Ok(true);
            }
        }

        [HttpDelete, Route("DeleteSetting")]
        public async Task<IActionResult> DeleteSetting([FromQuery] string Key)
        {
            bool result = false;
            result = manger.DeleteWebsiteSetting(Key);
            return Ok(result);

        }

        private WebsiteSetting BindFilesURL(WebsiteSetting model)
        {
            string str = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
            if (model.Value != null)
            {
                model.Value = model.Value.Replace('\\', '/');
                model.Value = $"{str}/{model.Value}";
            }
            return model;
        }

    }
}
