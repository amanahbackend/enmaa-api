﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Inventory.IEntities;

namespace DispatchProduct.Inventory.Entities
{
    public class TechnicanUsedItems : BaseEntity, ITechnicanUsedItems, IBaseEntity
    {
        public int Id { get; set; }

        public int ItemId { get; set; }

        public Item Item { get; set; }

        public int Amount { get; set; }

        public string FK_Technican_Id { get; set; }

        public int FK_Order_Id { get; set; }

        public bool IsReleased { get; set; }
    }
}