﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Inventory.IEntities;

namespace DispatchProduct.Inventory.Entities
{
    public class Category : BaseEntity, ICategory, IBaseEntity
    {
        public string Name { get; set; }

        public int Id { get; set; }
    }
}
