﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Inventory.IEntities
{
    public interface ICategory : IBaseEntity
    {
        string Name { get; set; }

        int Id { get; set; }
    }
}
