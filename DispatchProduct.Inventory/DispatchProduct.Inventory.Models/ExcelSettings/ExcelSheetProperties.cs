﻿using System;

namespace DispatchProduct.Inventory.ExcelSettings
{
    public class ExcelSheetProperties
    {
        public string CategoryName { get; set; }

        public string Quantity { get; set; }

        public string Price { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime ExpiryDate { get; set; }

    }
}
