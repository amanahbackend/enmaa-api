﻿using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.Context;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Utilites.ProcessingResult;
using Utilities.Utilites.Paging;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public class TechnicanUsedItemsManager : Repositry<TechnicanUsedItems>, ITechnicanUsedItemsManager, IRepositry<TechnicanUsedItems>
    {
        private ITechnicanAssignedItemsManager manager;
        private IItemManager itemManger;

        public TechnicanUsedItemsManager(InventoryDbContext context, ITechnicanAssignedItemsManager _manager, IItemManager _itemManger)
          : base((DbContext)context)
        {
            manager = _manager;
            itemManger = _itemManger;
        }

        public ProcessResult<List<TechnicanUsedItems>> AddUsedItems(List<TechnicanUsedItems> items)
        {
            ProcessResult<List<TechnicanUsedItems>> result = new ProcessResult<List<TechnicanUsedItems>>();
            result.returnData = new List<TechnicanUsedItems>();
            result.MethodName = MethodBase.GetCurrentMethod().Name;
            foreach (TechnicanUsedItems technicanUsedItems in items)
            {
                ProcessResult<TechnicanUsedItems> input = AddUsedItem(technicanUsedItems);
                if (input.IsSucceeded)
                {
                    result.returnData.Add(input.returnData);
                }
                else
                {
                    result = ProcessResultMapping.Map(input, result);
                    return result;
                }
            }
            result.IsSucceeded = true;
            return result;
        }

        public ProcessResult<TechnicanUsedItems> AddUsedItem(TechnicanUsedItems item)
        {
            ProcessResult<TechnicanUsedItems> result = new ProcessResult<TechnicanUsedItems>();
            result.MethodName = MethodBase.GetCurrentMethod().Name;
            ProcessResult<TechnicanAssignedItems> input = manager.UsedItem(item.ItemId, item.Amount, item.FK_Technican_Id);
            if (input.IsSucceeded)
            {
                result.returnData = AddAsync(item);
                result.IsSucceeded = true;
            }
            else
                ProcessResultMapping.Map(input, result);
            return result;
        }

        public ProcessResult<bool> ReleaseUsedItem(int usedItemId)
        {
            ProcessResult<bool> result = new ProcessResult<bool>();
            result.MethodName = MethodBase.GetCurrentMethod().Name;
            TechnicanUsedItems entity = Get(usedItemId);
            if (entity != null)
            {
                ProcessResult<TechnicanAssignedItems> input = manager.ReleaseItems(entity.ItemId, entity.Amount, entity.FK_Technican_Id);
                if (input.IsSucceeded)
                {
                    entity.IsReleased = true;
                    Update(entity);
                    result.returnData = true;
                    result.IsSucceeded = true;
                }
                else
                    ProcessResultMapping.Map(input, result);
            }
            else
            {
                result.returnData = false;
                result.IsSucceeded = false;
                result.Message = "item not found to release";
            }
            return result;
        }

        public List<TechnicanUsedItems> Filter(FilterTechnican filter)
        {
            List<TechnicanUsedItems> list = GetAll().Where<TechnicanUsedItems>((Expression<Func<TechnicanUsedItems, bool>>)(itm => (filter.ItemIds != default(object) && filter.ItemIds.Count > 0 ? filter.ItemIds.Contains(itm.ItemId) : true) && (filter.FK_Technican_Ids != default(object) && filter.FK_Technican_Ids.Count > 0 ? filter.FK_Technican_Ids.Contains(itm.FK_Technican_Id) : true) && (filter.FK_Order_Ids != default(object) && filter.FK_Order_Ids.Count > 0 ? filter.FK_Order_Ids.Contains(itm.FK_Order_Id) : true) && (filter.AmountFrom != new int?() ? (int?)itm.Amount >= filter.AmountFrom : true) && (filter.AmountTo != new int?() ? (int?)itm.Amount <= filter.AmountTo : true) && (filter.DateFrom != new DateTime?() ? (DateTime?)itm.CreatedDate >= filter.DateFrom : true) && (filter.DateTo != new DateTime?() ? (DateTime?)itm.CreatedDate <= filter.DateTo : true))).ToList<TechnicanUsedItems>();
            //foreach (TechnicanUsedItems technicanUsedItems in list)
            //    technicanUsedItems.Item = itemManger.Get(technicanUsedItems.ItemId);
            return list;
        }


        public async System.Threading.Tasks.Task<PagedResult<TechnicanUsedItems>> FilterPagingAsync(FilterTechnican filter)
        {
            var query  = GetAll().Where<TechnicanUsedItems>((Expression<Func<TechnicanUsedItems, bool>>)(itm => (filter.ItemIds != default(object) && filter.ItemIds.Count > 0 ? filter.ItemIds.Contains(itm.ItemId) : true) && (filter.FK_Technican_Ids != default(object) && filter.FK_Technican_Ids.Count > 0 ? filter.FK_Technican_Ids.Contains(itm.FK_Technican_Id) : true) && (filter.FK_Order_Ids != default(object) && filter.FK_Order_Ids.Count > 0 ? filter.FK_Order_Ids.Contains(itm.FK_Order_Id) : true) && (filter.AmountFrom != new int?() ? (int?)itm.Amount >= filter.AmountFrom : true) && (filter.AmountTo != new int?() ? (int?)itm.Amount <= filter.AmountTo : true) && (filter.DateFrom != new DateTime?() ? (DateTime?)itm.CreatedDate >= filter.DateFrom : true) && (filter.DateTo != new DateTime?() ? (DateTime?)itm.CreatedDate <= filter.DateTo : true)));

            return await   base.GetAllByPaginationAsync(query, filter.pagingParameterModel);
   
        }

        public List<TechnicanUsedItems> GetUsedItemsByTechnican(string technicanId)
        {
            List<TechnicanUsedItems> list = GetAll().Where(itm => itm.FK_Technican_Id == technicanId).Include(x=>x.Item).ToList();




            return list;
        }



        public async System.Threading.Tasks.Task<PagedResult<TechnicanUsedItems>> GetUsedItemsByTechnicanPagedAsync(PaginatedItemsViewModel pagingParameterModel)
        {
            var  Querylist = GetAll().Where(itm => itm.FK_Technican_Id == pagingParameterModel.SearchBy).Include(x => x.Item);

           return await base.GetAllByPaginationAsync(Querylist, pagingParameterModel);
        }
    }
}
