﻿using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilities.Utilites.Paging;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public interface ITechnicanUsedItemsManager : IRepositry<TechnicanUsedItems>
    {
        ProcessResult<List<TechnicanUsedItems>> AddUsedItems(List<TechnicanUsedItems> items);

        ProcessResult<TechnicanUsedItems> AddUsedItem(TechnicanUsedItems item);

        List<TechnicanUsedItems> GetUsedItemsByTechnican(string technicanId);

        ProcessResult<bool> ReleaseUsedItem(int usedItemId);

        List<TechnicanUsedItems> Filter(FilterTechnican filter);

        Task<PagedResult<TechnicanUsedItems>> FilterPagingAsync(FilterTechnican filter);

        Task<PagedResult<TechnicanUsedItems>> GetUsedItemsByTechnicanPagedAsync(PaginatedItemsViewModel pagingParameterModel);
    }
}
