﻿using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.IManagers
{
    public interface IItemManager : IRepositry<Item>
    {
        Task<Item> GetByCode(string Code);

        ProcessResult<bool> DecreaseAssignedAmount(int amount, int itemId);
    }
}
