﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchingProduct.Inventory.BLL.Managers;
using DispatchProduct.Inventory.API.ViewModel;
using DispatchProduct.Inventory.Entities;
using Utilites.ProcessingResult;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Paging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class TechnicanAssignedItemsController : Controller
    {
        public ITechnicanAssignedItemsManager manger;
        public readonly IMapper mapper;
        IItemManager itemManger;
        public TechnicanAssignedItemsController(ITechnicanAssignedItemsManager _manger, IMapper _mapper, IItemManager _itemManger)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            itemManger = _itemManger;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
           TechnicanAssignedItemsViewModel result = new TechnicanAssignedItemsViewModel();
            TechnicanAssignedItems entityResult = new TechnicanAssignedItems();
            entityResult = manger.Get(id);
            result = mapper.Map<TechnicanAssignedItems, TechnicanAssignedItemsViewModel>(entityResult);
            return Ok(result);
        }
        [HttpGet]
        public IActionResult Get()
        {
            List<TechnicanAssignedItemsViewModel> result = new List<TechnicanAssignedItemsViewModel>();
            List<TechnicanAssignedItems> entityResult = new List<TechnicanAssignedItems>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<TechnicanAssignedItems>, List<TechnicanAssignedItemsViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]TechnicanAssignedItemsViewModel model)
        {
            var entityResult = mapper.Map<TechnicanAssignedItemsViewModel, TechnicanAssignedItems>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.AddAsync(entityResult);
            var result = mapper.Map<TechnicanAssignedItems, TechnicanAssignedItemsViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]TechnicanAssignedItemsViewModel model)
        {
            bool result = false;
            TechnicanAssignedItems entityResult = new TechnicanAssignedItems();
            entityResult = mapper.Map<TechnicanAssignedItemsViewModel, TechnicanAssignedItems>(model);
           // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            TechnicanAssignedItems entity = manger.Get(id);
           // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("AssignItem")]
        public IActionResult AssignItem([FromBody] TechnicanAssignedItemsViewModel model)
        {
            TechnicanAssignedItemsController assignedItemsController = this;
            TechnicanAssignedItems technicanAssignedItems = mapper.Map<TechnicanAssignedItemsViewModel, TechnicanAssignedItems>(model);
            ProcessResult<TechnicanAssignedItems> processResult = manger.AssignItem(technicanAssignedItems);
            if (!processResult.IsSucceeded)
                return BadRequest(processResult);
            TechnicanAssignedItemsViewModel assignedItemsViewModel = mapper.Map<TechnicanAssignedItems, TechnicanAssignedItemsViewModel>(processResult.returnData);
            return Ok(assignedItemsViewModel);
        }

        [HttpPost]
        [Route("AssignItems")]
        public IActionResult AssignItems([FromBody] List<TechnicanAssignedItemsViewModel> model)
        {
            TechnicanAssignedItemsController assignedItemsController = this;
            List<TechnicanAssignedItems> list = mapper.Map<List<TechnicanAssignedItemsViewModel>, List<TechnicanAssignedItems>>(model);
            ProcessResult<List<TechnicanAssignedItems>> processResult = manger.AssignItems(list);
            if (!processResult.IsSucceeded)
                return BadRequest(processResult);
            List<TechnicanAssignedItemsViewModel> assignedItemsViewModelList = mapper.Map<List<TechnicanAssignedItems>, List<TechnicanAssignedItemsViewModel>>(processResult.returnData);
            return Ok(assignedItemsViewModelList);
        }

        [HttpGet]
        [Route("GetAssignedItemsByTechnican/{technicanId}")]
        public async Task<IActionResult> GetAssignedItemsByTechnicanAsync([FromRoute] string technicanId)
        {
          return await Task.Run(() => {

                   List<TechnicanAssignedItems> itemsByTechnican = manger.GetAssignedItemsByTechnican(technicanId);
                   List<TechnicanAssignedItemsViewModel> assignedItemsViewModelList = mapper.Map<List<TechnicanAssignedItems>, List<TechnicanAssignedItemsViewModel>>(itemsByTechnican);
                   return Ok(assignedItemsViewModelList);
         });

        }


        [HttpPost]
        [Route("GetAssignedItemsByTechnicanPaging")]
        public async Task<IActionResult> GetUsedItemsByTechnican([FromBody] PaginatedItemsViewModel pagingParameterModel)
        {
            return await Task.Run(async () =>
            {
                PagedResult<TechnicanAssignedItemsViewModel> pagedResult = new PagedResult<TechnicanAssignedItemsViewModel>();

                var itemsByTechnican = await manger.GetAssignedItemsByTechnicanPagedAsync(pagingParameterModel);
                List<TechnicanAssignedItemsViewModel> result = mapper.Map<List<TechnicanAssignedItems>, List<TechnicanAssignedItemsViewModel>>(itemsByTechnican.Result);

                pagedResult.Result = result;
                pagedResult.TotalCount = itemsByTechnican.TotalCount;
                return Ok(pagedResult);
            });
        }


        [HttpPost]
        [Route("ChekItemExistForTechnican")]
        public  IActionResult ChekItemExistForTechnican([FromBody] TechnicanAssignedItemsViewModel model)
        {
            bool result = manger.IsItemExistForTechnican(model.FK_Technican_Id, model.ItemId);
            Item source = null;
            if (!result)
            {
                Item obj = itemManger.Get(model.ItemId);
                return Ok(obj);
            }
            mapper.Map<Item, ItemViewModel>(source);
            return Ok(result);
        }
    }

}

