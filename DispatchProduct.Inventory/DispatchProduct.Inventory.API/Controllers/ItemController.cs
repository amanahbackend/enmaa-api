﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchingProduct.Inventory.BLL.Managers;
using DispatchProduct.Inventory.API.ViewModel;
using DispatchProduct.Inventory.Entities;
using Utilities.Utilites;
using Utilites.UploadFile;
using Microsoft.AspNetCore.Hosting;
using Utilites.ExcelToGenericList;
using DispatchProduct.Inventory.BLL.IManagers;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using AutoMapper.QueryableExtensions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class ItemController : Controller
    {
        private readonly IHostingEnvironment _hostingEnv;
        public IItemManager manger;
        public readonly IMapper mapper;
        IExcelItemsManager excelItemsManager;
        public ItemController(IItemManager _manger, IMapper _mapper,IHostingEnvironment hostingEnv , IExcelItemsManager _excelItemsManager)
        {
            _hostingEnv = hostingEnv;
            excelItemsManager = _excelItemsManager;
            this.manger = _manger;
            this.mapper = _mapper;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            return await Task.Run(() =>
            {
                ItemViewModel result = new ItemViewModel();
                Item entityResult = new Item();
                entityResult = manger.Get(id);
                result = mapper.Map<Item, ItemViewModel>(entityResult);
                return Ok(result);
            });

        }
        [HttpGet]
        public async  Task<IActionResult> Get()
        {
          return    await  Task.Run(() =>
            {
                List<ItemViewModel> result = new List<ItemViewModel>();
                result = manger.GetAll().ProjectTo<ItemViewModel>().ToList();
                return Ok(result);                                             
            });
        }





        [Route("GetAllByPaging")]
        [HttpPost]
        public async Task<IActionResult> GetAllByPaging([FromBody]PaginatedItemsViewModel pagingParameterModel)
        {

            var entityResult = await manger.GetAllByPaginationAsync(manger.GetAll() ,pagingParameterModel);
            return Ok(entityResult);

        }



        [HttpGet("{code}")]
        [Route("GetByCode/{code}")]
        public async Task<IActionResult> GetByCodeAsync(string code)
        {


            ItemViewModel result = new ItemViewModel();
            Item entityResult = new Item();
            entityResult =await manger.GetByCode(code);
            result = mapper.Map<Item, ItemViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]ItemViewModel model)
        {
            ItemViewModel result = new ItemViewModel();
            Item entityResult = new Item();
            entityResult = mapper.Map<ItemViewModel, Item>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.AddAsync(entityResult);
            result = mapper.Map<Item, ItemViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]ItemViewModel model)
        {
          return await  Task.Run(() =>
            {
                bool result = false;
                Item entityResult = new Item();
                entityResult = mapper.Map<ItemViewModel, Item>(model);
                result = manger.Update(entityResult);
                return Ok(result);
            }
            );
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {

            return await Task.Run(() =>
            {
                bool result = false;
                Item entity = manger.Get(id);
                // await fillEntityIdentity(entity);
                result = manger.Delete(entity);
                return Ok(result);
            });

            }
        #endregion
        #endregion

        [HttpPost]
        [Route("ImportItemFromFile"), AllowAnonymous]
        public async Task<IActionResult> ImportTechFromFile([FromBody]UploadFile file)
        {
            return await Task.Run(() =>
            {


                var fileExt = StringUtilities.GetFileExtension(file.FileContent);
                file.FileName = $"Tech_{DateTime.Now.Ticks}.{fileExt}";
                UploadExcelFileManager fileManager = new UploadExcelFileManager();
                var path = $@"{_hostingEnv.WebRootPath}\Import\";
                var addFileResult = fileManager.AddFile(file, path);
                IList<ExcelItem> models = ExcelReader.GetDataToList($@"{addFileResult.returnData}\{file.FileName}", excelItemsManager.GetItems);
                var result = excelItemsManager.SaveItems(models);

                return Ok(result);
            });
        }

    }
}
