﻿using AutoMapper;
using DispatchProduct.Inventory.API.ViewModel;
using DispatchProduct.Inventory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Inventory.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<Category, CategoryViewModel>(MemberList.None);
            CreateMap<CategoryViewModel, Category>(MemberList.None);

            CreateMap<FilterTechnicanViewModel, FilterTechnican>(MemberList.None);
            CreateMap<FilterTechnican, FilterTechnicanViewModel>(MemberList.None);

            CreateMap<TransferedItems, TransferedItemsViewModel>(MemberList.None)
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));

            CreateMap<TransferedItemsViewModel, TransferedItems>(MemberList.None)
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));


            CreateMap<TechnicanAssignedItems, TechnicanAssignedItemsViewModel>(MemberList.None)
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));

            CreateMap<TechnicanAssignedItemsViewModel, TechnicanAssignedItems>(MemberList.None)
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));


            CreateMap<TechnicanUsedItems, TechnicanUsedItemsViewModel>(MemberList.None)
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item))
            .ForMember(dest => dest.Order, opt => opt.Ignore());

            CreateMap<TechnicanUsedItemsViewModel, TechnicanUsedItems>(MemberList.None)
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));


            CreateMap<Item, ItemViewModel>(MemberList.None)
            .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category));
            CreateMap<ItemViewModel, Item>(MemberList.None)
            .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category));
        }
    }
}
