﻿using DispatchProduct.Ordering.BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.BLL.ServicesViewModels.Calling
{
    public class CallLogViewModel : BaseEntityViewModel
    {
        public int FK_CallStatus_Id { get; set; }
        //public CallStatus CallStatus { get; set; }
        public string CustomerServiceName { get; set; }
        public string CustomerServiceUserName { get; set; }
        public string CustomerServiceComment { get; set; }
        public int Id { get; set; }
        public int FK_Call_Id { get; set; }
    }
}
