﻿using DispatchProduct.Ordering.BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.BLL.ServicesViewModels.Calling
{
    public class CallPriorityViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
