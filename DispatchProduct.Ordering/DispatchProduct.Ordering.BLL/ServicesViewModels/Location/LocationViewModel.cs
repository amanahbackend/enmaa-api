﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.BLL.ServicesViewModels.LocationViewModel
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using DispatchProduct.Ordering.BLL.ViewModel;

namespace DispatchProduct.Ordering.BLL.ServicesViewModels
{
  public class LocationViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public string PACINumber { get; set; }

    public string Title { get; set; }

    public string Governorate { get; set; }

    public string Area { get; set; }

    public string Block { get; set; }

    public string Street { get; set; }

    public string AddressNote { get; set; }

    public double Latitude { get; set; }

    public double Longitude { get; set; }

    public int Fk_Customer_Id { get; set; }
  }
}
