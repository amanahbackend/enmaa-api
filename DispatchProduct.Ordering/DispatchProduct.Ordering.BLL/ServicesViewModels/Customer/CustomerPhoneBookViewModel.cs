﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.BLL.ServicesViewModels.CustomerPhoneBookViewModel
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using DispatchProduct.Ordering.BLL.ViewModel;

namespace DispatchProduct.Ordering.BLL.ServicesViewModels
{
  public class CustomerPhoneBookViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public int FK_Customer_Id { get; set; }

    public string Phone { get; set; }

    public int FK_PhoneType_Id { get; set; }
  }
}
