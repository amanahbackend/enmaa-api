﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.BLL.ServicesViewModels.Dropout
{
    public class SendNotificationViewModel
    {
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public dynamic Data { get; set; }
    }
}
