﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Ordering.BLL.Enums
{
    public enum OrderStatuses
    {
        Open=1,
        Mounted=2,
        CustomerNotExist=3,
        Cancelled=4,
        Completed=5,
    }
}
