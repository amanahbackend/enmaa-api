﻿using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.Models.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class DispatureAreasAndProplemsManager : Repositry<DispatureAreasAndProplems>, IDispatureAreasAndProplemsManager
    {
        IOrderProblemManager orderManager;
        public DispatureAreasAndProplemsManager(OrderDbContext context, IOrderProblemManager _orderManager)
            : base(context)
        {
            orderManager = _orderManager;
        }
        //public List<DispatureAreasAndProplems> AssignCriteriaToDispatcher(List<DispatureAreasAndProplems> list)
        //{
        //    foreach (var item in list)
        //    {
        //        Add(item);
        //        item.OrderProblem = orderManager.Get(item.FK_OrderProblem_Id);
        //    }
        //    return list;
        //}
        //public List<DispatureAreasAndProplems> GetAllDistribution()
        //{
        //    var lstOrderDistribution = base.GetAll().ToList();
        //    foreach (var item in lstOrderDistribution)
        //    {
        //        item.OrderProblem = orderManager.Get(item.FK_OrderProblem_Id);
        //    }
        //    return lstOrderDistribution;
        //}


        /// <summary>
        /// get proplem name and area and return randome dispature id match this criteria 
        /// </summary>
        /// <param name="proplem">proplem name</param>
        /// <param name="area"> area name </param>
        /// <returns></returns>
        public string GetDispatcherIdForOrder(int  proplemid, string area)
        {
            string result = null;
            var dispatchers = GetAll().Where(Disp => HasProblem(Disp.Proplems, proplemid) && Disp.Areas.ToLower().Contains(area.ToLower()));
            if (dispatchers.Count() > 0)
            {
                Random rand = new Random();
                int toSkip = rand.Next(0, dispatchers.Count() - 1);
                result = dispatchers.Skip(toSkip).Take(1).FirstOrDefault().FK_Dispatcher_Id;
            }
            return result;
        }

         /// <summary>
         /// Predicat for searching for proplem id 
         /// </summary>
         /// <param name="DispatureProplems"></param>
         /// <param name="ProplemId"></param>
         /// <returns></returns>
        private bool HasProblem(string DispatureProplems ,  int ProplemId)
        {
            try
            {
                var proplemsList = DispatureProplems.Split(",");
                foreach(string DispatureProplem in proplemsList)
                {
                    int DispatureProblemId=   Convert.ToInt32(DispatureProplem);
                    if(DispatureProblemId == ProplemId)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch(Exception e)
            {
                return false;
            }  
        }








        //public List<DispatureAreasAndProplems> GetByDispatcherId(string dispatcherId)
        //{
        //    var lstOrderDistribution = GetAll().Where(ordr => ordr.FK_Dispatcher_Id == dispatcherId).ToList();
        //    foreach (var item in lstOrderDistribution)
        //    {
        //        item.OrderProblem = orderManager.Get(item.FK_OrderProblem_Id);
        //    }
        //    return lstOrderDistribution;
        //}
        //public bool DeleteByDispatcherId(string dispatcherId)
        //{
        //    bool result = false;
        //    var items = GetAll().Where(crit => crit.FK_Dispatcher_Id == dispatcherId).ToList();
        //    if (items != null && items.Count > 0)
        //    {
        //        result = base.Delete(items);
        //    }
        //    else
        //    {
        //        result = true;
        //    }
        //    return result;
        //}
    }
}
