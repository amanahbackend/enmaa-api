﻿using AutoMapper;
using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.BLL.ViewModel;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class AssignedTechnicansManager : BaseManager<AssignedTechnicansViewModel, AssignedTechnicans>, IAssignedTechnicansManager
    {
        public AssignedTechnicansManager(OrderDbContext context, IRepositry<AssignedTechnicans> _repository, IMapper _mapper)
            : base(context, _repository, _mapper)
        {
        }


        public async Task<List<string>> GetByDispatcherIdAsync(string fk_Dispatcher_Id)
        {

            var technicans = await Task.Run(() =>
            {
              return     repository.GetAll().Where(t => t.FK_Dispatcher_Id == fk_Dispatcher_Id).Select(x=>x.FK_Technican_Id).ToList();
            }

         );

            return technicans;
        }



        public override async Task<AssignedTechnicansViewModel> AddAsync(AssignedTechnicansViewModel entity)
        {
            var technicanViewModel = await GetByTechnicanIdAsync(entity.FK_Technican_Id);
            if (technicanViewModel != null)
            {
                technicanViewModel.FK_Dispatcher_Id = entity.FK_Dispatcher_Id;
                await base.UpdateAsync(technicanViewModel);
            }
            else
            {



                technicanViewModel = await base.AddAsync(entity);
            }
            return technicanViewModel;
        }



        public async Task<bool> UpdateAssignedTechnicanAsync(AssignedTechnicansViewModel assignedTechnicansViewModel)
        {
            var technicanView = await GetByTechnicanIdAsync(assignedTechnicansViewModel.FK_Technican_Id);

            technicanView.FK_Dispatcher_Id = assignedTechnicansViewModel.FK_Dispatcher_Id;

            return await base.UpdateAsync(technicanView);
        }



        public async Task<bool> DeleteAssignedTechnicanAsync(string technicanId)
        {
         
            return await Task.Run(()=> repository.Delete(repository.GetAll().Where(t => t.FK_Technican_Id == technicanId).ToList()));
        }

        public async  Task<List<string>> GetAllTecIdsAsync()
        {
            return await repository.GetAll().Select(x => x.FK_Technican_Id).ToListAsync();
        }


        public async Task<AssignedTechnicansViewModel> GetByTechnicanIdAsync(string technicanId)
        {
            return await Task<AssignedTechnicansViewModel>.Run(() => repository.GetAll()
            .Where(t => t.FK_Technican_Id == technicanId)
            .ProjectTo<AssignedTechnicansViewModel>().FirstOrDefaultAsync());
        }

        public async  Task<AssignedTechnicansViewModel> GetByIdAsync(int id)
        {
            return await (context as OrderDbContext).AssignedTechnicans.ProjectTo<AssignedTechnicansViewModel>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
        }



    }

}
