﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.BLL.Enums;
using DispatchProduct.Ordering.BLL.Filters;
using DispatchProduct.Ordering.BLL.ViewModel;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.Settings;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilites;
using Utilities.Utilites.Paging;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class OrderManager : BaseManager<OrderViewModel, Order>, IOrderManager
    {
        private OrderAppSettings ordersettings;
        private IOrderTypeManager orderTypeMnger;
        private IOrderStatusManager orderStatusManager;
        private IOrderPriorityManager orderPriorityManager;
        private IOrderProblemManager orderProblemManager;
        private IOrderProgressManager orderProgressManager;
        private IOrderFilesManager orderFilesManager;
        private IProgressStatusManager progressStatusManager;

        public OrderManager(OrderDbContext context, IRepositry<Order> _repository, IMapper _mapper, IOptions<OrderAppSettings> _ordersettings, IOrderTypeManager _orderTypeMnger, IOrderStatusManager _orderStatusManager, IOrderPriorityManager _orderPriorityManager, IOrderProgressManager _orderProgressManager, IProgressStatusManager _progressStatusManager, IOrderProblemManager _orderProblemManager, IOrderFilesManager _orderFilesManager)
          : base((DbContext)context, _repository, _mapper)
        {
            ordersettings = _ordersettings.Value;
            orderStatusManager = _orderStatusManager;
            orderTypeMnger = _orderTypeMnger;
            orderPriorityManager = _orderPriorityManager;
            orderProgressManager = _orderProgressManager;
            progressStatusManager = _progressStatusManager;
            orderFilesManager = _orderFilesManager;
            orderProblemManager = _orderProblemManager;

        }

        public override async Task<OrderViewModel> AddAsync(OrderViewModel order)
        {
            order.Code = await CreateRefNumberAsync();
            OrderStatus intialOrderStatus = orderStatusManager.GetIntialOrderStatus();
            if (intialOrderStatus != null)
                order.FK_OrderStatus_Id = intialOrderStatus.Id;
            order = await base.AddAsync(order);
            //order = fillOrderNavProps(order);
            return order;
        }

        public override async Task<List<OrderViewModel>> GetAllAsync()
        {
            return await Task<List<OrderViewModel>>.Run(() =>
            {
                var result = repository.GetAll().Where(x => x.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled && x.FK_OrderStatus_Id != (int)OrderStatuses.Completed).ProjectTo<OrderViewModel>().AsNoTracking().ToList();

                return result;
            });

        }

        public async Task<OrderViewModel> GetAsync(int id)
        {

            return await Task<OrderViewModel>.Run(() =>
                  {
                      var result = (context as OrderDbContext).Order.ProjectTo<OrderViewModel>().Where(x => x.Id == id).AsNoTracking().FirstOrDefault();

                      return result;

                  });
        }

        //public List<Order> GetAllOrderFilledProps()
        //{
        //    List<int> statusIds = orderStatusManager.GetAll()
        //        .Where(st => st.Name == ordersettings.OrderCancelStatus || st.Name == ordersettings.OrderCompleteStatus)
        //        .Select(st => st.Id).ToList();
        //    List<Order> list = base.GetAll().Where(ord => !statusIds.Contains(ord.FK_OrderStatus_Id)).ToList();
        //    FillOrdersNavProps(list);
        //    return list;
        //}
        public async Task<PagedResult<OrderViewModel>> GetOrdersBySearchAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            var orderListQuery = repository.GetAll().Where(ord => ord.Code.Contains(pagingparametermodel.SearchBy)).AsQueryable();

            var result = await base.GetAllByPaginationAsync(orderListQuery, pagingparametermodel);
            //var result = FillOrdersNavProps(lstQuery.ToList());
            return result;
        }

        public async Task<PagedResult<OrderViewModel>> GetAllOrderFilledPropsByPaging(PaginatedItemsViewModel pagingparametermodel)
        {
            //var statusIds = orderStatusManager.GetAll()
            //    .Where(st => st.Name == ordersettings.OrderCancelStatus || st.Name == ordersettings.OrderCompleteStatus)
            //    .Select(st => st.Id).ToList();
            var orderListQuery =repository.GetAll().Where(x => x.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled &&  x.FK_OrderStatus_Id != (int)OrderStatuses.Completed);

            var OrderResultQuery =  await base.GetAllByPaginationAsync(orderListQuery, pagingparametermodel);
            return OrderResultQuery;
        }

        public async Task<List<OrderViewModel>> GetFilteredOrderByDispatcherId(FilterOrderByDispatcher filter)
        {
            return await Task<List<OrderViewModel>>.Run(() =>
             {
                 var ordersQuery = repository.GetAll().Where(ord =>
                 (ord.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled && ord.FK_OrderStatus_Id != (int)OrderStatuses.Completed) &&
                 (filter.FK_Dispatcher_Ids != null && filter.FK_Dispatcher_Ids.Count > 0 ? filter.FK_Dispatcher_Ids.Contains(ord.FK_Dispatcher_Id) : true)
                &&
                (filter.FK_OrderProblem_Ids != null && filter.FK_OrderProblem_Ids.Count > 0 ? filter.FK_OrderProblem_Ids.Contains(ord.FK_OrderProblem_Id) : true)
                &&
                (filter.FK_OrderStatus_Ids != null && filter.FK_OrderStatus_Ids.Count > 0 ? filter.FK_OrderStatus_Ids.Contains(ord.FK_OrderStatus_Id) : true)
                &&
                (filter.FK_Technican_Ids != null && filter.FK_Technican_Ids.Count > 0 ? filter.FK_Technican_Ids.Contains(ord.FK_Technican_Id) : true)
                &&
                (filter.Areas != null && filter.Areas.Count > 0 ? filter.Areas.Contains(ord.Area) : true)
                &&
                (filter.CreatedDateFrom != new DateTime?() ? (DateTime?)ord.CreatedDate >= filter.CreatedDateFrom : true)
                &&
                (filter.CreatedDateTo != new DateTime?() ? (DateTime?)ord.CreatedDate <= filter.CreatedDateTo : true));

                 var result = ordersQuery.ProjectTo<OrderViewModel>().ToList();
                 return result;
             });
        }


        public async Task<PagedResult<OrderViewModel>> GetFilteredOrderByDispatcherIdPagginated(PaginatedItemsViewModel pagingparametermodel, FilterOrderByDispatcher filter)
        {
            return await Task<PagedResult<OrderViewModel>>.Run(async () =>
            {
                var ordersQuery = repository.GetAll().Where(ord =>
                                 (ord.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled && ord.FK_OrderStatus_Id != (int)OrderStatuses.Completed) &&
                (filter.FK_Dispatcher_Ids != null && filter.FK_Dispatcher_Ids.Count > 0 ? filter.FK_Dispatcher_Ids.Contains(ord.FK_Dispatcher_Id) : true)
               &&
               (filter.FK_OrderProblem_Ids != null && filter.FK_OrderProblem_Ids.Count > 0 ? filter.FK_OrderProblem_Ids.Contains(ord.FK_OrderProblem_Id) : true)
               &&
               (filter.FK_OrderStatus_Ids != null && filter.FK_OrderStatus_Ids.Count > 0 ? filter.FK_OrderStatus_Ids.Contains(ord.FK_OrderStatus_Id) : true)
               &&
               (filter.FK_Technican_Ids != null && filter.FK_Technican_Ids.Count > 0 ? filter.FK_Technican_Ids.Contains(ord.FK_Technican_Id) : true)
               &&
               (filter.Areas != null && filter.Areas.Count > 0 ? filter.Areas.Contains(ord.Area) : true)
               &&
               (filter.CreatedDateFrom != new DateTime?() ? (DateTime?)ord.CreatedDate >= filter.CreatedDateFrom : true)
               &&
               (filter.CreatedDateTo != new DateTime?() ? (DateTime?)ord.CreatedDate <= filter.CreatedDateTo : true));
              
                var result = await base.GetAllByPaginationAsync(ordersQuery, pagingparametermodel) ;
                return result;
            });
        }



        public async Task<List<OrderViewModel>> GetAllOrdersNavigatedAsync()
        {
            return await GetAllAsync();
        }


        public async Task<OrderProgress> AddOrderProgressAsync(OrderProgress orderProgress)
        {

           var  entity = await (context as OrderDbContext).Order.Where(x => x.Id == orderProgress.FK_Order_Id).AsNoTracking().FirstOrDefaultAsync();

            orderProgress.UpdatedDate = DateTime.Now;
            OrderProgress result = orderProgressManager.AddAsync(orderProgress);
            entity.FK_OrderStatus_Id = orderProgress.FK_ProgressStatus_Id;

            var r= (context as OrderDbContext).Order.Update(entity);
            (context as OrderDbContext).SaveChanges();
            (context as OrderDbContext).Entry(entity).State = EntityState.Detached;

            return result;
        }

        public async Task<List<OrderViewModel>> GetOrdersAssignedToTechnicans(List<string> technicanIds)
        {
            return await Task<List<OrderViewModel>>.Run(() =>
            {
                var result = repository.GetAll().Where(x => (x.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled && x.FK_OrderStatus_Id != (int)OrderStatuses.Completed) && technicanIds.Contains(x.FK_Technican_Id)).ProjectTo<OrderViewModel>().ToList();

                return result;
            });
        }

        public async Task<List<OrderViewModel>> GetOrdersAssignedToTechnican(string technicanId)
        {
            return await Task<List<OrderViewModel>>.Run(() =>
            {
                var result = repository.GetAll().Where(x => ( x.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled && x.FK_OrderStatus_Id != (int)OrderStatuses.Completed) && x.FK_Technican_Id == technicanId).ProjectTo<OrderViewModel>().ToList();

                return result;
            });

        }

        public async Task<List<OrderViewModel>> GetDispatcherOrders(string dispatcherId)
        {

            return await Task<List<OrderViewModel>>.Run(() =>
            {
                var result = repository.GetAll().
                Where(ord =>
                (
                 (ord.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled || ord.FK_OrderStatus_Id != (int)OrderStatuses.Completed) &&
                ord.FK_Dispatcher_Id == dispatcherId || ord.FK_Dispatcher_Id == null)
                &&
                ord.FK_Technican_Id == null
                ).ProjectTo<OrderViewModel>().ToList();

                return result;
            });


        }

        public async Task<List<OrderViewModel>> GetAllDispatcherOrders(string dispatcherId)
        {

            return await Task<List<OrderViewModel>>.Run(() =>
            {
                var result = repository.GetAll().Where(ord => (ord.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled && ord.FK_OrderStatus_Id != (int)OrderStatuses.Completed) && ord.FK_Dispatcher_Id == dispatcherId).ProjectTo<OrderViewModel>().ToList();

                return result;
            });

        }

        public async Task<bool> AssignOrderToTechnican(int orderId, string technicanId)
        {
            return await Task<bool>.Run(() =>
            {

                bool result = false;
                var entity = repository.Get(orderId);

                if (entity != null)
                {
                    entity.FK_Technican_Id = technicanId;


                    OrderProgress orderProgress = orderProgressManager.AddAssignedProgress(entity);
                    if (orderProgress != null)
                    {
                        ProgressStatus progressStatus = progressStatusManager.Get(orderProgress.FK_ProgressStatus_Id);
                        if (progressStatus != null)
                            entity.FK_OrderStatus_Id = progressStatus.FK_OrderStatus_Id;
                    }

                    result = repository.Update(entity);
                }
                return result;
            });
        }

        public async  Task<string> UnAssignOrderToTechnican(int orderId)
        {

            return await Task<string>.Run(() =>
            {

                string result = "";
                Order entity = repository.Get(ord => ord.Id == orderId);
                if (entity != null)
                {
                    result = entity.FK_Technican_Id;
                    entity.FK_Technican_Id = null;
                    OrderProgress orderProgress = orderProgressManager.AddOpenProgress(entity);
                    if (orderProgress != null)
                    {
                        ProgressStatus progressStatus = progressStatusManager.Get(orderProgress.FK_ProgressStatus_Id);
                        if (progressStatus != null)
                            entity.FK_OrderStatus_Id = progressStatus.FK_OrderStatus_Id;
                    }
                    repository.Update(entity);
                }
                return result;
            });

          }

        public async Task<bool> AssignOrderToDispatcher(int orderId, string dispatcherId)
        {
            return await Task<bool>.Run(() =>
            {
                bool result = false;
                Order entity =repository.Get(ord => ord.Id == orderId);
                if (entity != null)
                {
                    entity.FK_Dispatcher_Id = dispatcherId;
                    result = repository.Update(entity);
                }
                return result;
            }); 
            
         }


        public async Task<bool> SetPreferedVisitTime(int orderId, DateTime PreferedVisitTime)
        {
            return await Task<bool>.Run(() =>
            {
                bool result = false;
                Order entity = repository.Get(ord => ord.Id == orderId);
                if (entity != null)
                {
                    entity.PreferedVisitTime = PreferedVisitTime;
                    result = repository.Update(entity);
                }
                return result;
            });
        }

        public async Task<bool> TransferOrderToDispatcher(int orderId, string dispatcherId)
        {
            return await Task<bool>.Run(() =>
            {
                bool result = false;
                Order entity = repository.Get(ord => ord.Id == orderId);
                if (entity != null)
                {
                    entity.FK_Dispatcher_Id = dispatcherId;
                    entity.FK_Technican_Id = null;
                    result =repository.Update(entity);
                }
                return result;
            });
        
        }

        public async Task<bool> AssignOrderToDispatcher_Technican(int orderId, string dispatcherId, string technicanId)
        {
            return await Task<bool>.Run(() =>
            {
                bool result = false;
                Order entity =repository.Get(ord => ord.Id == orderId);
                if (entity != null)
                {
                    entity.FK_Dispatcher_Id = dispatcherId;
                    entity.FK_Technican_Id = technicanId;
                    result = repository.Update(entity);
                }
                return result;
            });
      }
        public async Task<bool> AssignOrdersToDispatcher(List<int> orderIds, string dispatcherId)
        {
            return await Task<bool>.Run(() =>
            {
                bool result = false;
                List<Order> list = repository.GetAll().Where(ord => orderIds.Contains(ord.Id)).ToList();
                if (list != null)
                {
                    foreach (Order entity in list)
                    {
                        if (entity != null)
                        {
                            entity.FK_Dispatcher_Id = dispatcherId;
                            result = repository.Update(entity);
                        }
                    }
                }
                return result;
            });
        }

        public async Task<bool> AssignOrdersToTechnican(List<int> orderIds, string technicanId)
        {
            return await Task<bool>.Run(() =>
            {
                bool result = false;
                List<Order> list = repository.GetAll().Where(ord => (ord.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled && ord.FK_OrderStatus_Id != (int)OrderStatuses.Completed) && orderIds.Contains(ord.Id)).ToList();
                if (list != null)
                {
                    foreach (Order entity in list)
                    {
                        if (entity != null)
                        {
                            entity.FK_Technican_Id = technicanId;
                            OrderProgress orderProgress = orderProgressManager.AddAssignedProgress(entity);
                            if (orderProgress != null)
                            {
                                ProgressStatus progressStatus = progressStatusManager.Get(orderProgress.FK_ProgressStatus_Id);
                                if (progressStatus != null)
                                    entity.FK_OrderStatus_Id = progressStatus.FK_OrderStatus_Id;
                            }
                            result = repository.Update(entity);
                        }
                    }
                }
                return result;
            });
        }

        public async Task<bool> AssignOrdersToDispatcher_Technican(List<int> orderIds, string dispatcherId, string technicanId)
        {
            return await Task<bool>.Run(() =>
            {
                bool result = false;
                IQueryable<Order> queryable =repository.GetAll().Where(ord => (ord.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled && ord.FK_OrderStatus_Id != (int)OrderStatuses.Completed) && orderIds.Contains(ord.Id));
                if (queryable != null)
                {
                    foreach (Order entity in queryable)
                    {
                        if (entity != null)
                        {
                            entity.FK_Dispatcher_Id = dispatcherId;
                            entity.FK_Technican_Id = technicanId;
                            result = repository.Update(entity);
                        }
                    }
                }
                return result;
            });
        }

        public async Task<List<OrderViewModel>> GetOrdersAssignedToDispatchers(List<string> dispatcherIds)
        {

            return await Task<List<OrderViewModel>>.Run(() =>
            {
                var result = repository.GetAll().Where(ord => (ord.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled && ord.FK_OrderStatus_Id != (int)OrderStatuses.Completed) && dispatcherIds.Contains(ord.FK_Dispatcher_Id)).ProjectTo<OrderViewModel>().ToList();

                return result;
            });
           
        }

        public async Task<List<OrderViewModel>> GetOrdersAssignedToDispatcher(string dispatcherId)
        {
            return await Task<List<OrderViewModel>>.Run(() =>
            {
                var result = repository.GetAll().Where(ord => (ord.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled && ord.FK_OrderStatus_Id != (int)OrderStatuses.Completed) &&  ord.FK_Dispatcher_Id == dispatcherId).ProjectTo<OrderViewModel>().ToList();

                return result;
            });    
        }

        public async  Task<List<OrderViewModel>> SearchByCustomerId(int customerId)
        {

            return await Task<List<OrderViewModel>>.Run(() =>
            {
                var result = repository.GetAll().Where(ord => ord.FK_Customer_Id == customerId).ProjectTo<OrderViewModel>().ToList();

                return result;
            });

        }

        public async Task<PagedResult<OrderViewModel>> GetOrderByCustomerIdWithPaging(PaginatedItemsViewModel pagingParameterModel)
        {
            var orderQuery = repository.GetAll().Where(ord => ord.FK_Customer_Id == pagingParameterModel.Id).AsQueryable();

            var orderResult =  await base.GetAllByPaginationAsync(orderQuery, pagingParameterModel);

            return orderResult;
        }

        public async Task<PagedResult<OrderViewModel>> SearchByCustomerIdByPaging(int customerId, PaginatedItemsViewModel pagingparametermodel)
        {
            var orderListQuery = repository.GetAll().Where(ord => (ord.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled && ord.FK_OrderStatus_Id != (int)OrderStatuses.Completed) &&  ord.FK_Customer_Id == customerId).AsQueryable();

            var orderResult = await base.GetAllByPaginationAsync(orderListQuery, pagingparametermodel);
           
            return orderResult;
        }
        public async Task<List<OrderViewModel>> SearchByCallId(int CallId)
        {
            return await Task<List<OrderViewModel>>.Run(() =>
            {
                var result = repository.GetAll().Where(ord => ord.Fk_Call_Id == CallId).ProjectTo<OrderViewModel>().ToList();

                return result;
            });
        }
        public async Task<PagedResult<OrderViewModel>> SearchByCallIdByPaging(int CallId, PaginatedItemsViewModel pagingparametermodel)
        {
            var orderListQuery = repository.GetAll().Where(ord => ord.Fk_Call_Id == CallId).AsQueryable();

            var lstQuery = await base.GetAllByPaginationAsync(orderListQuery, pagingparametermodel);

            return lstQuery;
        }
        private  async Task<int?> GetMaxOrderToday(string date)
        {
            return await Task<List<OrderViewModel>>.Run(() =>
            {

                int? nullable = new int?();
                List<string> list =repository.GetAll().Where(o => o.Code.StartsWith(ordersettings.OrderPrefix + date))
                    .Select(e => e.Code.Replace(ordersettings.OrderPrefix + date, "")).ToList();
                if (list != null && list.Count > 0)
                    nullable = new int?(list.Select(str => int.Parse(str)).Max());
                return nullable;
            });
        }

        private async Task<string> CreateRefNumberAsync()
        {
            string orderDayStartNo = ordersettings.OrderDayStartNo;
            string infixDateCode = Helper.CreateInfixDateCode();
            int? maxOrderToday = await GetMaxOrderToday(infixDateCode);
            if (maxOrderToday.HasValue)
                orderDayStartNo = (maxOrderToday.Value + 1).ToString();
            return ordersettings.OrderPrefix + infixDateCode + orderDayStartNo;
        }

        private string CreateRefNumber(Order order)
        {
            string randomnum;
            string ordercode;
            bool isexist = false;
            do
            {
                randomnum = GenerateRandom();
                order.OrderType = orderTypeMnger.Get(order.FK_OrderType_Id);
                ordercode = order.OrderType.Code + randomnum;
                isexist = (context as OrderDbContext).Order.Count(o => o.Code == ordercode) > 0;
            } while (isexist);
            return ordercode;
        }

        private string GenerateRandom()
        {
            Random random = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 5; i++)
            {
                sb.Append(random.Next(0, 9).ToString());
            }
            return sb.ToString();
        }

        private Order fillOrderNavProps(Order order)
        {
            order.OrderType = orderTypeMnger.Get(new object[1]
            {
         order.FK_OrderType_Id
            });
            order.OrderStatus = orderStatusManager.Get(new object[1]
            {
         order.FK_OrderStatus_Id
            });
            order.OrderPriority = orderPriorityManager.Get(new object[1]
            {
         order.FK_OrderPriority_Id
            });
            order.OrderProblem = orderProblemManager.Get(new object[1]
            {
         order.FK_OrderProblem_Id
            });
            order.OrderFiles = orderFilesManager.GetByOrderId(order.Id);
            return order;

        }

        private List<Order> FillOrdersNavProps(List<Order> orders)
        {
            foreach (Order order in orders)
                fillOrderNavProps(order);
            return orders;
        }

        private List<Order> FillOrderProgress(List<Order> orders)
        {
            foreach (Order order in orders)
                order.LstOrderProgress = orderProgressManager.GetOrderProgressByOrderId(order.Id);
            return orders;
        }

        private Order FillOrderProgress(Order order)
        {
            order.LstOrderProgress = orderProgressManager.GetOrderProgressByOrderId(order.Id);
            return order;
        }

        public async Task<List<OrderViewModel>> GetGrouptOfOrders(List<int> orderIds)
        {
            return await Task<List<OrderViewModel>>.Run(() =>
            {
                var result = repository.GetAll().Where(ord => (ord.FK_OrderStatus_Id != (int)OrderStatuses.Cancelled && ord.FK_OrderStatus_Id != (int)OrderStatuses.Completed) && orderIds.Contains(ord.Id)).ProjectTo<OrderViewModel>().ToList();

                return result;
            });
        }
    }
}
