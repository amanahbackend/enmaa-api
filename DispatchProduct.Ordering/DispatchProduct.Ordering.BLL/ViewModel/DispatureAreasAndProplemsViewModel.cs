﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.BLL.ViewModel.AssignedMultiTechnicansViewModel
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using DispatchProduct.Ordering.BLL.ServicesViewModels;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.BLL.ViewModel
{
  public class DispatureAreasAndProplemsViewModel : BaseEntityViewModel
  {

        public int Id { get; set; }

        public string Proplems { get; set; }

        public string Areas { get; set; }
        public string Governments { get; set; }


        public string FK_Dispatcher_Id { get; set; }

        public ApplicationUserViewModel Dispatcher { get; set; }
    }
}
