﻿using System.Collections.Generic;
using Utilites.UploadFile;

namespace DispatchProduct.Ordering.BLL.ViewModel
{
  public class CustomerSignatureViewModel : BaseEntityViewModel
  {
    public UploadFile File { get; set; }

    public UploadFile ContractSignatureFile { get; set; }

    public List<int> UsedItems { get; set; }

    public OrderProgressViewModel OrderProgress { get; set; }
  }
}
