﻿using System.Collections.Generic;
using Utilites.UploadFile;

namespace DispatchProduct.Ordering.BLL.ViewModel
{
  public class CustomerSignaturesViewModel : BaseEntityViewModel
  {
    public UploadFile ContractSignatureFile { get; set; }

    public List<UploadFile> OrderFiles { get; set; }

    public List<int> UsedItems { get; set; }

    public OrderProgressViewModel OrderProgress { get; set; }
  }
}
