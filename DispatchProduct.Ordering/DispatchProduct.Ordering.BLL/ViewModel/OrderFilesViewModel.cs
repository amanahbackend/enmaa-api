﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Ordering.BLL.ViewModel;
using DispatchProduct.Ordering.IEntities;

namespace DispatchProduct.Ordering.Entities
{
    public class OrderFilesViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FileRelativePath { get; set; }
        public string FileURL { get; set; }
        public int OrderId { get; set; }
    }
}
