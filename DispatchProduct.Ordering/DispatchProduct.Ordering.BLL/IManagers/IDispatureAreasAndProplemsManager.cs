﻿using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.Models.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IDispatureAreasAndProplemsManager : IRepositry<DispatureAreasAndProplems>
    {
        string GetDispatcherIdForOrder(int proplemid, string area);

        //List<DispatureAreasAndProplems> AssignCriteriaToDispatcher(List<DispatureAreasAndProplems> list);

        //List<DispatureAreasAndProplems> GetAllDistribution();

        //bool DeleteByDispatcherId(string dispatcherId);

        //List<DispatureAreasAndProplems> GetByDispatcherId(string dispatcherId);
    }
}
