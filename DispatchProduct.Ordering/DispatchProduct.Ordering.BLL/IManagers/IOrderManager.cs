﻿

using DispatchProduct.Ordering.BLL.Filters;
using DispatchProduct.Ordering.BLL.ViewModel;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IOrderManager : IBaseManager<OrderViewModel, Order>
    {



        //List<Order> Search(string orderCode);
        Task<PagedResult<OrderViewModel>> GetOrderByCustomerIdWithPaging(PaginatedItemsViewModel pagingParameterModel);
        Task<List<OrderViewModel>> SearchByCustomerId(int customerId);
        Task<PagedResult<OrderViewModel>> SearchByCustomerIdByPaging(int customerId, PaginatedItemsViewModel pagingparametermodel);


        Task<List<OrderViewModel>> SearchByCallId(int CallId);
        Task<PagedResult<OrderViewModel>> SearchByCallIdByPaging(int CallId, PaginatedItemsViewModel pagingparametermodel);


        //List<Order> GetAllOrderFilledProps();
        Task<PagedResult<OrderViewModel>> GetAllOrderFilledPropsByPaging(PaginatedItemsViewModel pagingparametermodel);
        Task<PagedResult<OrderViewModel>> GetOrdersBySearchAsync(PaginatedItemsViewModel pagingparametermodel);

        Task<OrderProgress> AddOrderProgressAsync(OrderProgress orderProgress);

        Task<List<OrderViewModel>> GetOrdersAssignedToTechnicans(List<string> technicanIds);

        Task<List<OrderViewModel>> GetOrdersAssignedToTechnican(string technicanId);

        Task<List<OrderViewModel>> GetDispatcherOrders(string dispatcherId);

        Task<List<OrderViewModel>> GetOrdersAssignedToDispatchers(List<string> dispatcherIds);

        Task<List<OrderViewModel>> GetFilteredOrderByDispatcherId(FilterOrderByDispatcher filter);


        Task<List<OrderViewModel>> GetOrdersAssignedToDispatcher(string dispatcherId);

        Task<OrderViewModel> GetAsync(int id);

        Task<List<OrderViewModel>> GetAllOrdersNavigatedAsync();

        Task<bool> AssignOrderToTechnican(int orderId, string technicanId);

        Task<bool> AssignOrderToDispatcher(int orderId, string dispatcherId);

        Task<bool> TransferOrderToDispatcher(int orderId, string dispatcherId);

        Task<bool> SetPreferedVisitTime(int orderId, DateTime PreferedVisitTime);

        Task<string> UnAssignOrderToTechnican(int orderId);

        Task<List<OrderViewModel>> GetAllDispatcherOrders(string dispatcherId);

        Task<bool> AssignOrderToDispatcher_Technican(int orderId, string dispatcherId, string technicanId);

        Task<bool> AssignOrdersToDispatcher(List<int> orderIds, string dispatcherId);

        Task<bool> AssignOrdersToTechnican(List<int> orderIds, string technicanId);

        Task<bool> AssignOrdersToDispatcher_Technican(List<int> orderIds, string dispatcherId, string technicanId);


        Task<List<OrderViewModel>> GetGrouptOfOrders(List<int> orderIds);

        Task<PagedResult<OrderViewModel>> GetFilteredOrderByDispatcherIdPagginated(PaginatedItemsViewModel pagingparametermodel, FilterOrderByDispatcher filter);

    }
}
