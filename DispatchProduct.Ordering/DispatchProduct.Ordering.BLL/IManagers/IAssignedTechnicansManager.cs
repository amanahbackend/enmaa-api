﻿using DispatchProduct.Ordering.BLL.ViewModel;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IAssignedTechnicansManager : IBaseManager<AssignedTechnicansViewModel, AssignedTechnicans>
    {
        Task<List<string>> GetByDispatcherIdAsync(string fk_Dispatcher_Id);
        Task<bool> UpdateAssignedTechnicanAsync(AssignedTechnicansViewModel assignedTechnicansViewModel);
        Task<bool> DeleteAssignedTechnicanAsync(string technicanId);
        Task<AssignedTechnicansViewModel> GetByTechnicanIdAsync(string technicanId);
        Task<AssignedTechnicansViewModel> GetByIdAsync(int id);

        Task<List<string>> GetAllTecIdsAsync();

    }
}
