﻿using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesSettings
{
    public class CallingServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri { get; set; }

        public string SearchByKey { get; set; }
    }
}
