﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.Settings;
using DispatchProduct.Ordering.BLL.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication.PreventiveMaintainence
{
    public interface IPreventiveMaintainenceService : IDefaultHttpClientCrud<PreventiveMaintainenceServiceSetting, PreventiveMaintainenceScheduleViewModel, PreventiveMaintainenceScheduleViewModel>
    {
        Task<List<PreventiveMaintainenceScheduleViewModel>> GetDailyPreventiveOrders(string authHeader = "");

        Task<bool> UpdatePreventiveMaintainenceByOrder(List<PreventiveOrderViewModel> model, string authHeader = "");
    }
}
