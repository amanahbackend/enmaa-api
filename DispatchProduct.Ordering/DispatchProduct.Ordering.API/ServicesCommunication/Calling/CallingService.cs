﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesSettings;
using DispatchProduct.Ordering.BLL.ServicesViewModels.Calling;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication.Calling
{
    public class CallingService : DefaultHttpClientCrud<CallingServiceSetting, CallingViewModel, CallingViewModel>, ICallingService
    {
        CallingServiceSetting settings;
        public CallingService(IOptions<CallingServiceSetting> _settings) : base(_settings.Value)
        {
            settings = _settings.Value;
        }

        public async Task<List<CallingViewModel>> SearchCallByNameOrNumber(string key, string authHeader = "")
        {
            var requesturi = $"{settings.Uri}/{settings.SearchByKey}?key={key}";
            return await GetListByUri(requesturi, authHeader);
        }

    }
}
