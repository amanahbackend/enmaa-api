﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesSettings;
using DispatchProduct.Ordering.BLL.ServicesViewModels.Calling;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication.Calling
{
    public interface ICallingService : IDefaultHttpClientCrud<CallingServiceSetting, CallingViewModel, CallingViewModel>
    {
       Task<List<CallingViewModel>> SearchCallByNameOrNumber(string key, string authHeader = "");
    }
}
