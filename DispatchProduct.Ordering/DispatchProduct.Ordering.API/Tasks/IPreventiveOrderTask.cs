﻿using AutoMapper;
using DispatchProduct.Ordering.API.ServicesCommunication.PreventiveMaintainence;
using DispatchProduct.Ordering.BLL.ViewModel;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.Tasks
{
    public interface IPreventiveOrderTask
    {
        Task AddPreventiveOrder();
    }
}
