﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.BLL.ViewModel;
using DispatchProduct.Ordering.Entities;
using Utilites;
using DispatchProduct.Ordering.API.ServicesCommunication;
using DispatchProduct.Ordering.API;
using DispatchProduct.Ordering.Models.Entities;
using DispatchProduct.Ordering.BLL.ServicesViewModels;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    //[Authorize]
    [Route("api/DispatureAreasAndProplems")]
    public class DispatureAreasAndProplemsController : Controller
    {
        public IDispatureAreasAndProplemsManager manger;
        public readonly IMapper mapper;
        IUserService userService;
        public DispatureAreasAndProplemsController(IDispatureAreasAndProplemsManager _manger, IMapper _mapper, IUserService _userService)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            userService = _userService;


        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> GetAsync(int id)
        {
            DispatureAreasAndProplems entityResult = manger.Get(id);
            var result = mapper.Map<DispatureAreasAndProplems, DispatureAreasAndProplemsViewModel>(entityResult);

            if (result.FK_Dispatcher_Id != null)
            {
                var dispatcher = await GetUser(result.FK_Dispatcher_Id);
                result.Dispatcher = dispatcher;

            }

            return Ok(result);
        }
        //[Route("GetByDispatcherId/{dispatcherId}")]
        //[HttpGet]
        //public async Task<IActionResult> GetByDispatcherId(string dispatcherId)
        //{
        //    List<DispatureAreasAndProplems> entityResult = manger.GetByDispatcherId(dispatcherId);
        //    List<DispatureAreasAndProplemsViewModel> result = mapper.Map<List<DispatureAreasAndProplems>, List<DispatureAreasAndProplemsViewModel>>(entityResult);
        //    var itm=result.FirstOrDefault();
        //    if (itm.FK_Dispatcher_Id != null)
        //    {
        //        var dispatcher = await GetUser(itm.FK_Dispatcher_Id);
        //        foreach (var item in result)
        //        {
        //            item.Dispatcher = dispatcher;
        //        }
        //    }
        //    return Ok(result);
        //}
        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            List<DispatureAreasAndProplems> entityResult = manger.GetAll().ToList();
            List<DispatureAreasAndProplemsViewModel> results = mapper.Map<List<DispatureAreasAndProplems>, List<DispatureAreasAndProplemsViewModel>>(entityResult);

            foreach (var result in results)
            {
                if (result.FK_Dispatcher_Id != null)
                {
                    var dispatcher = await GetUser(result.FK_Dispatcher_Id);
                    result.Dispatcher = dispatcher;

                }
            }
            return Ok(results);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]DispatureAreasAndProplemsViewModel model)
        {
            DispatureAreasAndProplems entityResult = mapper.Map<DispatureAreasAndProplemsViewModel, DispatureAreasAndProplems>(model);
            entityResult = manger.AddAsync(entityResult);
            DispatureAreasAndProplemsViewModel result = mapper.Map<DispatureAreasAndProplems, DispatureAreasAndProplemsViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]DispatureAreasAndProplemsViewModel model)
        {
            DispatureAreasAndProplems entityResult = mapper.Map<DispatureAreasAndProplemsViewModel, DispatureAreasAndProplems>(model);
            bool result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            DispatureAreasAndProplems entity = manger.Get(id);
            result = manger.SoftDelete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
        //[Route("AssignCriteriaToDispatchers")]
        //[HttpPost]
        //public async Task<IActionResult> AssignCriteriaToDispatchers([FromBody]List<OrderMultiDistributionCriteriaViewModel> model)
        //{
        //    List<DispatureAreasAndProplemsViewModel> result = new List<DispatureAreasAndProplemsViewModel>();

        //    foreach (var item in model)
        //    {
        //        manger.DeleteByDispatcherId(item.FK_Dispatcher_Id);
        //        item.Dispatcher = (await GetUser(item.FK_Dispatcher_Id));
        //        var dispatcherCriterias = mapper.Map<OrderMultiDistributionCriteriaViewModel, List<DispatureAreasAndProplems>>(item);
        //        dispatcherCriterias = manger.AssignCriteriaToDispatcher(dispatcherCriterias);
        //        var dispatcherCriteriasVM = mapper.Map<List<DispatureAreasAndProplems>, List< DispatureAreasAndProplemsViewModel>>(dispatcherCriterias);
        //        result.AddRange(dispatcherCriteriasVM);
        //    }

        //    return Ok(result);
        //}

        //[Route("GetAllDistribution")]
        //[HttpGet]
        //public async Task<IActionResult> GetAllDistribution()
        //{

        //    var entityResult = manger.GetAllDistribution();
        //    var result = mapper.Map<List<DispatureAreasAndProplems>, List<DispatureAreasAndProplemsViewModel> >(entityResult);

        //    foreach (var item in result)
        //    {
        //        item.Dispatcher = await GetUser(item.FK_Dispatcher_Id);
        //    }
        //    return Ok(result);

        //}


        public async Task<ApplicationUserViewModel> GetUser([FromRoute]string userId, string authHeader = null)
        {
            ApplicationUserViewModel result = null;
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            result = (await userService.GetByUserIds(new List<string> { userId }, authHeader)).FirstOrDefault();
            return result;
        }

    }
}