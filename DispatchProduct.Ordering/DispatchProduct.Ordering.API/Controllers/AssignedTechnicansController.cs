﻿using AutoMapper;
using DispatchProduct.Ordering.API;
using DispatchProduct.Ordering.API.Controllers;
using DispatchProduct.Ordering.API.ServicesCommunication;
using DispatchProduct.Ordering.BLL.ViewModel;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Utilites;
using DispatchProduct.Ordering.BLL.ServicesViewModels;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Authorize]
    [Route("api/AssignedTechnicans")]
    public class AssignedTechnicansController : Controller
    {
        public IAssignedTechnicansManager manger;
        public readonly IMapper mapper;
        IUserService userService;
        IOrderManager ordermanager;
        OrderController ordController;
        public AssignedTechnicansController(IAssignedTechnicansManager _manger, IMapper _mapper, IUserService _userService, IOrderManager _ordermanager, OrderController _ordController)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            userService = _userService;
            ordermanager = _ordermanager;
            ordController = _ordController;
        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetAsync(int id)
        {
            var entityResult = await manger.GetByIdAsync(id);

            return Ok(entityResult);
        }



        /// <summary>
        /// Get Dispature for technican id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("GetAssignedDispatureForTechnican/{technicanId}")]
        [HttpGet]
        public async Task<IActionResult> GetAssignedDispatureForTechnicanAsync([FromRoute]string technicanId)
        {
            var entityResult = await manger.GetByTechnicanIdAsync(technicanId);

            return Ok(entityResult);
        }





        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var entityResult = await manger.GetAllAsync();

            return Ok(entityResult);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]AssignedTechnicansViewModel model)
        {
            var entityResult = await manger.AddAsync(model);

            return Ok(entityResult);
        }

        [Route("Assign")]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]AssignedMultiTechnicansViewModel model)
        {
            var result = new List<AssignedTechnicansViewModel>();

            foreach (var technicanId in model.FK_Technicans_Id)
            {
                var assignedTechnicicnVM = new AssignedTechnicansViewModel
                {
                    FK_Technican_Id = technicanId,
                    FK_Dispatcher_Id = model.FK_Dispatcher_Id
                };

                var res = await manger.AddAsync(assignedTechnicicnVM);
                result.Add(res);
            }
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]AssignedTechnicansViewModel model)
        {
            bool result = await manger.UpdateAsync(model);
            return Ok(result);
        }

        [Route("UpdateAssignedTechnican")]
        [HttpPut]
        public async Task<IActionResult> UpdateAssignedTechnican([FromBody]AssignedTechnicansViewModel model)
        {
            bool result = await manger.UpdateAssignedTechnicanAsync(model);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            var entity = await manger.GetByIdAsync(id);
            result = await manger.DeleteAsync(entity);
            return Ok(result);
        }

        [Route("DeleteAssignedTechnican/{technicanId}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteAssignedTechnican([FromRoute]string technicanId)
        {
            bool result = false;
            result = await manger.DeleteAssignedTechnicanAsync(technicanId);
            return Ok(result);
        }
        #endregion
        #endregion
        [Route("GetAssignedTechnicans")]
        [HttpGet]
        public async Task<List<ApplicationUserViewModel>> GetAssignedTechnicans(string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var fK_Technican_Ids =await  manger.GetAllTecIdsAsync();
            return await userService.GetByUserIds(fK_Technican_Ids, authHeader);
        }





        [Route("GetUnAssignedTechnicans")]
        [HttpGet]
        public async Task<List<ApplicationUserViewModel>> GetUnAssignedTechnicans(string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var fK_Technican_Ids =await  manger.GetAllTecIdsAsync();
            return await userService.GetUnAssignedTechnicans(fK_Technican_Ids, authHeader);
        }


        [Route("GetTechnicansByDispatcherId/{dispatcherId}")]
        [HttpGet]
        public async Task<List<TechnicanViewModel>> GetTechnicansByDispatcherId([FromRoute]string dispatcherId, string authHeader = null)

        {

            List<TechnicanViewModel> Result = new List<TechnicanViewModel>();

            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var technicansIds = await manger.GetByDispatcherIdAsync(dispatcherId);

           

            var tecnicansInfo = await userService.GetByUserIds(technicansIds, authHeader);
            foreach (var techn in tecnicansInfo)
            {
                Technican tech = new Technican();


                var techInfo = tecnicansInfo.Find(t => t.Id == techn.Id);
                mapper.Map(techInfo, tech);
                var techViewMod = mapper.Map<Technican, TechnicanViewModel>(tech);

                techViewMod.Orders = await ordermanager.GetOrdersAssignedToTechnican(techn.Id);
                var orders = techViewMod.Orders;
                orders = await ordController.FillCustomerOrders(orders, authHeader);
                orders = await ordController.FillLocationOrders(orders, authHeader);
                orders = await ordController.FillCallOrders(orders, authHeader);
                techViewMod.Orders = orders;



                Result.Add(techViewMod);


            }
           
            return Result;
        }



        [Route("GetDispatcher/{dispatcherId}")]
        [HttpGet]
        public async Task<DispatcherViewModel> GetDispatcher([FromRoute]string dispatcherId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            Dispatcher entityResult = new Dispatcher();

            var orderVM = await ordermanager.GetDispatcherOrders(dispatcherId);
            entityResult.Orders = mapper.Map<List< OrderViewModel>,List<Order>>(orderVM);

            var dispatcherInfo = (await userService.GetByUserIds(new List<string> { dispatcherId }, authHeader)).FirstOrDefault();
            mapper.Map(dispatcherInfo, entityResult);
            var result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
            result.Orders = await ordController.FillCustomerOrders(result.Orders, authHeader);
            result.Orders = await ordController.FillLocationOrders(result.Orders, authHeader);
            result.Orders = await ordController.FillCallOrders(result.Orders, authHeader);
            return result;
        }
        [Route("GetOrderByDispatcher/{dispatcherId}")]
        [HttpGet]
        public async Task<DispatcherViewModel> GetOrderByDispatcher([FromRoute]string dispatcherId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            Dispatcher entityResult = new Dispatcher();
            var orderVM =  await ordermanager.GetAllDispatcherOrders(dispatcherId);
             entityResult.Orders = mapper.Map<List<OrderViewModel>, List<Order>>(orderVM);

            var dispatcherInfo = (await userService.GetByUserIds(new List<string> { dispatcherId }, authHeader)).FirstOrDefault();
            mapper.Map(dispatcherInfo, entityResult);
            var result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
            result.Orders = await ordController.FillCustomerOrders(result.Orders, authHeader);
            result.Orders = await ordController.FillLocationOrders(result.Orders, authHeader);
            result.Orders = await ordController.FillCallOrders(result.Orders, authHeader);
            return result;
        }
        [Route("GetDispatcherSupervisor/{dispatcherId}")]
        [HttpGet]
        public async Task<DispatcherViewModel> GetDispatcherSupervisor([FromRoute]string dispatcherId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var dispatcherInfo = (await userService.GetByUserIds(new List<string> { dispatcherId }, authHeader)).FirstOrDefault();
            Dispatcher entityResult = new Dispatcher();
            var orderVM = await ordermanager.GetOrdersAssignedToDispatcher(dispatcherId);
            entityResult.Orders = mapper.Map<List<OrderViewModel>, List<Order>>(orderVM);


            mapper.Map(dispatcherInfo, entityResult);
            var result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
            return result;
        }
        [Route("GetTechnican/{technicanId}")]
        [HttpGet]
        public async Task<DispatcherViewModel> GetTechnican([FromRoute]string technicanId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var dispatcherInfo = (await userService.GetByUserIds(new List<string> { technicanId }, authHeader)).FirstOrDefault();
            Dispatcher entityResult = new Dispatcher();
            var orderVM = await ordermanager.GetOrdersAssignedToTechnican(technicanId);
            entityResult.Orders = mapper.Map<List<OrderViewModel>, List<Order>>(orderVM);


            mapper.Map(dispatcherInfo, entityResult);
            var result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);

               result.Orders = orderVM;
              await FillOrderInfo(result);
            return result;
        }
        public async Task FillOrderInfo(DispatcherViewModel result, string authHeader = null)
        {
            if (result != null && result.Orders != null)
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                foreach (var item in result.Orders)
                {
                    if (item.FK_Customer_Id != null)
                    {
                        item.Customer = await ordController.GetCustomerById(item.FK_Customer_Id.Value, authHeader);
                    }
                    if (item.FK_Location_Id != null)
                    {
                        item.Location = await ordController.GetLocationById(item.FK_Location_Id.Value, authHeader);
                    }
                    if (item.FK_Contract_Id != null)
                    {
                        item.Contract = await ordController.GetContractById(item.FK_Contract_Id.Value, authHeader);
                    }
                    if (item.Fk_Call_Id != null)
                    {
                        item.Call = await ordController.GetCallById(item.Fk_Call_Id.Value, authHeader);
                    }
                }
            }
        }
    }
}