﻿using AutoMapper;
using DispatchProduct.Api.HttpClient;
using DispatchProduct.Ordering.API.ServicesCommunication;
using DispatchProduct.Ordering.API.ServicesCommunication.Calling;
using DispatchProduct.Ordering.API.ServicesCommunication.PreventiveMaintainence;
using DispatchProduct.Ordering.BLL.ServicesViewModels;
using DispatchProduct.Ordering.BLL.ServicesViewModels.Calling;
using DispatchProduct.Ordering.BLL.ServicesViewModels.Dropout;
using DispatchProduct.Ordering.BLL.ViewModel;
using DispatchProduct.Ordering.BLL.Filters;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.Hubs;
using DispatchProduct.Ordering.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using Utilities.Utilites.Paging;

namespace DispatchProduct.Ordering.API.Controllers
{
    [Authorize]
    [Route("api/Order")]
    public class OrderController : Controller
    {
        public IOrderManager manger;
        public IOrderFilesManager filesManger;
        public readonly IMapper mapper;
        private ICustomerService customerService;
        private ILocationService locService;
        private IOrderDistributionCriteriaManager ordDistribtion;
        private IUserService userService;
        private OrderAppSettings appSettings;
        private IPreventiveMaintainenceService preventiveService;
        private IContractService contractService;
        private IHubContext<OrderingHub> orderingHub;
        private IOrderingHub orderHub;
        private ITechnicanUsedItemService usedItmService;
        private IOrderPriorityManager orderPriorityManager;
        private ICallingService callingService;
        private IOrderTypeManager orderTypeManager;
        private IOrderProblemManager orderProblemManage;

        private IConfiguration configuration;
        private IDispatureAreasAndProplemsManager dispatureAreaAndProplem;
     

        //public OrderController(IContractService _contractService, IConfigurationRoot _configuration, IOrderManager _manger, IOrderFilesManager _filesManger, IMapper _mapper, ICustomerService _customerService, ILocationService _locService, IOrderDistributionCriteriaManager _ordDistribtion, IUserService _userService, IOptions<OrderAppSettings> _appSettings, IPreventiveMaintainenceService _preventiveService, IHubContext<OrderingHub> _orderingHub, IOrderingHub _orderHub, ITechnicanUsedItemService _usedItmServ)
        public OrderController(IContractService _contractService, IConfiguration _configuration, IOrderManager _manger,
    IOrderFilesManager _filesManger, IMapper _mapper, ICustomerService _customerService,
    ILocationService _locService, IOrderDistributionCriteriaManager _ordDistribtion,
    IUserService _userService, IOptions<OrderAppSettings> _appSettings,
    IPreventiveMaintainenceService _preventiveService, IHubContext<OrderingHub> _orderingHub,
    IOrderingHub _orderHub, ITechnicanUsedItemService _usedItmServ,
    IOrderPriorityManager _orderPriorityManager, ICallingService _callingService,
    IOrderTypeManager _orderTypeManager, IDispatureAreasAndProplemsManager _dispatureAreaAndProplem, IOrderProblemManager _orderProblemManage )

        {
            orderTypeManager = _orderTypeManager;
            callingService = _callingService;
            orderPriorityManager = _orderPriorityManager;
            configuration = _configuration;
            manger = _manger;
            filesManger = _filesManger;
            mapper = _mapper;
            customerService = _customerService;
            locService = _locService;
            ordDistribtion = _ordDistribtion;
            userService = _userService;
            appSettings = _appSettings.Value;
            preventiveService = _preventiveService;
            contractService = _contractService;
            orderingHub = _orderingHub;
            orderHub = _orderHub;
            usedItmService = _usedItmServ;
            dispatureAreaAndProplem = _dispatureAreaAndProplem;
            orderProblemManage = _orderProblemManage;
         
        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public async Task<IActionResult> Get([FromRoute]int id)
        {
            var result = await manger.GetAsync(id);
            //var result = mapper.Map<Order, OrderViewModel>(entityResult);
            if (result.FK_Customer_Id != null)
            {
                result.Customer = await GetCustomerById(result.FK_Customer_Id.Value);
            }
            if (result.FK_Location_Id != null)
            {
                result.Location = await GetLocationById(result.FK_Location_Id.Value);
            }
            if (result.FK_Contract_Id != null)
            {
                result.Contract = await GetContractById(result.FK_Contract_Id.Value);
            }
            if (result.Fk_Call_Id != null)
            {
                result.Call = await GetCallById(result.Fk_Call_Id.Value);
            }
            BindFilesURL(result);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpPost]
        public async Task<IActionResult> Get([FromBody]PaginatedItemsViewModel pagingparametermodel)
        {
            if (pagingparametermodel.PageNumber < 1 && pagingparametermodel.PageSize < 1)
                return BadRequest();

            var  PagedResult =await manger.GetAllOrderFilledPropsByPaging(pagingparametermodel);
            List<OrderViewModel> result = PagedResult.Result;
            result = await FillCustomerOrders(result);
            result = await FillLocationOrders(result);
            result = await FillContractOrders(result);
            result = await Fill_Tech_Dispatcher_Orders(result);
            result = await FillCallOrders(result);
            BindFilesURL(result);
            //BindFilesURL(result);
            return Ok(PagedResult);
        }
        [Route("GetFilteredOrderByDispatcherId")]
        [HttpPost]
        public async Task<IActionResult> GetFilteredOrderByDispatcherId([FromBody] FilterOrderViewModelByDispatcher filter)
        {
            FilterOrderByDispatcher filter1 = mapper.Map<FilterOrderViewModelByDispatcher, FilterOrderByDispatcher>(filter);
            List<OrderViewModel> orders =  await  manger.GetFilteredOrderByDispatcherId(filter1);
            List<FilteredOrderViewModel> result = new List<FilteredOrderViewModel>();
            orders = await FillCustomerOrders(orders);
            orders = await FillLocationOrders(orders);
            var model = await Fill_Tech_Dispatcher_Orders(orders);
            BindFilesURL(model);
            foreach (OrderViewModel orderViewModel in model)
            {
                OrderViewModel item = orderViewModel;
                if (item.LstOrderProgress != null && item.LstOrderProgress.Count > 0)
                {
                    List<OrderProgressViewModel> progressViewModelList = await Fill_Tech_Dispatcher_OrderProgress(item.LstOrderProgress);
                    foreach (OrderProgressViewModel source in item.LstOrderProgress)
                    {
                        FilteredOrderViewModel destination = mapper.Map<OrderViewModel, FilteredOrderViewModel>(item);
                        result.Add(mapper.Map(source, destination));
                    }
                }
                else
                    result.Add(mapper.Map<OrderViewModel, FilteredOrderViewModel>(item));
            }
            return Ok(result);
        }


        [Route("GetFilteredOrderByDispatcherIdPaged")]
        [HttpPost]
        public async Task<IActionResult> GetFilteredOrderByDispatcherIdPaged([FromBody]PaginatedItemsViewModel pagingparametermodel, [FromBody] FilterOrderViewModelByDispatcher filter)
        {
            FilterOrderByDispatcher filter1 = mapper.Map<FilterOrderViewModelByDispatcher, FilterOrderByDispatcher>(filter);
            var ordersPaged = await manger.GetFilteredOrderByDispatcherIdPagginated(pagingparametermodel, filter1);
            List<FilteredOrderViewModel> result = new List<FilteredOrderViewModel>();
            ordersPaged.Result = await FillCustomerOrders(ordersPaged.Result);
            ordersPaged.Result = await FillLocationOrders(ordersPaged.Result);
            var model = await Fill_Tech_Dispatcher_Orders(ordersPaged.Result);
            BindFilesURL(model);
            foreach (OrderViewModel orderViewModel in model)
            {
                OrderViewModel item = orderViewModel;
                if (item.LstOrderProgress != null && item.LstOrderProgress.Count > 0)
                {
                    List<OrderProgressViewModel> progressViewModelList = await Fill_Tech_Dispatcher_OrderProgress(item.LstOrderProgress);
                    foreach (OrderProgressViewModel source in item.LstOrderProgress)
                    {
                        FilteredOrderViewModel destination = mapper.Map<OrderViewModel, FilteredOrderViewModel>(item);
                        result.Add(mapper.Map(source, destination));
                    }
                }
                else
                    result.Add(mapper.Map<OrderViewModel, FilteredOrderViewModel>(item));
            }




            return Ok(new { Result = result, TotalCount= ordersPaged.TotalCount });
        }

        [Route("GetMapFilteredOrderByDispatcherId")]
        [HttpPost]
        public async Task<IActionResult> GetMapFilteredOrderByDispatcherId([FromBody] FilterOrderViewModelByDispatcher model)
        {
            FilterOrderByDispatcher filter = mapper.Map<FilterOrderViewModelByDispatcher, FilterOrderByDispatcher>(model);
            List<OrderViewModel> ordersViewModel = await manger.GetFilteredOrderByDispatcherId(filter);
            //List<OrderViewModel> ordersViewModel = mapper.Map<List<Order>, List<OrderViewModel>>(orders);
            ordersViewModel = await FillCustomerOrders(ordersViewModel);
            ordersViewModel = await FillLocationOrders(ordersViewModel);
            ordersViewModel = await Fill_Tech_Dispatcher_Orders(ordersViewModel);
            BindFilesURL(ordersViewModel);
            return Ok(ordersViewModel);
        }

        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] OrderViewModel model)
        {
            ProcessResult<OrderViewModel> processResult = await AddOrder(model);
            return !processResult.IsSucceeded ? BadRequest(processResult.Message) : (IActionResult)Ok(processResult.returnData);
        }

        public async Task<ProcessResult<OrderViewModel>> AddOrder([FromBody] OrderViewModel model, string auth = null)
        {
            ProcessResult<OrderViewModel> result = new ProcessResult<OrderViewModel>();
            result.IsSucceeded = true;
            Order entityResult = mapper.Map<OrderViewModel, Order>(model);
            if (entityResult.FK_Location_Id != null)
            {
                LocationViewModel locationById = await GetLocationById(entityResult.FK_Location_Id.Value, auth);
                if (locationById != null)
                {
                    entityResult.Area = locationById.Area;
                }
                else
                {
                    result.IsSucceeded = false;
                    result.Message = "Can't create order by this location";
                    return result;
                }
            }
            if (entityResult.Fk_Call_Id != null)
            {
                CallingViewModel calling = await GetCallById(entityResult.Fk_Call_Id.Value, auth);
                if (calling != null)
                {
                    entityResult.Area = calling.Area;
                }
                else
                {
                    result.IsSucceeded = false;
                    result.Message = "Can't create order by this Call";
                    return result;
                }
            }

            string str = dispatureAreaAndProplem.GetDispatcherIdForOrder(entityResult.FK_OrderProblem_Id, entityResult.Area);
            if (str == null)
                str = (await GetDispatcherSupervisor(auth)).Id;
            if (str != null)
            {
                entityResult.FK_Dispatcher_Id = str;
                if (entityResult.FK_Customer_Id != null)
                {
                    var customer = await GetCustomerById(entityResult.FK_Customer_Id.Value);
                    var customerType = customer.CustomerType.Name;
                    switch (customerType)
                    {
                        case "VIP":
                            entityResult.FK_OrderPriority_Id = orderPriorityManager.Get(x => x.Name.Equals("High")).Id;
                            break;
                        case "Regular":
                            entityResult.FK_OrderPriority_Id = orderPriorityManager.Get(x => x.Name.Equals("Medium")).Id;
                            break;
                        case "Basic":
                            entityResult.FK_OrderPriority_Id = orderPriorityManager.Get(x => x.Name.Equals("Low")).Id;
                            break;
                        default:
                            break;
                    }
                }
                if (entityResult.Fk_Call_Id != null)
                {
                    entityResult.FK_OrderType_Id = orderTypeManager.Get(x => x.Name.Equals("Cash Call")).Id;
                }
                else if (entityResult.FK_Contract_Id != null)
                {
                    entityResult.FK_OrderType_Id = orderTypeManager.Get(x => x.Name.Equals("Contract")).Id;
                }

                var   orderViewModel = mapper.Map<Order, OrderViewModel>(entityResult);
                result.returnData = await manger.AddAsync(orderViewModel);
            
                orderHub.AssignOrderToDispatcher(result.returnData, orderingHub, entityResult.FK_Dispatcher_Id);
            }
            else
            {
                result.IsSucceeded = false;
                result.Message = "Can't create order without assigning to dispatcher. set supervisor dispatcher or assign convenient criteria to dispatcher";
                return result;
            }
            return result;
        }
        [Route("AddOrderProgress")]
        [HttpPost]
        public async Task<IActionResult> AddOrderProgressAsync([FromBody]OrderProgressViewModel model)
        {
            OrderProgress entityResult = mapper.Map<OrderProgressViewModel, OrderProgress>(model);
            entityResult = await manger.AddOrderProgressAsync(entityResult);
            OrderProgressViewModel result = mapper.Map<OrderProgress, OrderProgressViewModel>(entityResult);
            OrderViewModel orderViewModel = await manger.GetAsync(model.FK_Order_Id);
            result.Order = orderViewModel;
            //if (result.FK_CreatedBy_Id == result.FK_Technican_Id)
            //    orderHub.ChangeOrderProgress(result, orderingHub, orderViewModel.FK_Dispatcher_Id);
            //else
            //    orderHub.ChangeOrderProgress(result, orderingHub, orderViewModel.FK_Technican_Id);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody]OrderViewModel model, string authHeader = null)
        {
            //Order entityResult = mapper.Map<OrderViewModel, Order>(model);
            bool result =await manger.UpdateAsync(model);
            //if (Request != null && authHeader == null)
            //    authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
            //orderHub.UpdateOrder(model, orderingHub, authHeader);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteAsync([FromRoute]int id)
        {
            bool result = false;
            var entityModel =await manger.GetAsync(id);
            result =await manger.SoftDeleteAsync(entityModel);
            return Ok(result);
        }
        #endregion
        #endregion
        #region Assign
        [Route("AssignOrderToTechnican")]
        [HttpPost]
        public async Task<bool> AssignOrderToTechnican([FromBody]AssignOrderToTechnicanViewModel model)
        {
            var result =await manger.AssignOrderToTechnican(model.FK_Order_Id, model.FK_Technican_Id);
            var orderentity = await manger.GetAsync(model.FK_Order_Id);
            //var order = mapper.Map<Order, OrderViewModel>(orderentity);
            //orderHub.AssignOrderToTechnican(order, orderingHub, model.FK_Technican_Id);
            await NotifyAssignTechnician(orderentity);
            return result;
        }
        [Route("UnAssignOrderToTechnican/{orderId}")]
        [HttpGet]
        public async Task<bool> UnAssignOrderToTechnican([FromRoute]int orderId)
        {
            var userId =await manger.UnAssignOrderToTechnican(orderId);
            await NotifyUnAssignTechnician(userId);
            return true;
        }
        [Route("AssignOrdersToTechnican")]
        [HttpPost]
        public async Task<bool> AssignOrdersToTechnican([FromBody]AssignMultiOrderToTechnicanViewModel model)
        {
            var result =await manger.AssignOrdersToTechnican(model.FK_Order_Ids, model.FK_Technican_Id);
            var OrderVMlist =await manger.GetGrouptOfOrders(model.FK_Order_Ids);
            if (OrderVMlist != null)
            {
                OrderVMlist.ForEach(async x =>
                { 
                    await NotifyAssignTechnician(x);
                });                              
            }
            return result;
        }

        [Route("TransferOrderToDispatcher")]
        [HttpPost]
        public async Task<bool> TransferOrderToDispatcherAsync([FromBody]AssignOrderToDispatcherViewModel model)
        {

            var result =await manger.TransferOrderToDispatcher(model.FK_Order_Id, model.FK_Dispatcher_Id);
            var order = await manger.GetAsync(model.FK_Order_Id);
            //var order = mapper.Map<Order, OrderViewModel>(orderentity);
            orderHub.AssignOrderToDispatcher(order, orderingHub, model.FK_Dispatcher_Id);
            return result;
        }
        [Route("SetPreferedVisitTime")]
        [HttpPost]
        public async Task<bool> SetPreferedVisitTimeAsync([FromBody]OrderPreferedVisitTimeViewModel model)
        {
            return await manger.SetPreferedVisitTime(model.FK_Order_Id, model.PreferedVisitTime);
        }
        [Route("AssignOrdersToDispatcher")]
        [HttpPost]
        public async Task<bool> AssignOrdersToDispatcherAsync([FromBody]AssignMultiOrderToDispatcherViewModel model)
        {
            return await manger.AssignOrdersToDispatcher(model.FK_Order_Ids, model.FK_Dispatcher_Id);
        }
        [Route("AssignOrderToDispatcher_Technican")]
        [HttpPost]
        public async Task<bool> AssignOrderToDispatcher_TechnicanAsync([FromBody]AssignOrderToDispatcher_TechnicanViewModel model)
        {
            return await manger.AssignOrderToDispatcher_Technican(model.FK_Order_Id, model.FK_Technican_Id, model.FK_Dispatcher_Id);
        }
        [Route("AssignOrdersToDispatcher_Technican")]
        [HttpPost]
        public async Task<bool> AssignOrdersToDispatcher_TechnicanAsync([FromBody]AssignMultiOrderToDispatcher_TechnicanViewModel model)
        {
            return await manger.AssignOrdersToDispatcher_Technican(model.FK_Order_Ids, model.FK_Technican_Id, model.FK_Dispatcher_Id);
        }
        #endregion
        [HttpPost]
        [Route("CustomerSignature")]
        public async Task<IActionResult> CustomerSignature([FromBody]CustomerSignatureViewModel signature)
        {
            signature.OrderProgress.FK_CreatedBy_Id = signature.OrderProgress.FK_Technican_Id;
            var issucceed = await AddUsedItems(signature.UsedItems, signature.OrderProgress.FK_Technican_Id, signature.OrderProgress.FK_Order_Id);
            if (issucceed)
            {
                await AddOrderProgressAsync(signature.OrderProgress);
                var entity =await manger.GetAsync(signature.OrderProgress.FK_Order_Id);
                if (entity != null)
                {
                    if (signature.File != null)
                    {
                        signature.File.FileName = signature.OrderProgress.FK_Order_Id.ToString() + ".jpeg";
                        if (appSettings.SignaturePath == null)
                            appSettings.SignaturePath = "OrderSignatures";
                        UploadImageFileManager imageFileManager = new UploadImageFileManager();
                        string path = $"{appSettings.SignaturePath}/{signature.OrderProgress.FK_Order_Id}";
                        ProcessResult<string> processResult = imageFileManager.AddFile(signature.File, path);
                        if (!processResult.IsSucceeded)
                            return BadRequest(processResult.Exception);
                        entity.SignaturePath = $"{path}/{signature.File.FileName}";
                    }
                    if (signature.ContractSignatureFile != null)
                    {
                        signature.ContractSignatureFile.FileName = signature.OrderProgress.FK_Order_Id.ToString() + ".jpeg";
                        if (appSettings.SignatureContractPath == null)
                            appSettings.SignatureContractPath = "OrderContractSignatures";
                        UploadImageFileManager imageFileManager = new UploadImageFileManager();
                        string path = $"{appSettings.SignatureContractPath}/{signature.OrderProgress.FK_Order_Id}";
                        ProcessResult<string> processResult = imageFileManager.AddFile(signature.ContractSignatureFile, path);
                        if (!processResult.IsSucceeded)
                            return BadRequest(processResult.Exception);
                        entity.SignatureContractPath = $"{path}/{signature.ContractSignatureFile.FileName}";
                    }
                    string filePath = "LoggingText.txt";
                    string entitySignatureContractPath = entity.SignaturePath + " entity.SignatureContractPath= " + entity.SignatureContractPath;
                    FileUtilities.LogToPath(entitySignatureContractPath, filePath);
                    var isUpdated = await manger.UpdateAsync(entity);
                    entitySignatureContractPath = entity.SignaturePath + " entity.SignatureContractPath= " + entity.SignatureContractPath;
                    FileUtilities.LogToPath(entitySignatureContractPath, filePath);

                }
                return Ok(true);
            }
            else
            {
                return BadRequest("Add Used Items Process Failed");
            }

        }

        [HttpPost]
        [Route("CustomerSignatures")]
        public async Task<IActionResult> CustomerSignatures([FromBody]CustomerSignaturesViewModel signature)
        {

            await AddOrderProgressAsync(signature.OrderProgress);


            var issucceed = await AddUsedItems(signature.UsedItems, signature.OrderProgress.FK_Technican_Id, signature.OrderProgress.FK_Order_Id);
            if (issucceed)
            {
                var entity =await manger.GetAsync(signature.OrderProgress.FK_Order_Id);
                if (entity != null)
                {
                    if (signature.OrderFiles != null && signature.OrderFiles.Count > 0)
                    {
                        if (appSettings.OrderFilesPath == null)
                            appSettings.OrderFilesPath = "OrderFiles";
                        UploadImageFileManager imageFileManager = new UploadImageFileManager();
                        string path = $"{appSettings.OrderFilesPath}/{signature.OrderProgress.FK_Order_Id}";

                        for (int i = 0; i < signature.OrderFiles.Count; i++)
                        {
                            var filename = signature.OrderFiles[i].FileName = (i + 1) + ".jpeg";
                            ProcessResult<string> processResult = imageFileManager.AddFile(signature.OrderFiles[i], path);
                            if (processResult.IsSucceeded)
                            {
                                var orderFile = new OrderFiles();
                                orderFile.FileName = filename;
                                orderFile.OrderId = signature.OrderProgress.FK_Order_Id;
                                orderFile.FileRelativePath = processResult.returnData + "/" + filename;
                                filesManger.AddAsync(orderFile);
                            }
                            else
                            {
                                return BadRequest(processResult.Exception);
                            }
                        }
                    }
                    if (signature.ContractSignatureFile != null)
                    {
                        signature.ContractSignatureFile.FileName = signature.OrderProgress.FK_Order_Id.ToString() + ".jpeg";
                        if (appSettings.SignatureContractPath == null)
                            appSettings.SignatureContractPath = "OrderContractSignatures";
                        UploadImageFileManager imageFileManager = new UploadImageFileManager();
                        string path = $"{appSettings.SignatureContractPath}/{signature.OrderProgress.FK_Order_Id}";
                        ProcessResult<string> processResult = imageFileManager.AddFile(signature.ContractSignatureFile, path);
                        if (!processResult.IsSucceeded)
                            return BadRequest(processResult.Exception);
                        entity.SignaturePath = $"{path}/{signature.ContractSignatureFile.FileName}";

                       
                        await  manger.UpdateAsync(entity);
                    }

                }

                return Ok(true);
            }
            else
            {
                return BadRequest("Add Used Items Process Failed");
            }

        }
        [Route("Search")]
        [HttpPost]
        public async Task<IActionResult> Search([FromBody]PaginatedItemsViewModel pagingparametermodel)
        {
            List<OrderViewModel> orderViewModelList = null;
            PagedResult<OrderViewModel> pagedResult = null;

            if (pagingparametermodel.SearchBy != null)
            {

                 pagedResult =await manger.GetOrdersBySearchAsync(pagingparametermodel);
                if (pagedResult != null && pagedResult.Result.Count != 0)
                {
                    orderViewModelList = pagedResult.Result;
                     orderViewModelList = await FillCustomerOrders(orderViewModelList);
                    orderViewModelList = await FillCallOrders(orderViewModelList);
                }
                else
                {
                    CustomerViewModel customerViewModel = await SearchCustomer(pagingparametermodel.SearchBy);
                    if (customerViewModel != null)
                    {
                        pagedResult = await manger.SearchByCustomerIdByPaging(customerViewModel.Id, pagingparametermodel);
                        orderViewModelList = pagedResult.Result;
                        foreach (OrderViewModel orderViewModel in orderViewModelList)
                            orderViewModel.Customer = customerViewModel;

                    }
                    List<CallingViewModel> CallingViewModel = await SearchCalling(pagingparametermodel.SearchBy);
                    List<OrderViewModel> callingEntityResult = null;
                    if (CallingViewModel.Count() > 0)
                    {
                        pagedResult = await manger.SearchByCallIdByPaging(CallingViewModel.FirstOrDefault().Id, pagingparametermodel);
                        callingEntityResult = pagedResult.Result;
                        foreach (OrderViewModel orderViewModel in callingEntityResult)
                            orderViewModel.Call = CallingViewModel.FirstOrDefault();

                    }
                    if (callingEntityResult != null && callingEntityResult.Count != 0)
                    {
                        orderViewModelList = callingEntityResult;
                    }
                }
            }
            return Ok(pagedResult);
        }



        [Route("SearchByCustomerId/{customerId}")]
        [HttpGet]
        public async Task<IActionResult> SearchByCustomerId(int customerId)
        {
            List<OrderViewModel> result = null;
            if (customerId > 0)
            {
                result = await manger.SearchByCustomerId(customerId);
            }
            return Ok(result);
        }


        [Route("GetOrderByCustomerByPaging")]
        [HttpPost]
        public async Task<IActionResult> GetOrderByCustomerByPaging([FromBody]PaginatedItemsViewModel pagingParameterModel)
        {
            PagedResult<OrderViewModel> result = null;
            if (pagingParameterModel.Id > 0)
            {
                 result =await manger.GetOrderByCustomerIdWithPaging(pagingParameterModel);
            }
            return Ok(result);
        }

        public async Task<List<OrderViewModel>> FillCustomerOrders(List<OrderViewModel> orders, string authHeader = null)
        {
           return await   Task<List<OrderViewModel>>.Run(async () =>
            {
                List<OrderViewModel> listActiveOrders = new List<OrderViewModel>();

                foreach (var item in orders)
                {
                    if (item.FK_Customer_Id != null)
                    {
                        try
                        {
                            item.Customer = await GetCustomerById(item.FK_Customer_Id.Value, authHeader);


                            //var CustomerVM = customerManger.Get(item.FK_Customer_Id.Value);


                            //item.Customer = mapper.Map<DispatchProduct.CustomerModule.BLL.ViewModel.CustomerViewModel, DispatchProduct.Ordering.BLL.ServicesViewModels.CustomerViewModel>(CustomerVM);

                            if (item.Customer.IsDeleted != true)
                            {
                                listActiveOrders.Add(item);
                            }
                        }
                        catch (Exception e)
                        {

                        }
                    }
                    else
                    {
                        listActiveOrders.Add(item);
                    }


                }


                return listActiveOrders;
            });
        }

        public async Task<List<OrderViewModel>> FillCallOrders(List<OrderViewModel> orders, string authHeader = null)
        {
            List<OrderViewModel> listActiveOrders = new List<OrderViewModel>();

            foreach (var item in orders)
            {
                if (item.Fk_Call_Id != null)
                {
                    try
                    {
                        item.Call = await GetCallById(item.Fk_Call_Id.Value, authHeader);
                        if (item.Call.IsDeleted != true)
                        {
                            listActiveOrders.Add(item);
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
                else
                {
                    listActiveOrders.Add(item);
                }
            }

            return listActiveOrders;
        }
        public async Task<List<OrderProgressViewModel>> Fill_Tech_Dispatcher_OrderProgress(List<OrderProgressViewModel> orderProgress, string authHeader = null)
        {
            foreach (var item in orderProgress)
            {
                ApplicationUserViewModel user = await GetUser(item.FK_Technican_Id, authHeader);
                if (user != null)
                    item.TechnicanName = user.UserName;
                item.CreatedBy = await GetUser(item.FK_CreatedBy_Id, authHeader);
            }

            return orderProgress;
        }
        private async Task<CustomerViewModel> SearchCustomer(string key, string authHeader = null)
        {
            CustomerViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                    authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
                result = await customerService.SearchCustomer(key, authHeader);
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        private async Task<List<CallingViewModel>> SearchCalling(string key, string authHeader = null)
        {
            List<CallingViewModel> result = null;
            try
            {
                if (Request != null && authHeader == null)
                    authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
                result = await callingService.SearchCallByNameOrNumber(key, authHeader);
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        public async Task<List<OrderViewModel>> FillContractOrders(List<OrderViewModel> orders, string authHeader = null)
        {
            List<OrderViewModel> listActiveOrders = new List<OrderViewModel>();
            foreach (var item in orders)
            {

                    if (item.FK_Contract_Id != null)
                {
                    try
                    {
                        item.Contract = await GetContractById(item.FK_Contract_Id.Value, authHeader);
                        if (item.Contract.IsDeleted != true)
                        {
                            listActiveOrders.Add(item);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }

                    else
                    {
                        listActiveOrders.Add(item);
                    }
               
            }
           
            return listActiveOrders;

        }

        public async Task<List<OrderViewModel>> FillLocationOrders(List<OrderViewModel> orders, string authHeader = null)
        {
            foreach (var item in orders)
            {
                if (item.FK_Location_Id != null)
                {
                    item.Location = await GetLocationById(item.FK_Location_Id.Value, authHeader);
                }
            }
            return orders;
        }
        public async Task<List<OrderViewModel>> Fill_Tech_Dispatcher_Orders(List<OrderViewModel> orders, string authHeader = null)
        {
            foreach (var item in orders)
            {
                var tech = await GetUser(item.FK_Technican_Id, authHeader);
                if (tech != null)
                    item.TechnicanName = tech.UserName;
                var dispatcher = await GetUser(item.FK_Dispatcher_Id, authHeader);
                if (dispatcher != null)
                    item.DispatcherName = dispatcher.UserName;
            }

            return orders;
        }
        [Route("GetOrderByDispatcher/{dispatcherId}")]
        [HttpGet]
        private async Task<ApplicationUserViewModel> GetUser([FromRoute] string usreId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
                authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
            List<string> userIds = new List<string>();
            userIds.Add(usreId);
            string authHeader1 = authHeader;
            return (await userService.GetByUserIds(userIds, authHeader1)).FirstOrDefault();
        }
        public async Task<CustomerViewModel> GetCustomerById(int id, string authHeader = null)
        {

            CustomerViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                result = await customerService.GetItem(id.ToString(), authHeader);
            }
            catch (Exception ex)
            {
                return null; 
            }
            return result;
        }
        public async Task<ContractViewModel> GetContractById(int id, string authHeader = null)
        {

            ContractViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                result = await contractService.GetItem(id.ToString(), authHeader);
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public async Task<CallingViewModel> GetCallById(int id, string authHeader = null)
        {
            CallingViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                result = await callingService.GetItem(id.ToString(), authHeader);
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        public async Task<LocationViewModel> GetLocationById(int locId, string authHeader = null)
        {
            LocationViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                result = await locService.GetItem(locId.ToString(), authHeader);
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        public async Task<ApplicationUserViewModel> GetDispatcherSupervisor(string authHeader = null)
        {
            ApplicationUserViewModel result = null;
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            result = (await userService.GetDispatcherSupervisor(authHeader)).FirstOrDefault();
            return result;
        }
        private async Task<bool> AddUsedItems(List<int> usedItemIds, string FK_TechnicanId, int FK_Order_Id, string authHeader = null)
        {
            bool flag;
            try
            {
                if (usedItemIds != null && usedItemIds.Count > 0)
                {
                    List<TechnicanUsedItemsViewModel> usedItems = new List<TechnicanUsedItemsViewModel>();
                    var usdItmsgroped = usedItemIds.GroupBy((itm => itm)).Select(g => new
                    {
                        itemId = g.Key,
                        Count = g.Count()
                    });
                    if (Request != null && authHeader == null)
                        authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
                    foreach (var data in usdItmsgroped)
                        usedItems.Add(new TechnicanUsedItemsViewModel()
                        {
                            ItemId = data.itemId,
                            Amount = data.Count,
                            FK_Technican_Id = FK_TechnicanId,
                            FK_Order_Id = FK_Order_Id
                        });
                    if (usedItems.Count > 0)
                    {
                        List<TechnicanUsedItemsViewModel> usedItemsViewModelList = await usedItmService.AddUsedItems(usedItems, authHeader);
                        flag = usedItemsViewModelList != null && usedItemsViewModelList.Count > 0;
                    }
                    else
                        flag = true;
                }
                else
                    flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
            }
            return flag;
        }
        private List<OrderViewModel> BindFilesURL(List<OrderViewModel> model)
        {
            foreach (OrderViewModel model1 in model)
                BindFilesURL(model1);
            return model;
        }
        private OrderViewModel BindFilesURL(OrderViewModel model)
        {
            string str = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
            if (model.SignaturePath != null)
            {
                model.SignaturePath = model.SignaturePath.Replace('\\', '/');
                model.SignatureURL = $"{str}/{model.SignaturePath}";
            }
            if (model.SignatureContractPath != null)
            {
                model.SignatureContractPath = model.SignatureContractPath.Replace('\\', '/');
                model.SignatureContractURL = $"{str}/{model.SignatureContractPath}";
            }
            if (model.OrderFiles != null && model.OrderFiles.Count > 0)
            {
                BindFilesURL(model.OrderFiles);
            }

            return model;
        }

        private List<OrderFilesViewModel> BindFilesURL(List<OrderFilesViewModel> model)
        {
            foreach (OrderFilesViewModel model1 in model)
                BindFilesURL(model1);
            return model;
        }
        private OrderFilesViewModel BindFilesURL(OrderFilesViewModel model)
        {
            string str = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
            if (model.FileRelativePath != null)
            {
                model.FileURL = model.FileRelativePath.Replace('\\', '/');
                model.FileURL = $"{str}/{model.FileURL}";
            }
            return model;
        }

        private async Task NotifyAssignTechnician(OrderViewModel orderViewModel)
        {
            var uri = $"{configuration["DropoutServiceSetting:Uri"]}/{configuration["DropoutServiceSetting:SendVerb"]}";
            SendNotificationViewModel model = new SendNotificationViewModel
            {
                Body = "New order assigned to you",
                Title = appSettings.NotifyReference,
                UserId = orderViewModel.FK_Technican_Id,
                Data = new { orderViewModel.Id, orderViewModel.Code }
            };
            await HttpRequestFactory.Post(uri, model);
        }

        private async Task NotifyUnAssignTechnician(string userId)
        {
            var uri = $"{configuration["DropoutServiceSetting:Uri"]}/{configuration["DropoutServiceSetting:SendVerb"]}";
            SendNotificationViewModel model = new SendNotificationViewModel
            {
                Body = "Order unassigned from you",
                Title = appSettings.NotifyReference,
                UserId = userId
            };
            await HttpRequestFactory.Post(uri, model);
        }
    }
}