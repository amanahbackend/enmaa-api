﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Ordering.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Ordering.Models.Entities
{
    public interface IDispatureAreasAndProplems : IBaseEntity
    {

        int Id { get; set; }

        string Proplems { get; set; }

        string Areas { get; set; }
        string Governments { get; set; }


        string FK_Dispatcher_Id { get; set; }

        //Dispatcher Dispatcher { get; set; }

    }
}
