﻿namespace DispatchProduct.Ordering.IEntities
{
    public interface IOrderPriority
    {
        int Id { get; set; }

        string Name { get; set; }

        bool IsDefault { set; get; }
    }
}
