﻿namespace DispatchProduct.Ordering.IEntities
{
    public interface IOrderStatus
    {
        int Id { get; set; }

        string Name { get; set; }
        bool IsDefault { set; get; }

    }
}
