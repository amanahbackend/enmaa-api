﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Ordering.IEntities;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.Entities
{
    public class OrderPriority : BaseEntity, IOrderPriority
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public bool IsDefault { set; get; }
        public ICollection<Order> Orders { get; set; }


    }
}
