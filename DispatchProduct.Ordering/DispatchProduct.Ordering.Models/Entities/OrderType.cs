﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.Entities
{
    public class OrderType : BaseEntity, IOrderType
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public bool IsDefault { set; get; }

        public ICollection<Order> Orders { get; set; }


    }
}
