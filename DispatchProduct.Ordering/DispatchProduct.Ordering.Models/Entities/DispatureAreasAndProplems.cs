﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Ordering.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Ordering.Models.Entities
{
    public class DispatureAreasAndProplems  : BaseEntity, IDispatureAreasAndProplems, IBaseEntity
    {

        public int Id { get; set; }

        public string Proplems { get; set; }

        public string Areas { get; set; }
        public string Governments { get; set; }


        public string FK_Dispatcher_Id { get; set; }

        //public Dispatcher Dispatcher { get; set; }
      }
}
