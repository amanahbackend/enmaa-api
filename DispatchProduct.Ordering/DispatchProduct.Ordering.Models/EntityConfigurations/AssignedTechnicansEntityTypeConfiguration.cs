﻿using DispatchProduct.Ordering.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class AssignedTechnicansEntityTypeConfiguration
        : IEntityTypeConfiguration<AssignedTechnicans>
    {
        public void Configure(EntityTypeBuilder<AssignedTechnicans> OrderStatusConfiguration)
        {
            OrderStatusConfiguration.ToTable("AssignedTechnicans");

            OrderStatusConfiguration.HasKey(o => o.Id);

            OrderStatusConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("OrderStatusseq");

            OrderStatusConfiguration.Property(o => o.FK_Technican_Id)
                .IsRequired();
            OrderStatusConfiguration.Property(o => o.FK_Dispatcher_Id)
               .IsRequired();

          //OrderStatusConfiguration.HasKey(compositeKey => new { compositeKey.FK_Dispatcher_Id, compositeKey.FK_Technican_Id });
            //OrderStatusConfiguration
            //.HasOne(o => o.Dispatcher)
            //.WithMany(o => o.LstAssignedTechnicans) // <--
            //.HasForeignKey(o => o.FK_Dispatcher_Id)
            //.OnDelete(DeleteBehavior.SetNull);

            //OrderStatusConfiguration
            // .HasOne(o => o.Technican)
            // .WithMany(o => o.LstAssignedTechnicans)
            // .HasForeignKey(o => o.FK_Technican_Id)
            //.OnDelete(DeleteBehavior.SetNull);
        }
    }
}
