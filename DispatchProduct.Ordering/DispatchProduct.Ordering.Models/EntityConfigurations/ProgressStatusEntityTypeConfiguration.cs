﻿using DispatchProduct.Ordering.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class ProgressStatusEntityTypeConfiguration
        : IEntityTypeConfiguration<ProgressStatus>
    {
        public void Configure(EntityTypeBuilder<ProgressStatus> OrderStatusConfiguration)
        {
            OrderStatusConfiguration.ToTable("ProgressStatus");

            OrderStatusConfiguration.HasKey(o => o.Id);

            OrderStatusConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("ProgressStatusseq");

            OrderStatusConfiguration.Ignore(o => o.OrderStatus);

            OrderStatusConfiguration.Property(o => o.Name).IsRequired();
            OrderStatusConfiguration.Property(o => o.FK_OrderStatus_Id).IsRequired();
        }
    }
}
