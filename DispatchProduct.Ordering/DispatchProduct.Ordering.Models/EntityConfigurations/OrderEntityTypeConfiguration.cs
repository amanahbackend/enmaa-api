﻿using DispatchProduct.Ordering.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class OrderEntityTypeConfiguration
        : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> OrderConfiguration)
        {
            OrderConfiguration.ToTable("Order");
            OrderConfiguration.HasKey(o =>o.Id);
            OrderConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            OrderConfiguration.Property(o => o.Code).IsRequired(true);
            OrderConfiguration.Property(o => o.SignaturePath).IsRequired(false);

        OrderConfiguration.Property(o => o.FK_Contract_Id).IsRequired(false);
            OrderConfiguration.Property(o => o.FK_Customer_Id).IsRequired(false);
            OrderConfiguration.Property(o => o.QuotationRefNo).IsRequired(false);
            OrderConfiguration.Property(o => o.FK_Location_Id).IsRequired(false);
            OrderConfiguration.Property(o => o.FK_OrderPriority_Id).IsRequired(true);
            OrderConfiguration.Property(o => o.FK_OrderStatus_Id).IsRequired(true);
            OrderConfiguration.Property(o => o.FK_OrderType_Id).IsRequired(true);
            OrderConfiguration.Property(o => o.Note).IsRequired(false);
          //  OrderConfiguration.Ignore(o => o.OrderPriority);
            OrderConfiguration.HasOne(o => o.OrderPriority).WithMany(x => x.Orders).HasForeignKey(x => x.FK_OrderPriority_Id).OnDelete(DeleteBehavior.SetNull);

            //OrderConfiguration.Ignore(o => o.LstOrderProgress);

        //    OrderConfiguration.Ignore(o => o.OrderStatus);

            OrderConfiguration.HasOne(o => o.OrderStatus).WithMany(x => x.Orders).HasForeignKey(x => x.FK_OrderStatus_Id).OnDelete(DeleteBehavior.SetNull);


            //  OrderConfiguration.Ignore(o => o.OrderType);

            OrderConfiguration.HasOne(o => o.OrderType).WithMany(x => x.Orders).HasForeignKey(x => x.FK_OrderType_Id).OnDelete(DeleteBehavior.SetNull);

            //OrderConfiguration.HasOne(o => o.Technican).WithMany(x => x.Orders).HasForeignKey(x => x.FK_Technican_Id).OnDelete(DeleteBehavior.SetNull);

            OrderConfiguration.Ignore(o => o.SignatureURL);

            OrderConfiguration.Ignore(o => o.SignatureContractURL);

            OrderConfiguration.Ignore(o => o.SignatureContractPath);

            //OrderConfiguration.HasOne(o => o.Dispature).WithMany(x=>x.Orders).HasForeignKey(x=>x.FK_Dispatcher_Id).IsRequired(false);
            OrderConfiguration.HasMany(o => o.OrderFiles).WithOne(x => x.Order).HasForeignKey(x => x.OrderId).OnDelete(DeleteBehavior.SetNull);


        }
    }
}
