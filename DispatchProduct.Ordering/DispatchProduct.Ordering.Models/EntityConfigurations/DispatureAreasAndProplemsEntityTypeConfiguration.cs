﻿using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.Models.Entities;
using DispatchProduct.Vehicles.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class DispatureAreasAndProplemsEntityTypeConfiguration
        : IEntityTypeConfiguration<DispatureAreasAndProplems>
    {
        public void Configure(EntityTypeBuilder<DispatureAreasAndProplems> DispatureAreasAndProplemsConfiguration)
        {
            DispatureAreasAndProplemsConfiguration.ToTable("DispatureAreasAndProplems");

            DispatureAreasAndProplemsConfiguration.HasKey(o => o.Id);

            DispatureAreasAndProplemsConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("DispatureAreasAndProplemsseq");

            DispatureAreasAndProplemsConfiguration.Property(o => o.Proplems).IsRequired();
            DispatureAreasAndProplemsConfiguration.Property(o => o.Areas).IsRequired();
            DispatureAreasAndProplemsConfiguration.Property(o => o.Governments).IsRequired();
            DispatureAreasAndProplemsConfiguration.Property(o => o.FK_Dispatcher_Id).IsRequired();

            //DispatureAreasAndProplemsConfiguration.Ignore(o => o.Dispatcher);

        }
    }
}
