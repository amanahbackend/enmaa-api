﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Calling.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Calling.EntityConfigurations
{
    class CallPriorityEntityTypeConfiguration
        : BaseEntityTypeConfiguration<CallPriority>, IEntityTypeConfiguration<CallPriority>
    {
        public override void Configure(EntityTypeBuilder<CallPriority> CallPriorityConfiguration)
        {
            CallPriorityConfiguration.ToTable("CallPriority");

            CallPriorityConfiguration.HasKey(o => o.Id);

            CallPriorityConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("CallPriorityseq");


            CallPriorityConfiguration.Property(o => o.Name)
                .HasMaxLength(500)
                .IsRequired();
        }
    }
}
