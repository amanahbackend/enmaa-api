﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Calling.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Calling.EntityConfigurations
{
    class CallTypeEntityTypeConfiguration
        : BaseEntityTypeConfiguration<CallType>, IEntityTypeConfiguration<CallType>
    {
        public override void Configure(EntityTypeBuilder<CallType> CallTypeConfiguration)
        {
            CallTypeConfiguration.ToTable("CallType");

            CallTypeConfiguration.HasKey(o => o.Id);

            CallTypeConfiguration.Property(o => o.Id)
               .ForSqlServerUseSequenceHiLo("CallTypeSeq");

            CallTypeConfiguration.Property(o => o.Name)
                .HasMaxLength(500)
                .IsRequired();
        }
    }
}
