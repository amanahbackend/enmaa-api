﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Calling.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Calling.EntityConfigurations
{
    class CallStatusEntityTypeConfiguration
        : BaseEntityTypeConfiguration<CallStatus>, IEntityTypeConfiguration<CallStatus>
    {
        public override void Configure(EntityTypeBuilder<CallStatus> CallStatusConfiguration)
        {
            CallStatusConfiguration.ToTable("CallStatus");

            CallStatusConfiguration.HasKey(o => o.Id);

            CallStatusConfiguration.Property(o => o.Id)
               .ForSqlServerUseSequenceHiLo("CallStatusSeq");

            CallStatusConfiguration.Property(o => o.Name)
                .HasMaxLength(500)
                .IsRequired();
        }
    }
}
