﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Calling.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.Calling.EntityConfigurations
{
    class CallEntityTypeConfiguration
        : BaseEntityTypeConfiguration<Call>,IEntityTypeConfiguration<Call>
    {
        public override void Configure(EntityTypeBuilder<Call> CallConfiguration)
        {
            base.Configure(CallConfiguration);

            CallConfiguration.ToTable("Call");

            CallConfiguration.HasKey(o => o.Id);

            CallConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("Callseq");
                

           


            CallConfiguration.Property(o => o.CallerNumber).IsRequired();
            CallConfiguration.Property(o => o.CallStatus).IsRequired();

            CallConfiguration.Property(o => o.PACINumber).IsRequired(false);
            CallConfiguration.Property(o => o.Governorate).IsRequired(false);
            CallConfiguration.Property(o => o.Area).IsRequired(false);
            CallConfiguration.Property(o => o.Block).IsRequired(false);
            CallConfiguration.Property(o => o.Street).IsRequired(false);
            CallConfiguration.Property(o => o.AddressNote).IsRequired(false);
            CallConfiguration.Property(o => o.Latitude).IsRequired();
            CallConfiguration.Property(o => o.Longitude).IsRequired();
            CallConfiguration.HasMany(o => o.Logs).WithOne(x=>x.Call).HasForeignKey(x=>x.FK_Call_Id);

            CallConfiguration.Property(o => o.CallerName).IsRequired();
            CallConfiguration.Property(o => o.CustomerServiceName).IsRequired(false);

            CallConfiguration.Property(o => o.CustomerDescription).IsRequired();
            CallConfiguration.HasOne(o => o.CallPriority).WithMany(x=>x.calls).HasForeignKey(x=>x.FK_CallPriority_Id);
            CallConfiguration.HasOne(o => o.CallType).WithMany(c=>c.calls).HasForeignKey(x=>x.FK_CallType_Id).IsRequired(false);
        }
    }
}
