﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System.Collections.Generic;

namespace DispatchProduct.Calling.Entities
{
    public class CallType : BaseEntity, ICallType, IBaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public bool IsDefault { set; get; }
        public IEnumerable<Call> calls { set; get; }

    }
}
