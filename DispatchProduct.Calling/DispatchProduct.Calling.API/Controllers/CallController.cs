﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingProduct.Calling.BLL.IManagers;
using DispatchProduct.Calling.Entities;
using AutoMapper;
using DispatchProduct.Calling.BLL.ViewModel;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Calling.API.ServicesCommunication.Customer;
using Utilities.Utilites.PACI;
using Utilites;
using DispatchProduct.Calling.API.ServicesCommunication;
using DispatchProduct.Calling.API.Settings;
using DispatchProduct.Calling.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class CallController : Controller
    {
        public ICallManager manger;
        public readonly IMapper mapper;
        public readonly ICustomerService customerService;
        private ILocationService locationService;
        private LocationServiceSetting locationSettings;
        private ICallingHub hub;
        private IHubContext<CallingHub> callingHub;

        public CallController(ICallManager _manger, IMapper _mapper, ICustomerService _customerService, ILocationService _locationService, IOptions<LocationServiceSetting> _locationSettings, ICallingHub _hub, IHubContext<CallingHub> _callingHub)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            this.customerService = _customerService;
            this.locationService = _locationService;
            this.locationSettings = _locationSettings.Value;
            this.hub = _hub;
            this.callingHub = _callingHub;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        [Route("Get/{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            CallViewModel result = new CallViewModel();
      
            result = await manger.Get(id);                              
            return Ok(result);
        }
        [HttpGet]
        [Route("GetALL")]
        public async Task<IActionResult> GetAsync()
        {
            var entityResult = await manger.GetAllAsync();
            return Ok(entityResult);
        }


        [HttpPost]
        [Route("GetALLByPaging")]
        public async Task<IActionResult> GetALLByPagingAsync([FromBody]PaginatedItemsViewModel pagingparametermodel)
        {
            if (pagingparametermodel.PageNumber < 1 && pagingparametermodel.PageSize < 1)
                return BadRequest();
            var entityResult = await manger.GetAllPagingAsync(pagingparametermodel);
            return Ok(entityResult);
        }

        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]CallViewModel model)
        {
            model.CallStatus = "started";
            bool locattrMissing = false;
            Tuple<Point, bool> coordinates = await GetCoordinates(model, locattrMissing, 0);
            if (coordinates.Item2)
                return BadRequest("Call Location attributes missing ");

            if (coordinates.Item2 || coordinates.Item1 == null)
            {
                model.Latitude  = 0;
                model.Longitude = 0;
            }
            else
            {
                model.Latitude = coordinates.Item1.Latitude.Value;
                model.Longitude = coordinates.Item1.Longitude.Value;
            }

            //var entityResult = mapper.Map<CallViewModel, Call>(model);
            var result = await manger.AddAsync(model);
            //var result = mapper.Map<Call, CallViewModel>(entityResult);

            if (result != null)
                await hub.AddCall(result, callingHub);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]CallViewModel model)
        {
            bool result = false;
            result = await manger.UpdateAsync(model);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            var entity = await manger.Get(id);
            // await fillEntityIdentity(entity);
            result =await manger.SoftDeleteAsync(entity);
            return Ok(result);
        }
        #endregion
        #endregion
        #region CustomBuisness
        [Route("Search/{phone}")]
        [HttpGet("{phone}")]
        public async Task<IActionResult> SearchAsync(string phone)
        {
            var entityResult = await manger.Search(phone);
            return Ok(entityResult);
        }


        [Route("SearchByPaging")]
        [HttpPost]
        public async Task<IActionResult> SearchByPagingAsync([FromBody]PaginatedItemsViewModel pagingparametermodel)
        {
            var entityResult = await manger.GetPagingBySearch(pagingparametermodel);
            return Ok(entityResult);
        }

        [Route("SearchByKey")]
        [HttpGet]
        public async Task<IActionResult> SearchByKeyAsync([FromQuery] string key)
        {
            var entitieis = await manger.SearchByNameOrNumber(key);
            return Ok(entitieis);
        }

        [Route("UpdateCallEstimation")]
        [HttpPost]
        public async Task<IActionResult> UpdateCallEstimationAsync([FromBody]CallEstimationViewModel callEstimation)
        {
            if (callEstimation != null && callEstimation.EstimationRefNo != null && callEstimation.CallId > 0)
            {
                 await manger.UpdateCallEstimation(callEstimation.CallId, callEstimation.EstimationRefNo);
                return Ok();
            }
            else
            {
                return BadRequest("one of the parameters has null value");
            }
        }

        [Route("SearchByCustomerId/{customerId}")]
        [HttpGet("{customerId}")]
        public async Task<IActionResult> SearchByCustomerIdAsync(int customerId)
        {
            var entityResult = await manger.GetCallsForCustomer(customerId);
            return Ok(entityResult);
        }


        [Route("GetCallByCustomerIdByPaging")]
        [HttpPost]
        public async Task<IActionResult> SearchByCustomerIdByPagingAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            var entityResult =await manger.GetCallsBySearchPageAsync(pagingparametermodel);
            return Ok(entityResult);
        }

        //[Route("Search/{phone}")]
        //[HttpGet("{phone}")]
        //public IActionResult Search(string phone)
        //{
        //    List<CallViewModel> result = new List<CallViewModel>();
        //    var entityResult = manger.Search(phone);
        //    result = mapper.Map<List<Call>, List<CallViewModel>>(entityResult);
        //    return Ok(result);
        //}


        [HttpPost]
        [Route("UpdateCallByCustomerId")]
        public async Task<IActionResult> UpdateCallByCustomerIdAsync([FromBody]CallViewModel call)
        {
           await  manger.UpdateCallByCustomerIdAsync(call.FK_Customer_Id, call.CallerNumber);
            return Ok();
        }
        #endregion

        [HttpGet]
        [Route("GetCoordinatesByPaci")]
        public async Task<Point> GetCoordinatesByPaci(string paciNo, string authHeader = null)
        {
            if (Request != null && authHeader == null)
                authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
            return await locationService.GetCoordinatesByPaci(paciNo, authHeader);
        }

        [HttpPost]
        [Route("GetCoordinatesByStreetBlock")]
        public async Task<Point> GetCoordinatesByStreet_Block([FromBody] PointInputViewModel Point, string authHeader = null)
        {
            if (Request != null && authHeader == null)
                authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
            return await locationService.GetCoordinatesByStreet_Block(Point, authHeader);
        }

        private async Task<Tuple<Point, bool>> GetCoordinates(CallViewModel model, bool locattrMissing, int recursiveNo = 0)
        {
            Point pointResult = null;
            locattrMissing = true;
            if (!string.IsNullOrEmpty(model.PACINumber))
            {
                pointResult = await GetCoordinatesByPaci(model.PACINumber);
                locattrMissing = false;
            }
            else if (!string.IsNullOrEmpty(model.Street) && !string.IsNullOrEmpty(model.Block))
            {
                locattrMissing = false;
                PointInputViewModel Point = mapper.Map<CallViewModel, PointInputViewModel>(model);
                pointResult = await GetCoordinatesByStreet_Block(Point);
            }
            if (!locattrMissing && pointResult == null && recursiveNo < locationSettings.RetryNo)
            {
                recursiveNo = ++recursiveNo;
                Tuple<Point, bool> coordinates = await GetCoordinates(model, locattrMissing, recursiveNo);
            }
            return new Tuple<Point, bool>(pointResult, locattrMissing);
        }
    }
}
