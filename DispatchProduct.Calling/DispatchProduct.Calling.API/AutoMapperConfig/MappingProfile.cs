﻿using AutoMapper;
using DispatchProduct.Calling.BLL.ViewModel;
using DispatchProduct.Calling.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Calling.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<CallPriority, CallPriorityViewModel>(MemberList.None);
            CreateMap<CallPriorityViewModel, CallPriority>(MemberList.None);

            CreateMap<CallType, CallTypeViewModel>(MemberList.None);
            CreateMap<CallTypeViewModel, CallType>(MemberList.None);

            CreateMap<CallStatusViewModel, CallStatus>(MemberList.None);
            CreateMap<CallStatus, CallStatusViewModel>(MemberList.None);

            CreateMap<CallLog, CallLogViewModel>(MemberList.None)
            .ForMember(dest => dest.CallStatus, opt => opt.MapFrom(src => src.CallStatus));
            CreateMap<CallLogViewModel, CallLog>(MemberList.None)
            .ForMember(dest => dest.CallStatus, opt => opt.MapFrom(src => src.CallStatus));


           CreateMap<CallViewModel, PointInputViewModel>(MemberList.None)
                .ForMember(dest => dest.Street,opt => opt.MapFrom(src => src.Street))
                .ForMember(dest => dest.Block,opt => opt.MapFrom(src => src.Block))
                .ForAllOtherMembers(dest => dest.Ignore());
            CreateMap<PointInputViewModel, CallViewModel>(MemberList.None)
                .ForMember(dest => dest.Street,opt => opt.MapFrom(src => src.Street))
                .ForMember(dest => dest.Block,opt => opt.MapFrom(src => src.Block))
                .ForAllOtherMembers(dest => dest.Ignore());

            CreateMap<CallViewModel, Call>(MemberList.None)
            .ForMember(dest => dest.CallPriority, opt => opt.MapFrom(src => src.CallPriority))
            .ForMember(dest => dest.CallStatus, opt => opt.MapFrom(src => src.CallStatus))
            .ForMember(dest => dest.CallType, opt => opt.Ignore())
            .ForMember(dest => dest.Logs, opt => opt.MapFrom(src => src.Logs));

            CreateMap<Call, CallViewModel>(MemberList.None)
           .ForMember(dest => dest.CallPriority, opt => opt.MapFrom(src => src.CallPriority))
           .ForMember(dest => dest.CallStatus, opt => opt.MapFrom(src => src.CallStatus))
           .ForMember(dest => dest.Logs, opt => opt.MapFrom(src => src.Logs));


        }
    }
}
