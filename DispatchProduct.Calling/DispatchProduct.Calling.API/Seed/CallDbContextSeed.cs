﻿
using DispatchProduct.Calling.API.Settings;
using DispatchProduct.Calling.Entities;
using DispatchProduct.Identity.Context;
using DispatchProduct.Repoistry;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Calling.API.Seed
{
    public class CallDbContextSeed : ContextSeed
    {
        public async Task SeedAsync(CallDbContext context, IHostingEnvironment env,
            ILogger<CallDbContextSeed> logger, IOptions<CallAppSettings> settings, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                var useCustomizationData = settings.Value.UseCustomizationData;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<CallType> callTypes = new List<CallType>();
                List<CallStatus> callStatus = new List<CallStatus>();
                List<CallPriority> callPriority = new List<CallPriority>();

                if (useCustomizationData)
                {
                    //from file e.g (look at ApplicationDbContextSeed)
                }
                else
                {
                    //default from here
                    callTypes = GetDefaultCallTypes();
                    callStatus = GetDefaultCallStatus();
                    callPriority = GetDefaultCallPriority();
                }
                await SeedEntityAsync(context, callTypes);
            
                await SeedEntityAsync(context, callStatus);
           
                await SeedEntityAsync(context, callPriority);
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for ApplicationDbContext");

                    await SeedAsync(context, env, logger, settings, retryForAvaiability);
                }
            }
        }
        private List<CallType> GetDefaultCallTypes()
        {
            List<CallType> result = new List<CallType>
            {
                new CallType() { Name = "Existing Client" , IsDefault=true },
                new CallType() { Name = "New Client" ,IsDefault=true}
            };
            return result;
        }

        private List<CallStatus> GetDefaultCallStatus()
        {
            List<CallStatus> result = new List<CallStatus>
            {
                new CallStatus() { Name = "Pending Check" ,IsDefault=true},
                new CallStatus() { Name = "Checked", IsDefault=true },
                new CallStatus() { Name = "In Surveying" ,IsDefault=true},
                new CallStatus() { Name = "Surveyed" ,IsDefault=true},
                new CallStatus() { Name = "under Estimation",IsDefault=true },
                new CallStatus() { Name = "Estimated" ,IsDefault=true},
                new CallStatus() { Name = "Under Quotation" ,IsDefault=true },
                new CallStatus() { Name = "Quoted" , IsDefault=true}
            };

            return result;
        }

        private List<CallPriority> GetDefaultCallPriority()
        {
            List<CallPriority> result = new List<CallPriority>
            {
                new CallPriority() { Name = "High" ,IsDefault=true},
                new CallPriority() { Name = "Medium" ,IsDefault=true},
                new CallPriority() { Name = "Low",IsDefault=true }
            };
            return result;
        }

    }
}
