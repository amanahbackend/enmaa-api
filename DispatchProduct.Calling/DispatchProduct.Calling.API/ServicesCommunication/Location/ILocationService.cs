﻿using DispatchProduct.Calling.API.Settings;
using DispatchProduct.Calling.BLL.ViewModel;
using DispatchProduct.HttpClient;
using System.Threading.Tasks;
using Utilities.Utilites.PACI;

namespace DispatchProduct.Calling.API.ServicesCommunication
{
    public interface ILocationService : IDefaultHttpClientCrud<LocationServiceSetting, PointInputViewModel, Point>
    {
        Task<Point> GetCoordinatesByPaci(string Paci, string authHeader = "");

        Task<Point> GetCoordinatesByStreet_Block(PointInputViewModel point, string authHeader = "");
    }
}
