﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchProduct.Calling.EFCore.MSSQL.Migrations
{
    public partial class NewAreaFaildsinCall : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "AppartmentNo",
                table: "Call",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "Building",
                table: "Call",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "FloorNo",
                table: "Call",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "HouseNo",
                table: "Call",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AppartmentNo",
                table: "Call");

            migrationBuilder.DropColumn(
                name: "Building",
                table: "Call");

            migrationBuilder.DropColumn(
                name: "FloorNo",
                table: "Call");

            migrationBuilder.DropColumn(
                name: "HouseNo",
                table: "Call");
        }
    }
}
