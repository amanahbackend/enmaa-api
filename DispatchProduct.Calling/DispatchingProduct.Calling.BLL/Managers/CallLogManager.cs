﻿using DispatchingProduct.Calling.BLL.IManagers;
using DispatchProduct.Calling.Entities;
using DispatchProduct.Identity.Context;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DispatchingProduct.Calling.BLL.Managers
{
    public class CallLogManager:Repositry<CallLog>,ICallLogManager
    {
        ICallManager callManger;
        ICallStatusManager callStatusManager;
        public CallLogManager(CallDbContext context,ICallManager _callManger,ICallStatusManager _callStatusManager)
            : base(context)
        {
            callManger=_callManger;
            callStatusManager = _callStatusManager;
        }
        public List<CallLog> GetByCallId(int callId)
        {
            var callLogs= GetAll().Where(cal=>cal.FK_Call_Id==callId).ToList();
            if (callLogs != null && callLogs.Count > 0)
            {
                foreach (var log in callLogs)
                {
                    log.CallStatus = callStatusManager.Get(log.FK_CallStatus_Id);
                }
            }
            return callLogs;
        }
        public async System.Threading.Tasks.Task<List<CallLog>> GetByCustomerPhoneNoAsync(string customerPhone)
        {
            List<CallLog> result = new List<CallLog>();
            var calls = await callManger.GetAllAsync();


            foreach (var item in calls.Where(cal => cal.CallerNumber == customerPhone))
            {
                var logs= GetAll().Where(cal => cal.FK_CallStatus_Id == item.Id).ToList();
                if (logs.Count > 0) {
                    result.AddRange(logs);
                }
            }
            return result;
        }
        public  async System.Threading.Tasks.Task<CallLog> AddNewLoggAsync(CallLog entity)
        {
            CallLog result=(CallLog)base.Add(entity);

            var call =await  callManger.Get(entity.FK_Call_Id);
            var CallStatus= callStatusManager.Get(entity.FK_CallStatus_Id);
            if (CallStatus != null)
            {
                call.CallStatus = CallStatus.Name;
            }
           await callManger.UpdateAsync(call);
            return result;
        }
    }
}
