﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DispatchingProduct.Calling.BLL.IManagers;
using DispatchProduct.Calling.BLL.ViewModel;
using DispatchProduct.Calling.Entities;
using DispatchProduct.Identity.Context;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace DispatchingProduct.Calling.BLL.Managers
{
    public class CallManager :  BaseManager<CallViewModel, Call>, ICallManager
    {


        public CallManager(CallDbContext context , IMapper _mapper , IRepositry<Call> _repository)
            : base(context , _repository, _mapper)
        {
        }


       public  async Task<PagedResult<CallViewModel>> GetAllPagingAsync(PaginatedItemsViewModel pagingparametermodel) {


            return await base.GetAllByPaginationAsync(repository.GetAll(), pagingparametermodel);


        }


        public async Task<CallViewModel> Get(int Id)
        {
            return await Task.Run(() =>
            {
                var callModel= (context as CallDbContext ).Call.FirstOrDefault(x=>x.Id == Id);

                var CallVM = mapper.Map<Call, CallViewModel>(callModel);

                return CallVM;



            });
        }


         public async Task<List<CallViewModel>> Search(string key)
        {
           return await Task.Run(() => { 
            var result = repository.GetAll().Where(
                cal =>
                cal.CallerName.ToLower().Contains(key.ToLower()) ||
                cal.CivilId.ToLower().Contains(key.ToLower()) ||
                cal.CallerNumber.ToLower().Contains(key.ToLower()) ||
                cal.Area.ToLower().Contains(key.ToLower()) ||
                cal.Block.ToLower().Contains(key.ToLower()) ||
                cal.Street.ToLower().Contains(key.ToLower()) ||
                cal.Governorate.ToLower().Contains(key.ToLower()) ||
                cal.CallStatus.ToLower().Contains(key.ToLower()) ||
                cal.PACINumber.ToLower().Contains(key.ToLower())
                ).ProjectTo<CallViewModel>().ToList();
            return result;
            }
          );
        }




        public async Task<List<CallViewModel>> GetCallsForCustomer(int customerid)
        {
            return await Task.Run(() =>
            {

               var result = repository.GetAll().Where(
                cal => cal.FK_Customer_Id == customerid
                            ).ProjectTo<CallViewModel>().ToList();
                return result;
            });
        }


        public async Task<PagedResult<CallViewModel>> GetCallsBySearchPageAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            var orderListQuery = repository.GetAll().Where( cal => cal.FK_Customer_Id == pagingparametermodel.Id).AsQueryable();

            var result = await base.GetAllByPaginationAsync(orderListQuery, pagingparametermodel);
            return result;
        }


        public async Task<List<CallViewModel>> UpdateCallByCustomerIdAsync(int customerId, string phoneNumber)
        {
            
             var result =repository.GetAll().Where(cal => cal.CallerNumber == phoneNumber).ProjectTo<CallViewModel>().ToList();
            foreach (var call in result)
            {
                call.FK_Customer_Id = customerId;
                call.NeedAction = false;
                await  base.UpdateAsync(call);
            }
            return result;
        }
        public async  Task<bool> UpdateCallEstimation(int callId, string estimationRefNo)
        {

            return await Task.Run(() =>
            {
                bool result = false;


                var call = (context as CallDbContext).Call.FirstOrDefault(x => x.Id == callId);
                if (call != null)
                {
                    call.HasEstimation = true;
                    call.EstimationRefNo = estimationRefNo;
                }
                result = repository.Update(call);
                return result;
            });
        }
        public async Task <List<CallViewModel>> SearchByNameOrNumber(string key)
        {
            return await Task.Run(() =>
            {
                if (key != null) { 
                  var   result = repository.GetAll().Where(
                    cal =>
                    cal.CallerName.ToLower().Contains(key.ToLower()) ||
                    cal.CallerNumber.ToLower().Contains(key.ToLower())).ProjectTo<CallViewModel>().ToList();
                return result;
                }
                else
                {
                    return null;
                }

            });

        }



        public async Task<PagedResult<CallViewModel>> GetPagingBySearch(PaginatedItemsViewModel pagingparametermodel)
        {

            var result = repository.GetAll().Where(
                  cal =>
                  cal.CallerName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                  cal.CivilId.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                  cal.CallerNumber.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                  cal.Area.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                  cal.Block.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                  cal.Street.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                  cal.Governorate.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                  cal.CallStatus.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                  cal.PACINumber.ToLower().Contains(pagingparametermodel.SearchBy.ToLower())
                  ).AsQueryable().AsNoTracking();




            var Filterdresult = await  base.GetAllByPaginationAsync(result, pagingparametermodel);

            //if (pagingparametermodel.PageNumber == 0)
            //    pagingparametermodel.PageNumber = 1;

            //var source = result.Where(x => x.IsDeleted == false).AsQueryable().AsNoTracking();
            //// Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            //int CurrentPage = pagingparametermodel.PageNumber;

            //// Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            //int PageSize = pagingparametermodel.PageSize;
            //totalElements = source.Count();
            //var items = source.Skip((CurrentPage - 1) * PageSize).Take(PageSize);
            //return items.ToList();

            return Filterdresult;
        }

        
    }
}
