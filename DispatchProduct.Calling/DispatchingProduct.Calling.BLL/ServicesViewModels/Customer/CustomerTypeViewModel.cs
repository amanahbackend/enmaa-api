﻿using DispatchProduct.Calling.BLL.ViewModel;

namespace DispatchProduct.Calling.BLL.ServicesViewCustomer
{
    public class CustomerTypeViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public bool IsDefault { set; get; }

    }
}
