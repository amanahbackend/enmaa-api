﻿using DispatchProduct.Calling.BLL.ViewModel;
using DispatchProduct.Calling.Entities;
using DispatchProduct.Repoistry;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace DispatchingProduct.Calling.BLL.IManagers
{
    public interface ICallManager :  IBaseManager<CallViewModel, Call>
    {
        Task<List<CallViewModel>> Search(string key);
        Task<PagedResult<CallViewModel>> GetCallsBySearchPageAsync(PaginatedItemsViewModel pagingparametermodel);

        Task<PagedResult<CallViewModel>> GetAllPagingAsync(PaginatedItemsViewModel pagingparametermodel);

        // List<Call> SearchByCallIdByPaging(int customerId, PaginatedItemsViewModel pagingparametermodel, out int totalNumbers);

        Task<PagedResult<CallViewModel>> GetPagingBySearch(PaginatedItemsViewModel pagingparametermodel);
        Task<CallViewModel> Get(int Id);

        Task<bool> UpdateCallEstimation(int callId, string estimationRefNo);
       Task<List<CallViewModel>> SearchByNameOrNumber(string key);
        Task<List<CallViewModel>> GetCallsForCustomer(int customerid);


        Task<List<CallViewModel>> UpdateCallByCustomerIdAsync(int customerId, string phoneNumber);
    }
}
