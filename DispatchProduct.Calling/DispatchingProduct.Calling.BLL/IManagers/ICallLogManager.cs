﻿using DispatchProduct.Repoistry;
using DispatchProduct.Calling.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingProduct.Calling.BLL.IManagers
{
    public interface ICallLogManager:IRepositry<CallLog>
    {
        List<CallLog> GetByCallId(int callId);
        System.Threading.Tasks.Task<List<CallLog>> GetByCustomerPhoneNoAsync(string customerPhone);
        System.Threading.Tasks.Task<CallLog> AddNewLoggAsync(CallLog entity);
    }
}
