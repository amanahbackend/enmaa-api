﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DispatchProduct.Calling.Entities;
using DispatchProduct.Calling.BLL.ViewModel;

namespace DispatchProduct.Calling.BLL.ViewModel
{
    public class UpdateCustomerCallViewModel : BaseEntityViewModel
    {
        public string CallerNumber { get; set; }

        public int FK_Customer_Id { get; set; }

    }

    
}
