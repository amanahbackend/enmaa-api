﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DispatchProduct.Calling.Entities;
using DispatchProduct.Calling.BLL.ViewModel.Infrastructure;

namespace DispatchProduct.Calling.BLL.ViewModel
{
    public class CallEstimationViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        ////dd
        public int CallId { get; set; }
        public string EstimationRefNo { get; set; }
    }
}
