﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DispatchProduct.Calling.Entities;
using DispatchProduct.Calling.BLL.ViewModel.Infrastructure;
using DispatchProduct.Calling.BLL.ViewModel;

namespace DispatchProduct.Calling.BLL.ViewModel
{
    public class CallPriorityViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDefault { set; get; }

    }
}
