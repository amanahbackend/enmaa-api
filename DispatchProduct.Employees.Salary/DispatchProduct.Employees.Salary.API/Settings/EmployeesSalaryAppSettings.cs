﻿namespace DispatchProduct.Employees.Salary.API.Settings
{
    public class EmployeesSalaryAppSettings
    {
        public bool UseCustomizationData { get; set; }
        public string IdentityUrl { get; set; }
        public string ClientId { get; set; }
        public string Secret { get; set; }
        public string ExclPath { get; set; }
    }
}
