﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Employees.Salary.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Employees.Salary.Entities
{
    public class Period:BaseEntity, IPeriod
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
