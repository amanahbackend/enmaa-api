﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Employees.Salary.IEntities
{
    public interface ICurrency : IBaseEntity
    {
        int Id { get; set; }
        string Name { get; set; }
    }
}
