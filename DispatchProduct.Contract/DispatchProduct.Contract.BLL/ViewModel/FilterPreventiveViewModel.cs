﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

namespace DispatchProduct.Contracting.BLL.ViewModel
{
    public class FilterPreventiveViewModel
    {
        public List<string> ContractNumbers { get; set; }

        public DateTime? OrderDateFrom { get; set; }

        public DateTime? OrderDateTo { get; set; }

        public List<int> FK_Customer_Ids { get; set; }

        public List<int> FK_Contract_Ids { get; set; }

        public List<int> FK_Location_Ids { get; set; }

        public List<int> FK_OrderPriority_Ids { get; set; }

        public List<int> FK_OrderType_Ids { get; set; }

        public List<int> FK_OrderProblem_Ids { get; set; }

        public List<string> QuotationRefNos { get; set; }

        public List<int> FK_Order_Ids { get; set; }

        public PaginatedItemsViewModel paginatedItemsViewModel { get; set; }


    }
}
