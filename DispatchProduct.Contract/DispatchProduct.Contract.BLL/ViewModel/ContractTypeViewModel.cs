﻿namespace DispatchProduct.Contracting.BLL.ViewModel
{
    public class ContractTypeViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public bool IsDefault { set; get; }

    }
}
