﻿
using DispatchProduct.Contracting.BLL.ServicesViewModels;

namespace DispatchProduct.Contracting.BLL.ViewModel
{
    public class ContractQuotationViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string QuotationRefNumber { get; set; }

        public int Fk_Contract_Id { get; set; }

        public ContractViewModel Contract { get; set; }

        public QuotationViewModel Quotation { get; set; }
    }
}