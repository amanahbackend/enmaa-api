﻿using DispatchProduct.Contracting.BLL.ViewModel;
using DispatchProduct.Contracting.Entities;
using DispatchProduct.Repoistry;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace DispatchProduct.Contracting.BLL.IManagers
{
    public interface IContractManager  : IBaseManager<ContractViewModel, Contract>
    {
        Task<List<ContractViewModel>> GetByCustomerId(int customerId);
        Task<PagedResult<ContractViewModel>> GetByCustomerIdByPaging(PaginatedItemsViewModel pagingParameterModel);

        Task<PagedResult<ContractViewModel>> GetContractsByCustomerSearch(int customerId, PaginatedItemsViewModel pagingparametermodel);
        Task<PagedResult<ContractViewModel>> GetContractsBySearchAsync(PaginatedItemsViewModel pagingparametermodel);
        Task<List<Contract>> GetByQuotationRefAync(string quotationRef);
        Task<PagedResult<ContractViewModel>> GetAllContractsFilledPropsByPagingAsync(PaginatedItemsViewModel pagingparametermodel);
        Task<List<ContractViewModel>> Search(string key);

        Task<List<PreventiveMaintainenceSchedule>> GetAllPreventive();
        Task<PagedResult<PreventiveMaintainenceSchedule>> GetAllPreventivePaggination(PaginatedItemsViewModel pagingparametermodel);

        Task<List<PreventiveMaintainenceSchedule>> FilterPreventiveMaintainenceAsync(FilterPreventive filter);
        Task<PagedResult<PreventiveMaintainenceSchedule>> FilterPreventiveMaintainencePagingAsync(PaginatedItemsViewModel pagingparametermodel ,FilterPreventive filter);

        Task<bool> IsContractNumberExistAsync(string contractNum);
        Task<ContractViewModel> Get(int id);
        //Task GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel);
    }
}
