﻿using DispatchProduct.Contracting.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Contracting.BLL.IManagers
{
    public interface IContractQuotationManager : IRepositry<ContractQuotation>
    {
        List<ContractQuotation> GetByContractId(int contractId);
    }
}
