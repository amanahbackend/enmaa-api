﻿using DispatchProduct.Contracting.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace DispatchProduct.Contracting.BLL.IManagers
{
    public interface IPreventiveMaintainenceScheduleManager : IRepositry<PreventiveMaintainenceSchedule>
    {



        List<PreventiveMaintainenceSchedule> GetDailyPreventiveOrders();

        bool UpdateByOrder(List<PreventiveOrderModel> preventiveOrders);

        Task<List<PreventiveMaintainenceSchedule>> Filter(FilterPreventive filter);

    }
}
