﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Contracting.BLL.ServicesViewModels.EstimationViewModel
// Assembly: DispatchProduct.Contract.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 14326448-EC11-4F75-87E3-853819943164
// Assembly location: D:\EnmaaBKp\Enmaa\Contract\DispatchProduct.Contract.API.dll

using DispatchProduct.Contracting.BLL.ViewModel;
using System.Collections.Generic;

namespace DispatchProduct.Contracting.BLL.ServicesViewModels
{
    public class EstimationViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string RefNumber { get; set; }

        public string QuotationRefNumber { get; set; }

        public int FK_Call_Id { get; set; }

        public double Price { get; set; }

        public bool HasQuotation { get; set; }

        public List<EstimationItemViewModel> lstEstimationItems { get; set; }

        public string CustomerName { get; set; }

        public string CustomerMobile { get; set; }

        public string Area { get; set; }

        public CallViewModel Call { get; set; }
    }
}
