﻿
using DispatchProduct.Contracting.BLL.ViewModel;

namespace DispatchProduct.Contracting.BLL.ServicesViewModels
{
    public class CustomerTypeViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
