﻿
using DispatchProduct.Contracting.BLL.ViewModel;

namespace DispatchProduct.Contracting.BLL.ServicesViewModels
{
    public class ComplainViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Note { get; set; }

        public int FK_Customer_Id { get; set; }

        public CustomerViewModel Customer { get; set; }
    }
}
