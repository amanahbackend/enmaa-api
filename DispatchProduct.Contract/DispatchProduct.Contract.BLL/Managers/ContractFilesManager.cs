﻿
using DispatchProduct.Contracting.BLL.IManagers;
using DispatchProduct.Contracting.Context;
using DispatchProduct.Contracting.Entities;
using DispatchProduct.Contracting.Settings;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Utilites.UploadFile;

namespace DispatchProduct.Contracting.BLL.Managers
{
    public class ContractFilesManager : Repositry<ContractFiles>, IContractFilesManager, IRepositry<ContractFiles>
    {
        private ContractAppSettings ContractSettings;
        private IUploadFileManager uploadFileManager;

        public ContractFilesManager(ContractDbContext context, IOptions<ContractAppSettings> _ConSettings, IUploadFileManager _uploadFileManager)
          : base((DbContext)context)
        {
            this.ContractSettings = _ConSettings.Value;
            this.uploadFileManager = _uploadFileManager;
        }

        public async Task<List<ContractFiles>> UploadContractFiles(List<ContractFiles> files, int contractId)
        { 
            var contractFilesList = new List<ContractFiles>();
            if (this.ContractSettings.ContractFilesPath == null)
                this.ContractSettings.ContractFilesPath = "ContractFiles";

         var contractFiles=   await Task.Run(() =>
             {
                 if (this.uploadFileManager.AddFiles(files.Cast<Utilites.UploadFile.UploadFile>().ToList<Utilites.UploadFile.UploadFile>(), string.Format("{0}/{1}", (object)this.ContractSettings.ContractFilesPath, (object)contractId)).IsSucceeded)
                 {
                     foreach (ContractFiles file in files)
                     {
                         file.FK_Contract_Id = contractId;
                         contractFilesList.Add(this.AddAsync(file));
                     }
                 }
                 return contractFilesList;
             });

            return contractFiles;
        }

        public List<ContractFiles> GetByContractId(int contractId)
        {
            return GetAll().Where(con => con.FK_Contract_Id == contractId).ToList();
        }
    }
}
