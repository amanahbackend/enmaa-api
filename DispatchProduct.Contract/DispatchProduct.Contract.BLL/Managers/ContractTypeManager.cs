﻿using DispatchProduct.Contracting.BLL.IManagers;
using DispatchProduct.Contracting.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using DispatchProduct.Contracting.Context;

namespace DispatchProduct.Contracting.BLL.Managers
{
    public class ContractTypeManager : Repositry<ContractType>, IContractTypeManager
    {
        public ContractTypeManager(ContractDbContext context) : base(context)
        {
        }
    }
}
