﻿using DispatchProduct.Contracting.BLL.IManagers;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using DispatchProduct.Contracting.Context;
using DispatchProduct.Contracting.Entities;
using System.Linq;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;
using DispatchProduct.Contracting.Settings;
using Utilites;
using DispatchProduct.Contracting.IEntities;
using System.Threading.Tasks;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using DispatchProduct.Contracting.BLL.ViewModel;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Utilities.Utilites.Paging;

namespace DispatchProduct.Contracting.BLL.Managers
{
    public class ContractManager : BaseManager<ContractViewModel, Contract>, IContractManager
    {
        ContractAppSettings ContractSettings;
        IServiceProvider serviceProvider;
        IContractQuotationManager contQuotmanager;
        private IContractFilesManager contractFileManager;
        IPreventiveMaintainenceScheduleManager prevManager;

        public ContractManager(ContractDbContext _context, IContractQuotationManager _contQuotmanager, IRepositry<Contract> _repository,
            IMapper _mapper, IOptions<ContractAppSettings> _ConSettings, IServiceProvider _serviceProvider, IPreventiveMaintainenceScheduleManager _prevManager, IContractFilesManager _contractFileManager)
            : base(_context, _repository, _mapper)
        {
            ContractSettings = _ConSettings.Value;
            serviceProvider = _serviceProvider;
            contQuotmanager = _contQuotmanager;
            prevManager = _prevManager;
            contractFileManager = _contractFileManager;
        }

        public async Task<List<Contract>> GetByQuotationRefAync(string quotationRef)
        {
            var lstContracts = await Task.Run(() =>
            {
                return contQuotmanager.GetAll().Include(cd => cd.Contract).Where(cq => cq.QuotationRefNumber == quotationRef).Select(cd => cd.Contract).ToListAsync();
            });

            return lstContracts;
        }


        #region overrided methods
        public override async Task<ContractViewModel> AddAsync(ContractViewModel entityVM)
        {
            //entity.ContractNumber = CreateRefNumber(entity);
            var ContractEntity = mapper.Map<ContractViewModel, Contract>(entityVM);

            var contractFiles = mapper.Map<List<ContractFilesViewModel>, List<ContractFiles>>(entityVM.ContractFiles);

            ContractEntity.ContractFiles = new List<ContractFiles>();

            var   AddEntityresult =  repository.AddAsync(ContractEntity);

            var UploadedContractFiles =await this.contractFileManager.UploadContractFiles(contractFiles, AddEntityresult.Id);
            ContractEntity.ContractFiles.AddRange(UploadedContractFiles);

            var  ContractVm = mapper.Map<Contract, ContractViewModel>(ContractEntity);
            //var contractQuotations = new List<ContractQuotation>();

            var contractList=  ProcessPreventiveMaintainence(ContractEntity);

            ContractVm.PreventiveMaintainence = mapper.Map<List<PreventiveMaintainenceSchedule>, List<PreventiveMaintainenceScheduleViewModel>>(contractList); 
            return ContractVm;
        }



        public List<PreventiveMaintainenceSchedule> ProcessPreventiveMaintainence(Contract entity)
        {
            List<PreventiveMaintainenceSchedule> result = null;
            if (entity.HasPreventiveMaintainence && entity.PreventiveInfo != null)
            {
                int preventivePeriod;
                int noOfPreventive = GetNoOfPreventiveTimes(entity, out preventivePeriod);
                result = AddPreventiveMaintainenceSchedule(entity, noOfPreventive, preventivePeriod);
            }
            return result;
        }

        public List<PreventiveMaintainenceSchedule> AddPreventiveMaintainenceSchedule(Contract entity, int noOfPreventive, int preventivePeriod)
        {
            List<PreventiveMaintainenceSchedule> result = new List<PreventiveMaintainenceSchedule>();

            var date = entity.StartDate;
            for (int i = 0; i < noOfPreventive; i++)
            {
                date = CalculateNextPreventiveDate(date, preventivePeriod);
                var iPreventive = serviceProvider.GetService<IPreventiveMaintainenceSchedule>();
                PreventiveMaintainenceSchedule Preventive = (PreventiveMaintainenceSchedule)iPreventive;
                Preventive.OrderDate = date;
                Preventive.FK_Customer_Id = entity.FK_Customer_Id;
                Preventive.FK_Contract_Id = entity.Id;
                Preventive.ContractNumber = entity.ContractNumber;
                Preventive.FK_Location_Id = entity.PreventiveInfo.FK_Location_Id;
                Preventive.FK_OrderPriority_Id = entity.PreventiveInfo.FK_OrderPriority_Id;
                Preventive.FK_OrderType_Id = entity.PreventiveInfo.FK_OrderType_Id;
                Preventive.FK_OrderProblem_Id = entity.PreventiveInfo.FK_OrderProblem_Id;
                if (entity.ContractQuotations != null && entity.ContractQuotations.Count > 0)
                {
                    Preventive.QuotationRefNo = entity.ContractQuotations.FirstOrDefault().QuotationRefNumber;
                }
                result.Add(prevManager.AddAsync(Preventive));
            }
            return result;
        }

        public int GetNoOfPreventiveTimes(Contract entity, out int preventivePeriod)
        {
            int result = 0;
            preventivePeriod = 0;
            int noOfDays = Convert.ToInt32((entity.EndDate - entity.StartDate).TotalDays);
            if (entity.PreventivePeriod > 0)
            {
                preventivePeriod = entity.PreventivePeriod;
            }
            else
            {
                preventivePeriod = ContractSettings.PreventivePeriod;
            }
            if (preventivePeriod > 0)
            {
                result = noOfDays / ContractSettings.PreventivePeriod;
            }
            else
            {
                throw new Exception("Preventive Period can't be less than or equal 0 please check Preventive Configuration");
            }
            return result;
        }

        private DateTime CalculateNextPreventiveDate(DateTime date, int preventivePeriod)
        {

            DateTime result;
            result = date.AddDays(preventivePeriod);
            //result = date.AddMinutes(preventivePeriod);
            DayOfWeek day = result.DayOfWeek;
            if (day == DayOfWeek.Friday)
            {
                result.AddDays(1);
                //result = date.AddMinutes(preventivePeriod);
            }
            return result;
        }

        public override async Task<List<ContractViewModel>> AddAsync(List<ContractViewModel> entityLst)
        {
            List<ContractViewModel> ContractResult = new List<ContractViewModel>();
            foreach (var item in entityLst)
            {
                ContractResult.Add(await this.AddAsync(item));
            }
            return ContractResult;
        }


        public async Task<ContractViewModel> Get(int id)
        {
            return await Task<ContractViewModel>.Run(() =>
            {

                return (context as ContractDbContext).Contract.ProjectTo<ContractViewModel>().Where(x => x.Id == id).FirstOrDefault();
            }
          );

        }
        //public override IQueryable<Contract> GetAll()
        //{
        //    return context.Contract.Include(ct => ct.ContractType).Include(cq => cq.ContractQuotations);
        //}

        public async Task<PagedResult<ContractViewModel>> GetAllContractsFilledPropsByPagingAsync(PaginatedItemsViewModel pagingparametermodel)
        {

            var contractQuery = repository.GetAll().AsQueryable();
            var PagedResult = await base.GetAllByPaginationAsync(contractQuery, pagingparametermodel);

            return PagedResult;
        }
        public override async Task<bool> UpdateAsync(ContractViewModel entity)
        {
            var result = await base.UpdateAsync(entity);

            return result;
        }

        public async Task<bool> UpdateAsync(List<ContractViewModel> entityList)
        {
            var result = false;
            foreach (var item in entityList)
            {
                result = await UpdateAsync(item);
            }
            return result;
        }

        public override async Task<bool> DeleteAsync(ContractViewModel entity)
        {
            var contractQuotations = contQuotmanager.GetByContractId(entity.Id);
            contQuotmanager.Delete(contractQuotations);


            return await base.DeleteAsync(entity);
        }
        #endregion overrided methods

        public async Task<List<ContractViewModel>> GetByCustomerId(int customerId)
        {

            return await Task<List<ContractViewModel>>.Run(() =>
            {

                var result = repository.GetAll().Where(c => c.FK_Customer_Id == customerId).ProjectTo<ContractViewModel>().ToList();
                return result;
            }
            );


            //return GetAll().Where(c => c.FK_Customer_Id == customerId).ToList();
        }

        public async Task<PagedResult<ContractViewModel>> GetByCustomerIdByPaging(PaginatedItemsViewModel pagingParameterModel)
        {
            var customerQuery = repository.GetAll().Where(c => c.FK_Customer_Id == pagingParameterModel.Id).AsQueryable();

            var lstContracts = await base.GetAllByPaginationAsync(customerQuery, pagingParameterModel);

            return lstContracts;
        }



        public async Task<PagedResult<ContractViewModel>> GetContractsByCustomerSearch(int customerId, PaginatedItemsViewModel pagingparametermodel)
        {
            var customerQuery = repository.GetAll().Where(c => c.FK_Customer_Id == customerId).AsQueryable();

            var result = await base.GetAllByPaginationAsync(customerQuery, pagingparametermodel);
            return result;
        }




        public async Task<List<ContractViewModel>> Search(string key)
        {
            return await Task<List<ContractViewModel>>.Run(() =>
           {
               return repository.GetAll().Where(c => c.ContractNumber == key || c.Area == key).ProjectTo<ContractViewModel>().ToList();
           }
        );
        }

        public async Task<PagedResult<ContractViewModel>> GetContractsBySearchAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            var ContractsListQuery = repository.GetAll().Where(c => c.ContractNumber == pagingparametermodel.SearchBy || c.Area == pagingparametermodel.SearchBy).AsQueryable();

            var result = await base.GetAllByPaginationAsync(ContractsListQuery, pagingparametermodel);
            return result;
        }



        private int? GetMaxOrderToday(string date)
        {
            int? result = null;
            var lstCon = repository.GetAll().Where(co => co.ContractNumber.StartsWith(ContractSettings.ContractPrefix + date)).Select(e => e.ContractNumber.Replace(ContractSettings.ContractPrefix + date, "")).ToList();
            if (lstCon != null && lstCon.Count > 0)
            {
                result = lstCon.Select(str => int.Parse(str)).Max();
            }
            return result;
        }



        private string CreateRefNumber()
        {
            string result = null;
            string orderNo = ContractSettings.ContractOrderDayStartNo;
            string orderdate = Helper.CreateInfixDateCode();
            int? max = GetMaxOrderToday(orderdate);
            if (max != null)
            {
                orderNo = (max.Value + 1).ToString();
            }
            result = ContractSettings.ContractPrefix + orderdate + orderNo;
            return result;
        }
        private string CreateRefNumber(Contract contract)
        {
            string randomnum;
            string contractnumber;
            bool isexist = false;
            do
            {
                randomnum = GenerateRandom();
                contract.ContractType = (context as ContractDbContext).ContractType.Where(x => x.Id == contract.FK_ContractType_Id).FirstOrDefault();
                contractnumber = contract.ContractType.Code + randomnum;
                isexist = (context as ContractDbContext).Contract.Count(c => c.ContractNumber == contractnumber) > 0;
            } while (isexist);
            return contractnumber;
        }
        private string GenerateRandom()
        {
            Random random = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 5; i++)
            {
                sb.Append(random.Next(0, 9).ToString());
            }
            return sb.ToString();
        }
        public async Task<List<PreventiveMaintainenceSchedule>> GetAllPreventive()
        {
            var allPrev = await Task.Run(() =>
              {
                  var list = prevManager.GetAll().ToList();
                  foreach (PreventiveMaintainenceSchedule maintainenceSchedule in list)
                  {
                      maintainenceSchedule.Contract = (context as ContractDbContext).Contract.Where(x => x.Id == maintainenceSchedule.FK_Contract_Id).FirstOrDefault();
                  }
                  return list;

              });
            return allPrev;

        }




        public async Task<List<PreventiveMaintainenceSchedule>> FilterPreventiveMaintainenceAsync(FilterPreventive filter)
        {
            var maintainenceScheduleList = await prevManager.Filter(filter);
            
            var maintTainceSchedul = await Task.Run(() =>
               {

                   foreach (PreventiveMaintainenceSchedule maintainenceSchedule in maintainenceScheduleList)
                   {
                       maintainenceSchedule.Contract = (context as ContractDbContext).Contract.FirstOrDefault(x => x.Id == maintainenceSchedule.FK_Contract_Id);

                   }
                   return maintainenceScheduleList;

               });

            return maintainenceScheduleList;
        }

        public async Task<bool> IsContractNumberExistAsync(string contractNum)
        {
            return await (context as ContractDbContext).Contract.AnyAsync(x => x.ContractNumber == contractNum.ToLower());
        }

        public async Task<PagedResult<PreventiveMaintainenceSchedule>> GetAllPreventivePaggination(PaginatedItemsViewModel pagingparametermodel)
        {
            var allPrev = await Task.Run(async () =>
            {
                var list =await prevManager.GetAllByPaginationAsync(prevManager.GetAll().Include(x=>x.Contract), pagingparametermodel);
                foreach (PreventiveMaintainenceSchedule maintainenceSchedule in list.Result)
                {
                    maintainenceSchedule.Contract = (context as ContractDbContext).Contract.Where(x => x.Id == maintainenceSchedule.FK_Contract_Id).FirstOrDefault();
                }
                return list;

            });
            return allPrev;
        }

        public async Task<PagedResult<PreventiveMaintainenceSchedule>> FilterPreventiveMaintainencePagingAsync(PaginatedItemsViewModel pagingparametermodel, FilterPreventive filter)
        {

           var FilterQuery =   prevManager.GetAll().Include(x => x.Contract).
           Where(itm => (
           (filter.ContractNumbers != null && filter.ContractNumbers.Count > 0 ? filter.ContractNumbers.Contains(itm.ContractNumber) : true)
           &&
           (filter.FK_Customer_Ids != null && filter.FK_Customer_Ids.Count > 0 ? filter.FK_Customer_Ids.Contains(itm.FK_Customer_Id) : true)
           &&
           (filter.FK_Order_Ids != null && filter.FK_Order_Ids.Count > 0 ? filter.FK_Order_Ids.Contains(itm.FK_Order_Id) : true) && (filter.FK_Contract_Ids != null && filter.FK_Contract_Ids.Count > 0 ? filter.FK_Contract_Ids.Contains(itm.FK_Contract_Id) : true)
           &&
           (filter.FK_Location_Ids != null && filter.FK_Location_Ids.Count > 0 ? filter.FK_Location_Ids.Contains(itm.FK_Location_Id) : true)
           &&
           (filter.FK_OrderPriority_Ids != null && filter.FK_OrderPriority_Ids.Count > 0 ? filter.FK_OrderPriority_Ids.Contains(itm.FK_OrderPriority_Id) : true)
           &&
           (filter.FK_OrderType_Ids != null && filter.FK_OrderType_Ids.Count > 0 ? filter.FK_OrderType_Ids.Contains(itm.FK_OrderType_Id) : true)
           &&
           (filter.FK_OrderProblem_Ids != null && filter.FK_OrderProblem_Ids.Count > 0 ? filter.FK_OrderProblem_Ids.Contains(itm.FK_OrderProblem_Id) : true)
           &&
           (filter.QuotationRefNos != null && filter.QuotationRefNos.Count > 0 ? filter.QuotationRefNos.Contains(itm.QuotationRefNo) : true)
           &&
           (filter.OrderDateFrom != new DateTime?() ? (DateTime?)itm.OrderDate >= filter.OrderDateFrom : true)
           &&
           (filter.OrderDateTo != new DateTime?() ? (DateTime?)itm.OrderDate <= filter.OrderDateTo : true))).AsQueryable();


            var list = await prevManager.GetAllByPaginationAsync(FilterQuery, pagingparametermodel);
            foreach (PreventiveMaintainenceSchedule maintainenceSchedule in list.Result)
            {
                maintainenceSchedule.Contract = (context as ContractDbContext).Contract.Where(x => x.Id == maintainenceSchedule.FK_Contract_Id).FirstOrDefault();
            }
            return list;

           
        }

    
    }
}
