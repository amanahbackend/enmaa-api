﻿using DispatchProduct.Contracting.API.Settings;
using DispatchProduct.HttpClient;
using System.Threading.Tasks;
using DispatchProduct.Contracting.BLL.ServicesViewModels;

namespace DispatchProduct.Contracting.API.ServicesCommunication.Quotation
{
    public interface ICustomerService : IDefaultHttpClientCrud<CustomerServiceSetting, CustomerViewModel, CustomerViewModel>
    {
        Task<CustomerViewModel> CreateCustomerFromCall(CustomerViewModel model, string authHeader = "");
        Task<CustomerViewModel> SearchCustomer(string key, string authHeader = "");
        Task<CustomerViewModel> GetBy(string name, string phone, string authHeader = "");
    }
}
