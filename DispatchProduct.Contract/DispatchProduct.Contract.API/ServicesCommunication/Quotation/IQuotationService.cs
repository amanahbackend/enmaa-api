﻿using DispatchProduct.Contracting.API.Settings;
using DispatchProduct.Contracting.BLL.ServicesViewModels;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Contracting.API.ServicesCommunication.Quotation
{
    public interface IQuotationService : IDefaultHttpClientCrud<QuotationServiceSetting, QuotationViewModel, QuotationViewModel>
    {
        Task<QuotationViewModel> GetQuotationByRefNumber(string key, string authHeader = "");
    }
}
