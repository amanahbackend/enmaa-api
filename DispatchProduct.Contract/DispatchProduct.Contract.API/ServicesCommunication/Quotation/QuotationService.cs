﻿using DispatchProduct.Contracting.API.Settings;
using DispatchProduct.Contracting.BLL.ServicesViewModels;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Contracting.API.ServicesCommunication.Quotation
{
    public class QuotationService : DefaultHttpClientCrud<QuotationServiceSetting, QuotationViewModel, QuotationViewModel>, IQuotationService
    {
        QuotationServiceSetting settings;
        public QuotationService(IOptions<QuotationServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
        public async Task<QuotationViewModel> GetQuotationByRefNumber(string key, string authHeader="")
        {
            var requesturi = $"{settings.Uri}/{settings.GetByRefVerb}/{key}";
            return await GetByUri(requesturi, authHeader);
        }

    }
}
