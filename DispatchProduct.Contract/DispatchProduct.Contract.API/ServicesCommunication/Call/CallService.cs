﻿using DispatchProduct.Api.HttpClient;
using DispatchProduct.Contracting.API.Settings;
using DispatchProduct.Contracting.BLL.ServicesViewModels;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Contracting.API.ServicesCommunication.Call
{
    public class CallService : DefaultHttpClientCrud<CallServiceSetting, CallViewModel, CallViewModel>, ICallService
    {
        CallServiceSetting settings;
        public CallService(IOptions<CallServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
        public async Task<List<CallViewModel>> SearchCall(string key, string authHeader="")
        {
            var requesturi = $"{settings.Uri}/{settings.SearchCallVerb}/{key}";
            return await GetListByUri(requesturi, authHeader);
        }

        public async Task<List<CallViewModel>> UpdateCallByCustomerId(string key, string authHeader = "")
        {
            var requesturi = $"{settings.Uri}/{settings.SearchCallVerb}/{key}";
            return await GetListByUri(requesturi, authHeader);
        }
        public async Task<bool> UpdateCustomerById(CallViewModel model, string auth = "")
        {
            bool result = false;
            var requesturi = $"{settings.Uri}/{settings.UpdateCallByCustomerIdVerb}";
            var response = await HttpRequestFactory.Post(requesturi, model, auth);
            if (response.IsSuccessStatusCode)
            {
                result = true;
            }
            return result;
        }

    }
}
