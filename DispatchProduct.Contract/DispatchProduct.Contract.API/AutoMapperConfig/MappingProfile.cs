﻿using AutoMapper;
using DispatchProduct.Contracting.BLL.ServicesViewModels;
using DispatchProduct.Contracting.BLL.ViewModel;
using DispatchProduct.Contracting.Entities;

namespace DispatchProduct.Contracting.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<ContractType, ContractTypeViewModel>(MemberList.None);
            CreateMap<ContractTypeViewModel, ContractType>(MemberList.None);

            CreateMap<FilterPreventive, FilterPreventiveViewModel>(MemberList.None);
            CreateMap<FilterPreventiveViewModel, FilterPreventive>(MemberList.None);

            CreateMap<PreventiveOrderModel, PreventiveOrderViewModel>(MemberList.None);
            CreateMap<PreventiveOrderViewModel, PreventiveOrderModel>(MemberList.None);

            CreateMap<ContractFiles, ContractFilesViewModel>(MemberList.None)
                .ForMember(dest => dest.Contract, opt => opt.MapFrom(src => src.Contract));
            CreateMap<ContractFilesViewModel, ContractFiles>(MemberList.None)
                .ForMember(dest => dest.Contract, opt => opt.MapFrom(src => src.Contract));

            CreateMap<PreventiveInfo, PreventiveInfoViewModel>(MemberList.None);
            CreateMap<PreventiveInfoViewModel, PreventiveInfo>(MemberList.None);

            CreateMap<PreventiveMaintainenceSchedule, PreventiveMaintainenceScheduleViewModel>(MemberList.None)
                .ForMember(dest => dest.Contract, opt => opt.MapFrom(src => src.Contract))
                .ForMember(dest => dest.Customer, opt => opt.Ignore())
                .ForMember(dest => dest.Location, opt => opt.Ignore());

            CreateMap<PreventiveMaintainenceScheduleViewModel, PreventiveMaintainenceSchedule>(MemberList.None)
                .ForMember(dest => dest.Contract, opt => opt.Ignore());

            CreateMap<CustomerViewModel, CallViewModel>(MemberList.None)
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.PACINumber, opt => opt.MapFrom(src => src.PACINumber))
                .ForMember(dest => dest.Governorate, opt => opt.MapFrom(src => src.Governorate))
                .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.Area))
                .ForMember(dest => dest.Block, opt => opt.MapFrom(src => src.Block))
                .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.Street))
                .ForMember(dest => dest.AddressNote, opt => opt.MapFrom(src => src.AddressNote))
                .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
                .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
                .ForMember(dest => dest.CallerName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.FK_CustomerType_Id, opt => opt.MapFrom(src => src.FK_CustomerType_Id))
                .ForMember(dest => dest.CallerNumber, opt => opt.MapFrom(src => src.PhoneNumber))
                .ForMember(dest => dest.FK_PhoneType_Id, opt => opt.MapFrom(src => src.FK_PhoneType_Id))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForAllOtherMembers(dest => dest.Ignore());



            CreateMap<CallViewModel, CustomerViewModel>(MemberList.None)
               .ForMember(dest => dest.PACINumber, opt => opt.MapFrom(src => src.PACINumber))
               .ForMember(dest => dest.Governorate, opt => opt.MapFrom(src => src.Governorate))
               .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.Area))
               .ForMember(dest => dest.Block, opt => opt.MapFrom(src => src.Block))
               .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.Street))
               .ForMember(dest => dest.AddressNote, opt => opt.MapFrom(src => src.AddressNote))
               .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
               .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.CallerName))
               .ForMember(dest => dest.FK_CustomerType_Id, opt => opt.MapFrom(src => src.FK_CustomerType_Id))
               .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.CallerNumber))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
               .ForMember(dest => dest.FK_PhoneType_Id, opt => opt.MapFrom(src => src.FK_PhoneType_Id))
               .ForAllOtherMembers(dest => dest.Ignore());

            CreateMap<ContractQuotation, ContractQuotationViewModel>(MemberList.None)
                .ForMember(dest => dest.Contract, opt => opt.MapFrom(src => src.Contract))
                .ForMember(dest => dest.Quotation, opt => opt.Ignore());
            CreateMap<ContractQuotationViewModel, ContractQuotation>(MemberList.None)
                .ForMember(dest => dest.Contract, opt => opt.MapFrom(src => src.Contract));

            CreateMap<ContractViewModel, Contract>(MemberList.None)
                .ForMember(dest => dest.ContractType, opt => opt.MapFrom(src => src.ContractType));

            CreateMap<Contract, ContractViewModel>(MemberList.None)
                .ForMember(dest => dest.ContractType, opt => opt.MapFrom(src => src.ContractType))
                .ForMember(dest => dest.Customer, opt => opt.Ignore());

        }
    }
}
