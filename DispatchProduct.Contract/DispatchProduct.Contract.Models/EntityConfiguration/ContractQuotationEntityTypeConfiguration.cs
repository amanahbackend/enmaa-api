﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Contracting.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Contracting.EntityConfiguration
{
    class ContractQuotationEntityTypeConfiguration
        : BaseEntityTypeConfiguration<ContractQuotation>, IEntityTypeConfiguration<ContractQuotation>
    {
        public override void Configure(EntityTypeBuilder<ContractQuotation> ContractQuotationConfiguration)
        {
            base.Configure(ContractQuotationConfiguration);
            ContractQuotationConfiguration.ToTable("ContractQuotation");

            ContractQuotationConfiguration.HasKey(o => o.Id);

            ContractQuotationConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("ContractQuotationseq");

            ContractQuotationConfiguration.Property(o => o.Fk_Contract_Id).IsRequired();
            ContractQuotationConfiguration.Property(o => o.QuotationRefNumber).IsRequired();
            //ContractQuotationConfiguration.Ignore(o => o.Contract);
            ContractQuotationConfiguration.HasOne(c => c.Contract).WithMany(c => c.ContractQuotations).HasForeignKey(c => c.Fk_Contract_Id);

        }
    }
}
