﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Contracting.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Linq.Expressions;

namespace DispatchProduct.Contracting.EntityConfiguration
{
    internal class ContractFilesEntityTypeConfiguration : BaseEntityTypeConfiguration<ContractFiles>, IEntityTypeConfiguration<ContractFiles>
    {
        public override void Configure(EntityTypeBuilder<ContractFiles> ContractTypeConfiguration)
        {
            base.Configure(ContractTypeConfiguration);
            ContractTypeConfiguration.ToTable("ContractFiles");
            ContractTypeConfiguration.HasKey((o => o.Id));
            ContractTypeConfiguration.Property((o => o.Id)).ForSqlServerUseSequenceHiLo<int>("ContractFileseq", (string)null);
           // ContractTypeConfiguration.Ignore((o => o.Contract));
            ContractTypeConfiguration.Ignore((o => o.URL));
            ContractTypeConfiguration.Ignore((o => o.FileContent));
            ContractTypeConfiguration.Property((c => c.FileName)).IsRequired(true).HasMaxLength(500);
            ContractTypeConfiguration.Property((c => c.FileRelativePath)).IsRequired(false);
            ContractTypeConfiguration.Property((c => c.FK_Contract_Id)).IsRequired(true);
            ContractTypeConfiguration.HasOne(c => c.Contract).WithMany(c => c.ContractFiles).HasForeignKey(c => c.FK_Contract_Id);
        }
    }
}
