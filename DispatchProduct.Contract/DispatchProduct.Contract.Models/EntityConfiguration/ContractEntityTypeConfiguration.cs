﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Contracting.EntityConfiguration
{
    class ContractEntityTypeConfiguration
        : BaseEntityTypeConfiguration<Entities.Contract>, IEntityTypeConfiguration<Entities.Contract>
    {
        public override void Configure(EntityTypeBuilder<Entities.Contract> ContractConfiguration)
        {
            base.Configure(ContractConfiguration);
            ContractConfiguration.ToTable("Contract");

            ContractConfiguration.HasKey(o => o.Id);

            ContractConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("Contractseq");

            ContractConfiguration.Property(o => o.ContractNumber).IsRequired();
            ContractConfiguration.Property(o => o.EndDate).IsRequired();
            ContractConfiguration.Property(o => o.FK_ContractType_Id).IsRequired();
            ContractConfiguration.Property(o => o.FK_Customer_Id).IsRequired();
            ContractConfiguration.Property(o => o.StartDate).IsRequired();
            ContractConfiguration.Property(o => o.HasPreventiveMaintainence).IsRequired();
            //  ContractConfiguration.Ignore(o => o.ContractType);
            ContractConfiguration.HasOne(o => o.ContractType).WithMany(x => x.lstContracts).HasForeignKey(o => o.FK_ContractType_Id);
            //ContractConfiguration.Ignore(o => o.ContractFiles);
            ContractConfiguration.HasMany(o => o.ContractFiles).WithOne(x => x.Contract).HasForeignKey(o => o.FK_Contract_Id);

            ContractConfiguration.Ignore(o => o.PreventiveInfo);
            //  ContractConfiguration.Ignore(o => o.ContractQuotations);
            ContractConfiguration.HasMany(o => o.ContractQuotations).WithOne(x => x.Contract).HasForeignKey(o => o.Fk_Contract_Id);

            ContractConfiguration.HasMany(o => o.PreventiveMaintainence).WithOne(x => x.Contract).HasForeignKey(o => o.FK_Contract_Id);



        }
    }
}
