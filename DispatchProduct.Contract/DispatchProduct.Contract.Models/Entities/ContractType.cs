﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Contracting.IEntities;
using System.Collections.Generic;

namespace DispatchProduct.Contracting.Entities
{
    public class ContractType : BaseEntity, IContractType, IBaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public bool IsDefault { set; get; }

        public ICollection<Contract> lstContracts { get; set; }

    }
}
