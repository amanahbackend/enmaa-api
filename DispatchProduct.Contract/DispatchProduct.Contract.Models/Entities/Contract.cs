﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Contracting.IEntities;
using System;
using System.Collections.Generic;

namespace DispatchProduct.Contracting.Entities
{
    public class Contract : BaseEntity, IContract, IBaseEntity
    {
        public int Id { get; set; }

        public string ContractNumber { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public double Price { get; set; }

        public string Remarks { get; set; }

        public bool HasPreventiveMaintainence { get; set; }

        public int PreventivePeriod { get; set; }

        public int FK_Customer_Id { get; set; }

        public int FK_ContractType_Id { get; set; }

        public string Area { get; set; }

        public ContractType ContractType { get; set; }

        public PreventiveInfo PreventiveInfo { get; set; }

        public List<ContractQuotation> ContractQuotations { get; set; }

        public List<PreventiveMaintainenceSchedule> PreventiveMaintainence { get; set; }

        public List<ContractFiles> ContractFiles { get; set; }
    }
}
