﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Contracting.IEntities;

namespace DispatchProduct.Contracting.Entities
{
    public class ContractQuotation : BaseEntity, IContractQuotation, IBaseEntity
    {
        public int Id { get; set; }

        public string QuotationRefNumber { get; set; }

        public int Fk_Contract_Id { get; set; }

        public Contract Contract { get; set; }
    }
}