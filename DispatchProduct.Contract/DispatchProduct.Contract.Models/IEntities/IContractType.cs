﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Contracting.IEntities
{
    public interface IContractType : IBaseEntity
    {
        int Id { get; set; }

        string Name { get; set; }

        bool IsDefault { set; get; }

    }
}
