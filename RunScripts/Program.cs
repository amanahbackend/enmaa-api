﻿using DbUp;
using System;
using System.Linq;
using System.Reflection;
using System.Configuration;

namespace RunScripts
{
    class Program
    {
        static int Main(string[] args)
        {
            //var connectionString =
            //       args.FirstOrDefault()
            //       ?? "Data Source=SQL5040.site4now.net; Initial Catalog=DB_A37BE4_DascoStaging; Persist Security Info=True; User ID=DB_A37BE4_DascoStaging_admin; Password=P@ssw0rd;";

            string connectionString = ConfigurationManager.ConnectionStrings["DascoTesting"].ConnectionString;

            var upgrader =
                DeployChanges.To
                    .SqlDatabase(connectionString)
                    .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                    .LogToConsole()
                    .Build();

            var result = upgrader.PerformUpgrade();

            if (!result.Successful)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(result.Error);
                Console.ResetColor();
                #if DEBUG
                Console.ReadLine();
                #endif
                return -1;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Success!");
            Console.ResetColor();
            return 0;
        }
    }
}
