﻿using Dispatching.Dropout.API.ViewModels;
using DispatchProduct.Api.HttpClient;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dispatching.Dropout.API.Controllers
{
    [Route("api/[controller]")]
    public class NotificationController : Controller
    {
        private readonly IConfigurationRoot _configuration;
        private readonly IHostingEnvironment _hostingEnvironment;

        public NotificationController(IConfigurationRoot configuration, IHostingEnvironment hostingEnvironment)
        {
            _configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost, Route("Send")]
        public async Task<IActionResult> Send([FromBody]SendNotificationViewModel model)
        {
            string result = "";
            var apiKey = _configuration["FcmSettings:ApiKey"];
            var userDevices = await GetUserDevices(model.UserId);
            foreach (var item in userDevices)
            {
                result = FireBase.SendPushNotification(item.DeveiceId, model.Title, model.Body, model.Data, apiKey);
            }
            return Ok(result);
        }

        public async Task<List<UserDeviceViewModel>> GetUserDevices(string userId)
        {
            string uri = _configuration["UserServiceSetting:Uri"];
            string getUserDevicesVerb = _configuration["UserServiceSetting:GetUserDevicesVerb"];

            var requesturi = $"{uri}/{getUserDevicesVerb}/{userId}";
            var response = await HttpRequestFactory.Get(requesturi);
            var result = response.ContentAsType<List<UserDeviceViewModel>>();
            return result;
        }

    }
}
