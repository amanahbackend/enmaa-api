﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Vehicles.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Vehicles.API.ViewModel
{
    public class TrackingLocationViewModel:BaseEntityViewModel
    {
        public int Id { get; set; }
        public double Lat {get; set;}
        public double Long {get; set;}
    }
}
