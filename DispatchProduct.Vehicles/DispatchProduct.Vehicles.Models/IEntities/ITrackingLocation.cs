﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Vehicles.Entities
{
    public interface ITrackingLocation:IBaseEntity
    {
         double Lat {get; set;}
         double Long {get; set;}
    }
}
